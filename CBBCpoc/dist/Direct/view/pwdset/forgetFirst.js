// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 89);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = {
  "check": {
    "marginTop": "20",
    "color": "#FA5665",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "31",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  props: ['router', 'open', 'tips', 'text'],
  methods: {
    jump: function jump(router) {
      var _this = this;

      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      if (this.$parent.id == "forgetThird") {
        if (this.open) {
          this.$parent.isOK = true;
          setTimeout(function () {
            _this.$parent.isOK = false;
            navigator.push({
              url: urlFront + router,
              animated: "true"
            });
          }, 1000);
        }
      } else if (this.$parent.id == "faceFind") {
        if (!this.$parent.pass) {
          if (!/[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(this.$parent.identity)) {
            this.$parent.tip = '身份证信息错误，请重新填写';
          } else {
            this.$parent.tip = '';
            this.$parent.pass = true;
            this.$parent.noteText = 60;
            this.$parent.color = '#8F9AAE';
            this.$parent.tip = '短信验证码错误，请核对或重新获取';
            var timer = setInterval(function () {
              _this.$parent.noteText--;
              if (_this.$parent.noteText == 0) {
                clearInterval(timer);
                _this.$parent.noteText = '重发';
                _this.$parent.color = '#4BA0FF';
              }
            }, 1000);
          }
        } else {
          if (this.open) {
            navigator.push({
              url: urlFront + 'prepareFace.js',
              animated: true
            });
          }
        }
      } else {
        if (this.open) {
          navigator.push({
            url: urlFront + router,
            animated: "true"
          });
        }
      }
    }
  }
};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["check"]
  }, [_vm._v(_vm._s(_vm.tips))]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump(_vm.router)
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v(_vm._s(_vm.text ? _vm.text : '下一步'))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(4)
)

/* script */
__vue_exports__ = __webpack_require__(5)

/* template */
var __vue_template__ = __webpack_require__(6)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdNext.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-86c0103e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 89:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(90)
)

/* script */
__vue_exports__ = __webpack_require__(91)

/* template */
var __vue_template__ = __webpack_require__(92)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\pwdset\\forgetFirst.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-724a793c"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 90:
/***/ (function(module, exports) {

module.exports = {
  "text": {
    "marginTop": "127",
    "marginLeft": "52",
    "fontSize": "38",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(68,70,79,1)"
  },
  "phone-box": {
    "marginTop": "43",
    "paddingTop": 0,
    "paddingRight": "52",
    "paddingBottom": 0,
    "paddingLeft": "53",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "phone": {
    "fontSize": "30",
    "width": "400",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "phone-delete": {
    "width": "36",
    "height": "36",
    "lineHeight": "80",
    "marginTop": "20"
  },
  "line-phone": {
    "marginTop": "10",
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginLeft": "36"
  },
  "note-box": {
    "marginTop": "39",
    "paddingTop": 0,
    "paddingRight": "52",
    "paddingBottom": 0,
    "paddingLeft": "53",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "note": {
    "fontSize": "30",
    "width": "400",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "note-delete": {
    "width": "36",
    "height": "36",
    "marginTop": "25"
  },
  "getting": {
    "fontSize": "26",
    "lineHeight": "80",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(75,160,255,1)"
  },
  "line-note": {
    "marginTop": "10",
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginLeft": "36"
  },
  "phone-check": {
    "marginTop": "20",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53",
    "color": "#FA5665"
  }
}

/***/ }),

/***/ 91:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

var _pwdNext = __webpack_require__(7);

var _pwdNext2 = _interopRequireDefault(_pwdNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  components: {
    PwdHeader: _pwdHeader2.default,
    PwdNext: _pwdNext2.default
  },
  data: function data() {
    return {
      phoneCheckShow: false,
      phoneShow: 'hidden',
      phoneValue: "",
      noteShow: false,
      noteValue: '',
      clickAgain: true,
      getNote: '获取验证码',
      phoneOpen: false,
      noteOpen: false,
      open: false,
      deleteIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAADZUlEQVRYR82YSUwTYRTH/28qllU9aNSDB6LG0hJFpkUkaMLN7SAxxuXqknBUE/CmHosxeDJG9OqSaPTgdiNRgkhnkBBKazDh4MEYPLiwDTB95qudOqXT6XRhmfN77/t9b/+GsMo+WmU8yBkoHA5XTs3SUQK1AKhjoJqA9eJiDPwiYBzAEIN7Kkr5lc/nm8zl0o6BVHVkh05rOoj5DIAKh4dMMdEjFy8EZbn2ixOdrEA9PeOllVXadRBfArDWiVELmTmAbk/+dl9raametbNhC6QoY9sZ+lMQ1+UJkqrGNCTRwkk7b2UEUpTRepboNRibiwJjGCF8pxgf8fu9g1Z2LYFEvsTg6gOwqagw/41NSNCbrDyVBtTX97VsrXvyAwN7lgjGMDs8r1U2NjVtmzGfkwYUUqNBgNuXGCZhnjoDsqcjI1A8iWkhAqBkeYAwL0H3mkOX4qGBwWg3MZ/PBMNAPwhXibkWoFsA3BlkZ5n5CiSMgClIQGPGCzI9CPg9yTOTQL290Sp3GX+za3oSYgdl2fdeGB9Qo4cI/MICapZBrQ2y562QU9XwgRikdzYen9JmaGtzs+ePkEkCDaijpwj02LZpEbX56z13DRkLKI1Bxw0YIaco0TYmvmNnl8GnG2TvkxQgRY3cY+BCltxJuf0iT2ExjI0XU44hoNsv11xMAQqpkX4A+xwksyWU0DN7xilM4ryPAbkmnmfJkIXUyASAjQ6AhEgalFkvRxih+iMg18SbsBlIy3F4WkLlASM45gJyTbxiCwHSQFJroH7XG7N3QoOfD4Njz21aglUQLIFyCVlaNRUIZRkyp0mdBiPCVPSkLrDsRYhE2ScbYpbmmb3si9QY0xI978a4gqNjWpuhLWmjQ/gwpETvg/hcpl4khqsLsXZmVy0Td9kNV2K6TKSP6JA68xqu/wZhfFMcXc71Q5fY17jXO2Y4YYUXNL4ZkL0py6DlClvinhQtYLfDMZKv2DDx1H6/3z9tNrD6l3yDdlU9gwyoRJI/K2L4hiXoJ/J6KBpQ8af0Ou0GEH9K57v8zwPUVfBT2pxs/Z9Gd0ostRPzWQDlDjN5BkwPdVcsaC5tO92sPxsWK4vfMdMaHQNTS/zNz1QNYENC7ieIx8E0BOKecje/XLLfMQ49UrBYzh4q+MQsBv4CdFfBNHHCyTkAAAAASUVORK5CYII='
    };
  },

  methods: {
    phoneInput: function phoneInput(event) {
      if (event.value.length > 1) {
        this.phoneShow = 'visible';
      } else {
        this.phoneShow = 'hidden';
      }
      if (!/^((\+86|0086)\s*)?1[3-9]\d{9}$/.test(event.value)) {
        this.phoneOpen = false;
      } else {
        this.phoneOpen = true;
      }
    },
    noteInput: function noteInput(event) {
      if (event.value.length > 1) {
        this.noteShow = true;
      } else {
        this.noteShow = false;
      }
      if (!/^\d{6}$/.test(event.value)) {
        this.noteOpen = false;
      } else {
        this.noteOpen = true;
      }
    },
    phoneChange: function phoneChange(event) {
      if (!/^((\+86|0086)\s*)?1[3-9]\d{9}$/.test(event.value)) {
        this.phoneCheckShow = true;
      } else {
        this.phoneCheckShow = false;
      }
    },
    deletePhone: function deletePhone() {
      this.phoneValue = "";
      this.phoneShow = 'hidden';
    },
    deleteNote: function deleteNote() {
      this.noteValue = "";
      this.noteShow = false;
    },
    noteTime: function noteTime() {
      var _this = this;

      if (this.clickAgain) {
        var i = 60;
        this.getNote = i + 's';
        this.clickAgain = false;
        var timer = setInterval(function () {
          i--;
          _this.getNote = i + 's';
          if (i < 0) {
            _this.getNote = '重新获取';
            _this.clickAgain = true;
            clearInterval(timer);
          }
        }, 1000);
      }
    }
  }
};

/***/ }),

/***/ 92:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["container"]
  }, [_c('pwd-header', {
    attrs: {
      "title": "忘记登录密码"
    }
  }), _c('text', {
    staticClass: ["text"]
  }, [_vm._v("验证手机号")]), _c('div', {
    staticClass: ["phone-box"]
  }, [_c('input', {
    staticClass: ["phone"],
    attrs: {
      "maxlength": "16",
      "type": "tel",
      "placeholder": "请输入手机号",
      "value": (_vm.phoneValue)
    },
    on: {
      "input": [function($event) {
        _vm.phoneValue = $event.target.attr.value
      }, function($event) {
        _vm.phoneInput($event)
      }],
      "change": function($event) {
        _vm.phoneChange($event)
      }
    }
  }), (_vm.phoneShow) ? _c('image', {
    staticClass: ["phone-delete"],
    style: {
      visibility: _vm.phoneShow
    },
    attrs: {
      "src": _vm.deleteIcon
    },
    on: {
      "click": _vm.deletePhone
    }
  }) : _vm._e()]), _c('div', {
    staticClass: ["line-phone"]
  }), (_vm.phoneCheckShow) ? _c('text', {
    staticClass: ["phone-check"]
  }, [_vm._v("请输入正确的手机号")]) : _vm._e(), _c('div', {
    staticClass: ["note-box"]
  }, [_c('input', {
    staticClass: ["note"],
    attrs: {
      "maxlength": "6",
      "type": "number",
      "placeholder": "请输入短信验证码",
      "value": (_vm.noteValue)
    },
    on: {
      "input": [function($event) {
        _vm.noteValue = $event.target.attr.value
      }, function($event) {
        _vm.noteInput($event)
      }],
      "change": function($event) {
        _vm.noteChange($event)
      }
    }
  }), (_vm.noteShow) ? _c('image', {
    staticClass: ["note-delete"],
    attrs: {
      "src": _vm.deleteIcon
    },
    on: {
      "click": _vm.deleteNote
    }
  }) : _vm._e(), _c('text', {
    staticClass: ["getting"],
    on: {
      "click": _vm.noteTime
    }
  }, [_vm._v(_vm._s(_vm.getNote))])]), _c('div', {
    staticClass: ["line-note"]
  }), _c('pwd-next', {
    attrs: {
      "open": (_vm.phoneOpen && _vm.noteOpen) ? true : false,
      "router": "forgetSecond.js"
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })

/******/ });