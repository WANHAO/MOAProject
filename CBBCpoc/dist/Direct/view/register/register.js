// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 129);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 129:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(130)
)

/* script */
__vue_exports__ = __webpack_require__(131)

/* template */
var __vue_template__ = __webpack_require__(132)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\register\\register.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-973047c8"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 130:
/***/ (function(module, exports) {

module.exports = {
  "head-title": {
    "width": "750",
    "height": "60",
    "background": "rgba(255,255,255,1)",
    "alignItems": "center",
    "position": "relative",
    "marginTop": "38"
  },
  "goBack": {
    "width": "18",
    "height": "29",
    "position": "absolute",
    "top": 0,
    "left": "36"
  },
  "h-title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "register-content": {
    "marginTop": "127",
    "justifyContent": "flex-start",
    "width": "750",
    "position": "relative",
    "paddingRight": "36"
  },
  "textPhon": {
    "fontSize": "38",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(68,70,79,1)",
    "lineHeight": "74",
    "marginLeft": "53"
  },
  "phone-box": {
    "marginTop": "43",
    "paddingTop": 0,
    "paddingRight": "52",
    "paddingBottom": 0,
    "paddingLeft": "53",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "phone": {
    "fontSize": "30",
    "width": "400",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "phone-delete": {
    "width": "36",
    "height": "36",
    "marginTop": "20"
  },
  "reminder": {
    "width": "583",
    "height": "74",
    "position": "absolute",
    "left": "36",
    "top": "180"
  },
  "rem-content": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Regular",
    "color": "#3894FC",
    "marginTop": "-45",
    "marginLeft": "16"
  },
  "line": {
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginTop": "10",
    "marginLeft": "36"
  },
  "phone-check": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(250,86,101,1)",
    "marginTop": "20",
    "marginLeft": "52"
  },
  "note-box": {
    "marginTop": "39",
    "paddingTop": 0,
    "paddingRight": "52",
    "paddingBottom": 0,
    "paddingLeft": "53",
    "flexDirection": "row",
    "lineHeight": "80",
    "justifyContent": "space-between"
  },
  "note": {
    "fontSize": "30",
    "width": "400",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "note-delete": {
    "width": "36",
    "height": "36",
    "marginTop": "20"
  },
  "getting": {
    "fontSize": "26",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(75,160,255,1)",
    "lineHeight": "80"
  },
  "register-bottom": {
    "position": "fixed",
    "bottom": "40",
    "justifyContent": "center",
    "flexDirection": "row",
    "left": 0,
    "right": 0
  },
  "bottom-logo": {
    "width": "21",
    "height": "30"
  },
  "bottom-text": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(203,205,215,1)",
    "marginLeft": "11"
  },
  "node-box": {
    "position": "absolute",
    "top": "500",
    "left": "255",
    "right": "255",
    "width": "240",
    "height": "116",
    "backgroundColor": "rgba(0,0,0,1)",
    "opacity": 0.7,
    "borderRadius": "10",
    "alignItems": "center"
  },
  "node": {
    "fontSize": "26",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(255,255,255,1)",
    "marginTop": "45"
  }
}

/***/ }),

/***/ 131:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

var _pwdNext = __webpack_require__(7);

var _pwdNext2 = _interopRequireDefault(_pwdNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    var _ref;

    return _ref = {
      clickAgain: true,
      phoneCheckShow: false,
      phoneShow: 'hidden',
      phoneValue: "",
      remShow: false,
      noteShow: false,
      noteValue: ''
    }, _defineProperty(_ref, 'clickAgain', true), _defineProperty(_ref, 'getNote', '获取验证码'), _defineProperty(_ref, 'phoneOpen', false), _defineProperty(_ref, 'noteOpen', false), _defineProperty(_ref, 'noteClick', false), _defineProperty(_ref, 'open', false), _defineProperty(_ref, 'tips', ''), _defineProperty(_ref, 'isCode', true), _defineProperty(_ref, 'send', false), _defineProperty(_ref, 'i', 60), _defineProperty(_ref, 'deleteIcon', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAADZUlEQVRYR82YSUwTYRTH/28qllU9aNSDB6LG0hJFpkUkaMLN7SAxxuXqknBUE/CmHosxeDJG9OqSaPTgdiNRgkhnkBBKazDh4MEYPLiwDTB95qudOqXT6XRhmfN77/t9b/+GsMo+WmU8yBkoHA5XTs3SUQK1AKhjoJqA9eJiDPwiYBzAEIN7Kkr5lc/nm8zl0o6BVHVkh05rOoj5DIAKh4dMMdEjFy8EZbn2ixOdrEA9PeOllVXadRBfArDWiVELmTmAbk/+dl9raametbNhC6QoY9sZ+lMQ1+UJkqrGNCTRwkk7b2UEUpTRepboNRibiwJjGCF8pxgf8fu9g1Z2LYFEvsTg6gOwqagw/41NSNCbrDyVBtTX97VsrXvyAwN7lgjGMDs8r1U2NjVtmzGfkwYUUqNBgNuXGCZhnjoDsqcjI1A8iWkhAqBkeYAwL0H3mkOX4qGBwWg3MZ/PBMNAPwhXibkWoFsA3BlkZ5n5CiSMgClIQGPGCzI9CPg9yTOTQL290Sp3GX+za3oSYgdl2fdeGB9Qo4cI/MICapZBrQ2y562QU9XwgRikdzYen9JmaGtzs+ePkEkCDaijpwj02LZpEbX56z13DRkLKI1Bxw0YIaco0TYmvmNnl8GnG2TvkxQgRY3cY+BCltxJuf0iT2ExjI0XU44hoNsv11xMAQqpkX4A+xwksyWU0DN7xilM4ryPAbkmnmfJkIXUyASAjQ6AhEgalFkvRxih+iMg18SbsBlIy3F4WkLlASM45gJyTbxiCwHSQFJroH7XG7N3QoOfD4Njz21aglUQLIFyCVlaNRUIZRkyp0mdBiPCVPSkLrDsRYhE2ScbYpbmmb3si9QY0xI978a4gqNjWpuhLWmjQ/gwpETvg/hcpl4khqsLsXZmVy0Td9kNV2K6TKSP6JA68xqu/wZhfFMcXc71Q5fY17jXO2Y4YYUXNL4ZkL0py6DlClvinhQtYLfDMZKv2DDx1H6/3z9tNrD6l3yDdlU9gwyoRJI/K2L4hiXoJ/J6KBpQ8af0Ou0GEH9K57v8zwPUVfBT2pxs/Z9Gd0ostRPzWQDlDjN5BkwPdVcsaC5tO92sPxsWK4vfMdMaHQNTS/zNz1QNYENC7ieIx8E0BOKecje/XLLfMQ49UrBYzh4q+MQsBv4CdFfBNHHCyTkAAAAASUVORK5CYII='), _ref;
  },

  components: {
    PwdHeader: _pwdHeader2.default,
    PwdNext: _pwdNext2.default
  },
  methods: {
    phoneInput: function phoneInput(event) {
      if (event.value.length > 1) {
        this.phoneShow = 'visible';
      } else {
        this.phoneShow = 'hidden';
      }
      if (!/^((\+86|0086)\s*)?1[3-9]\d{9}$/.test(event.value)) {
        this.phoneOpen = false;
      } else {
        this.phoneOpen = true;
      }
    },
    phoneChange: function phoneChange(event) {
      if (!/^((\+86|0086)\s*)?1[3-9]\d{9}$/.test(event.value)) {
        this.phoneCheckShow = true;
      } else {
        this.phoneCheckShow = false;
        this.noteClick = true;
      }
    },
    phoneFocus: function phoneFocus() {
      this.remShow = true;
    },
    phoneBlur: function phoneBlur() {
      this.remShow = false;
    },
    deletePhone: function deletePhone() {
      this.phoneValue = "";
      this.phoneShow = 'hidden';
    },
    deleteNote: function deleteNote() {
      this.noteValue = "";
      this.noteShow = false;
    },
    noteInput: function noteInput(event) {
      if (event.value.length > 1) {
        this.noteShow = true;
      } else {
        this.noteShow = false;
      }
      if (!/^\d{6}$/.test(event.value)) {
        this.noteOpen = false;
      } else {
        this.noteOpen = true;
      }
    },
    noteChange: function noteChange(event) {
      if (!/^\d{6}$/.test(event.value)) {
        this.tips = '请输入6位短信验证码';
      } else {
        this.tips = '';
      }
    },
    noteTime: function noteTime() {
      var _this = this;

      if (this.clickAgain && this.noteClick) {
        var i = 60;
        this.getNote = i + 's';
        this.send = true;
        this.clickAgain = false;
        var timer = setInterval(function () {
          if (i == 58) {
            _this.send = false;
          }
          i--;
          _this.getNote = i + 's';
          if (i < 0) {
            _this.getNote = '重新获取';
            _this.clickAgain = true;
            clearInterval(timer);
          }
        }, 1000);
      }
    }
  }
};

/***/ }),

/***/ 132:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["container"]
  }, [_c('pwd-header', {
    attrs: {
      "title": "宝石山注册"
    }
  }), _c('div', {
    staticClass: ["register-content"]
  }, [_c('text', {
    staticClass: ["textPhon"]
  }, [_vm._v("验证手机号")]), _c('div', {
    staticClass: ["phone-box"]
  }, [_c('input', {
    staticClass: ["phone"],
    attrs: {
      "maxlength": "16",
      "type": "tel",
      "placeholder": "请输入手机号",
      "value": (_vm.phoneValue)
    },
    on: {
      "input": [function($event) {
        _vm.phoneValue = $event.target.attr.value
      }, function($event) {
        _vm.phoneInput($event)
      }],
      "change": function($event) {
        _vm.phoneChange($event)
      },
      "focus": _vm.phoneFocus,
      "blur": _vm.phoneBlur
    }
  }), (_vm.phoneShow) ? _c('image', {
    staticClass: ["phone-delete"],
    style: {
      visibility: _vm.phoneShow
    },
    attrs: {
      "src": _vm.deleteIcon
    },
    on: {
      "click": _vm.deletePhone
    }
  }) : _vm._e()]), (_vm.remShow) ? _c('div', {
    staticClass: ["reminder"]
  }, [_c('image', {
    staticStyle: {
      width: "583px",
      height: "74px"
    },
    attrs: {
      "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAkcAAABKCAIAAAA+IGdXAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFCMTA1NjZENUU1ODExRTg4MTk2QTQyRkYxOTFFRTFCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFCMTA1NjZFNUU1ODExRTg4MTk2QTQyRkYxOTFFRTFCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUIxMDU2NkI1RTU4MTFFODgxOTZBNDJGRjE5MUVFMUIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QUIxMDU2NkM1RTU4MTFFODgxOTZBNDJGRjE5MUVFMUIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz787AfdAAAdUklEQVR42uxdCWMbN64mRrLjpq/b/f+/cpu2iS1L8705SZAEOZxLl4HdzSa2NMMTH24QAKOk9AjUHlVq/qOkpKSUJPoiqPbz1E7z+6uyxAemS20uMBWZQ4NtupNKSkpfFtV+fZp/O1T7/ZV+e9FNV1JSUlJUe1h6/zT/nNwc/++V3hTYlJSUlBTVHpE+zubvj3CCf3yjb0fdeiUlJSVFtYei08X8/S5Mjxpge6PXg+6+kpKSkqLag9Dnxfx4T86tB7aXSoMOlJSUlBTVHhzSLLD9542OFfuJIpySkpKSotq90bk2f70XTauBsT/f6EDCz5WUlJSUFNVuT5fa/O99xpwaAPvvG1Uk/0pJSUlJSVHtlpDWaGn1zAlVncZWURL2lJSUlJQU1a5KzSSa//71gQbYFtChMn9+owyAKbYpKSkpKapdCc/aP02rpS2DtEJgU2xTUlJSUlTbHc96SPvxjnO99oHHqjVFTi+ZYpuSkpKSotoeeNZTA2mf9TZPfqnacP+ihVNsU1JSUroVCnTZWQMW+K08HgnVxJH++MDnZcu3vB7akloz5AKFNyUlJaWb4gLnw3eNapND++eEj/P2750LbApyzyH6uU28+hunz5Xu0KNoD/6W8X+uYbWzvk5zzhuNI3+YS+r3WUT3vw1QDWWXDSuvcpb+PeH9vNfyfTu21f31on4dJLsalqDs9SHKkgLeg5yiG6oJeYCaedyvfK5K9AH0qk6DYZWHaujaLi5BtVpaIA8h/aUbHoxdluznJ3597rvKb8fpLqPKUG5oZ7gGtFBul2nxe5F8OaYOG+UHMTI1ujONEFd5Y6wYXWNGuBdQi1cV40IUqnok/eOGXC6+73WXxEVd32AaOUPwsWlUA9PywoWh4aEXmM9Lm/5cd92K2+wxZcNKArNdTs3pIjXwKik9KfW3u+10X7XN7l8OXpFei3DwPVO9GSPgCklUs0Ilxn/4oNZ8j84Xc7q0IfWKYUrlQvRiVLMneyfDoCKmktJd4dzLoXUGvR4iQx8cQnFrZA7VAkiDr7Q1/OX0iVNtoGimNAeTuHg1F5x6XS2wE9BGl8d0BvYqsubFlpwUWu8jAzSzpoqeE3DXCzqPPneVokpvKJnfjubt6FXJaP4O5pjg1kgB1ULlDO4vDWf5OLeR9HjyI6fnbes1Xe0VSz2BVqMLJZirdUEDyTepXLdqQyOv/NcR8rg5PY7iuyKrehh214zy7aX5b7tOPCwGvrhMsa6G8cBZda2XspuffNbm/QzVz5SWXOPuz2o38ZZWAwxFf6PRiE8UjUAK91gcehaz+53ch9sysPVPez75sd87+NE65H3AxfOQpE70XxdBzuPj0QewNEZ/2zisvakRNH9/bRs+c4NNYI2UUA0tDxq0tO7vl87keK4f6nilGQ3U6LHou2tk6ppdu2VBH/kxbKaxSW+chCsqeUrx8PZDtc3xrMCKCMc3h38HsrX9V3qZEQoSQnQe3ULfa4cG9maCPeQk72YNBLGsA6YQS3eLDkqAlJsjMWLz+33Tt6P5/uIWvlffnDVSsEB2k7zUgwutgbSPz9ntXbaVo6/MxOPjaNfvJqqqmHv4qE94EHPT7EhoKQwa0vyxw1AXPNnFvlMAH7NVKxTmrfoc34ZoM1AoOhg76xYMQqfeUcJkOBsZIBD9SgDc+E0UoNrkBNuAvdyHEB/B9hXdIAadsv2T2mHAVJV5LDpU5o9XsmemIrYjIqo1knV9MZeur/SHb3UUNGtay+uXiyH9Lo2gA3YvhwPE1VQjX9/x7XCTGc/K+P/ja7qV4bMmLmqK69D3yBl+1YuZ7aNMqylXc+6ZwC5p3k298RP2kDxujnze+SqBFuwCbFdW0ST2mhNXIAk1XIHL4FkgXtRBdEBaQ6Z9phxYF/ngMhBjeQWi0iMpre4GcvOjXcZmub6/0qHnpF0G2yAQyKjWZZ6dLubXGZPMiKJSMfep0lFkBAmEPiJJXPOPLKJlbZTaA/kCqXlgzqVUzuB4MGd5SYc1IiAS3pQbQlpskouMgejVsmrw8tNgUoMgE0xOv8aQ1cRxZU2cBYeZTBGmBJq7YzChOIkIt3Twz32t5tL3lw7YaMx4q1Ko1kHaz0/UXKqALJJk0Ii72ZH2ZwZ3tXSe0ufy/pUezGs7HgyarP3VoFQ1nxlUKu+s9xKi80z27wtEMGteAIE0mPJuLs9q9nFDRMmNYea8yj+/4Xzr7k7lbVwlcRbobhTdTZzFsnltbqhYZoJ+GmpjI4/U5m5Xg7hzlHfLmM8z4O+2Fa/iQyAa32is2TVEW0YbCcgXhmZuZoCdRhSNaRiP9TVTAKXwsIoggPdg1mTaWHtk2ZrYeQ4/7j6/bfHQPbjzV3gC0fZPuD6sFdbK29HlRCu+zL6OepQXiV8vk4E1DL6nUdxkcRbGOEgLGEuAKGJMCa0AGCxdOpoSx6n4Ldj0vmA71N9bybPH6tcFL4Zeuh6ZVYxqDZQ1cHY+Xz4vnbW86q2V1PsYuY4dnL1OOzEMNUaRiBykEKtIhljsIRphAswVJnhPOTpSr/snrkMn3Xm55Mb528aPwUQhov2MiL9u9EPKx8hdm9FIMtol2mDT5g6j15FptFyM1w/uKLnraCN/7QEbLyeNtgtYcaGG8ARvaOTdbqLwtI6dipp1xBCjxe1I5ActMuMPIcjz73cPTCquem7kM2WKb8wQKjf4IMF2qmLfoOQTUtGHoagsCFfpJ8xGP9y+JqzoDaAF6E5pH1nCzJ6Ps/BNF1XidZSExMqdNB5nISw5TZWc3SrOokTMsKBrZlYevqOIWYzcCKKownSYMGZqYD6l5uuBg8IpGNK7HHsZ2UZbqfETH9Xh+0t1rCi0QLZBjxfz3pcO8XlAnR1ZatzkQ31858j3AGNKPUNabUP2+3mVLhTl4CZeHgvnpkBeAehzbfqaZgPIXQykg+uAUJLTQo8ueYNM3uOsn4/kLKyi+bpoYLLCwXgqSbicXLYKJuiloxopctxJ69lRIWTt8anb1X37hAlYaX8BIs3CYxdBDjsTP+jrxVnsGpefnztkXB9AiIqCLZOfKT8eNB+My5Mc+p/0asNLZcjmT5gRXd7POJ/MKWH3HUZjscEH55TvLc8f+/dWY84BIrZcsU/Pc7Yj9Odzhinud5fuYOqS5WaXeJbvjYbiAs73xpc0IwFk7BUiig/6W1Ju90Kr4cSkiYcTF0KZSnWBOYw11QLcQiDjeBMcDbXxXUEkHMn7OzyBmN4VlNimJFZuxXawsIg/mFN3Ztj6TcINYriajLOwbYttULHGWdxWhAITiMVlzMNVL9GKA2ij5yqHJgIcYEYKxywvcvPbVnloHn4ZDYP9606Xtg9n3126UApAJDcZJpFR2dBnSVs0BXIpwXyrVATiRlPp+VLmwPiXakhvdyys8uAkVMUSQTFYOmyMFiDP8kq+g5ALSlNGO2s7olqyLaclWwoqjkp7Jwdwk4x/4VInEP3OnOoIxJ3KT4itEWbI5mMf8vcfcXBBNs7ClMCVxlms065u5biKRajMSAK4SrmZyQfOnrdzCBS3D/A8oynBriSCqdXYzl16GtmE60ZRu5i67EAgUDBXNKDBivTeAouwNzBhz2YeKEoxiATrDMryUrfURLkwTlOcL5H0ixhPcfEyyrnpknIsvx5PpON9/D7YGbHgUhNUVvShMVbEKTAwImlENQl7KZnQcUbkvTH8cmC5pfAJ5J9kLqVSZFWLJS0xNoFSzDTQjxFapy032UOvvFU9iz7W/1CCalPy7po4i01MfHPhan0g6zZWUxaafqDsWklw1bCFQN6CdLZ5IOGkczHJFhLrPCTyj6zDukVaVMOlZbLND05nfNTJ11TRqQk0HupsUM7DVFyzYFsiM6NEQhiBckXfWyq/LX7+Mj0jb9UML5uPTMEYSPLbes3s6dq7vInyOm3rQ0LhzlqGQ7P8VMBbwGUmKca5lfBWXs9iG2Y65s+U17PYA18LsWQVqq1+BeaXSii3zdrW0lwao2K4yk/Emp1D88xqCOdaYztU1nCjF2fp1Db97GJI6jY+stA0R6ICO8Jy3UthuIZ/vpzr1SwIspIUiZv43mw4FzZi0yLMiMpTKJSQgKOZJ8B4Pjl+Kshn2aKPjRK2yTVcbFveF84i8ZfZZnNJqsBVGPSGX9wM5DTOQjIJGEn1L9+yuXEWlpOjGK5KJ+X/Y1Dd/NIwi23gTmusnYTUDrVFtXNXv7hrATpPxGCvcfOBW8FbUaxZAmEWS2VkWIojDBfzRFPge8O6aS6+8GSjWkmyFE5pq4jNa0FIiKTZi3iwoegTu6AKFzEIZ5BFTpL/chuWrHSji/OscRbG5GocYyoAXpR0qXgvltnAU77eI43t084A0r2mJmxxxu+veOvrTL43hQIvQjflmqbsJFtcBaJQHs/nvS0ATpO2biW1ip4vV1FtsBQyJeQvrpYJJYj8mZLJBVjihmclU76Q3U74EiWiNBzkUyymTlF89ZKaHG6/bg8dZ7GGNRHJeaGZkbS2Kwo5AKIvW6uydRQ5pxFjreKaBHEWc7P1iddaTsBVGLrsG6KsUzxo47lE2p6zU+JMj33fmUaDq2s3VVp94u8qzMzl9PYQTvM84Tzh08z0vVGZwW3yMqf2Lx8SEutDDtLAFJQpZJKHRC79iCtccWyOJ+HSKjaaH1eJosYXgW9T2FZt3Ls+j7y5HcdDJ0TXbdf5PvHzcHAPtBlaQ4Yv18Uj8OPnB0iKO1hxZmY7YyQzaeG31ps3l8PVTlFmZlrkxWK4iowKiPyuBP8mUnJbyS/ejjRumak4i0h7SsIVAmsTHBPAgrrplKyCkD8AqV8d+zFc/Fpq22bu7K23lQdrcB9PNY4tc39ivYfDAPmpvvEKcN8bCjToVEYWNxIKWhcrt8DHltQ2fY8gRyZKIJNsGfA9haEjTUL9Vb2wN2dMQf9P+Ilu5GUdVJUL2+uj0lu0w8CS+umeO+RDtxYUpBxAUnDi44Gp1Iidl+v+6lksPz97xFl4Ai6GQkK5OAuSb2sVl9INOoOTF2fhqiDRNJfLMH0LwyaOJCBXGpdf5hRcUWTnuNRhCXgsVbYKhTNIMNz85dhDWs3MjwtaqVK2U3vJA2lFDtmssEAac2VQ5WDJRCEhwXYTg9NEe47pgfUNFKzvrRpPHvnZyqmVFOM7XGA6kuJPztyKgmuDoUK2u6h9VorxozdZiPxKJuiqvUfR+Wv4Jsj3q3X1CYebWQs3B/CuUICMDfJdxvPQuixrxyzIhGXiBGPsUqP0PZRdFhFldXWuItOZyZY7KQTjAK5kMS44kH6nShTD1ST/JlFxJkkYmxlnwbXGqm99ULMxI5RWU3CVIS869woHEf6WdTB87BTpVcJwSWxMyeBo6ZUuvBXE30WeXiIauDKwtIfvDf4/w04/Ukw5kNxmSlnh0wIHpqya/ENcFwEre+19cUzuBoPnVbKLXQfM3n2ZOcI9DYHNMN1BZngpH4zYioihIygrM2zedG03bgJTFPtHiwTzPFxl4iwCS6BgwsGYu1LCiEjiMNkv5cKUshJ/xnFF0UMC5QFrajpbrbGKlixxeCbhiii0bBUejOW5hlEX7/4rx14zaO+eLTC/1FRNEn9MGfS2tS+hoCOtoQRTS7gxZjGHTXxvJtBv/EGWj4OKE92QeEjGqkkB+2Yf5mhKgckCnmqL+fsrHzDa4NggqT3lVKsgdZqPLRZ6KLsfyB6qybW6ZiH/PbBS4yzuJ85C/HmoqLEC5hRlLlH2USi76YXHBj5jt2t+HALQe5vJ4mScsguwbQgJFZS75dKWUztISBWCZHOopjrDma19b3ElRoiyp/RDJFRezL0bolXTeI43L66EPHtsoI2FrVlXHAPswK+x0XkvT/yXCt2vveHLDHrXNFQGFjPYYpKz4ErjLKIl2jbOgtJFA+IcZTKuGta0Ir55mr/fJrNmqsXRMt0uowuHinWqXSw1XMuCIh4+8ZIjKtdkbSmZznCga/veUj1u6nRZvwWsMJdHiQlbK1npZ/QFwi/SL0uvSB6PTVqor8WkHcIfsHXpps11LNqMt8yPs4gsZhpnYe4gzqL/s2doIlyJvlK32kjqZ+gqT1UYFOKZ2C1UHQMxObvTQGwx26NnhCHXf6vmzv9bX8JUKZOKsyTkmubYQxOYHEmsoMEt0ffke9uKPy5LdHNSbWWi/nBJ2LPm7wVIY8yMSoB7qxq0+jtP2Tt7QZyFkRxXGmdxD3EWQRlSKuAkDVJ8dtPsG8EcfFHAKg9d3cg26YUWVZMRA+UMb3dBDrBc19CXwy0NFEVmIr8PNXGwgVTcCC7oMWg5SqwSSp0uUlzie5uNJZvmvRUCQ07FiRLdYiuodU7wRLdYBxVhjyTILMrEoHn2tPyyr09028AM+Ny9sxfFWWAWXEV4YzTOQvrA4jiLug4rjOcLDffrQPUQSRyLJjy26FiVVZXHxNYHixZ/4EiQ6j5gs/KpGytwNINVuWnH4b/jc+p0ZzhISrTAMq7ueytkTIsT3erEMKxpQqx3JcBeUIMfYSfP1C21v63SVuXZJrIdtNvbqID741z+82ui81M+m0K40jiLTc55Js7CApWg7KZR9/VIyw7b+tgL8Q1HY7y2Heb+KoNsfJ/Jc7O5tDDf9xaHOQm6DpzD9vZ5b5smuuWzAjAFe8ZEaORPMFdhEtuETXJp0eW33bS3wH12uaRNZzErzsLj7xpnMd/issb2WPe1AuDFWcCaryipHe56ijd5/hAtUnW52MY3iZqy3JR7o8ne2c7BSMl+K2FQyQP53q6Q6IYZyXCIrnewvoHH2gWhbBE2aQoS3W5S0nCpCnjvvbPnxlkYjbMwe8VZBAsyPK1mcRbdqysWeREyMbiiboP70+x/flY//zh44xmsfV7a2j/NQWz00GMlM/q71smieg0xMyXGIsxkHN0D+t72TXQrgLS8gdEpUocxTRte7WkTdTddUwIqn+iGKQH5nk5+q1CP/aIwiPzFvbPzCsFOvbM1ziJ14a4cZ0EcQTmU9nEWlQC9zfAuY8/Frha+OR531L226nLeGkSrrhfqmT26mczhKHTgfVAK6vdnTGo2nJKv7oP63kQjp4j9ixPdZjQypdAg2chM1WgQrjr50Q2YV2+jKN7SdwNg1gFI2cpieYWK1iFpjC3XaWb3ziZeqaSKemcj3xag2CcUrAxpnAWDtIeIs0htdMPtazPIFvnKUH3HLlvPL44o3FXRXyyf0GfXKbSBsZ8nt6G8CdsXp7AAMXPwepUzU74349VprRPXLNOGdKt+b1utBq07spYjVORqItOYR8lNpsR7AgS2XM7KWWdxbCqB5WHPytSBlb7GDDPRl+qdnYppglRuqo50ZdlGt38L73i0YMF0din6fa9IUH+vM8hZ5uWttKIdp7Ni0Y494zhWnqiukMalLqo8U4koQd/A93YLaKL5zyZpYYlZ7bnIH/I4ck1hDKsjYF1Lbj0rrzJLFSisCC8zZk4238HAY8cdhMCERQyQifhC0njArQWLe2fHkLY86zwbZ7Hs8EzGWcQGEi/SZB/OmFHUniDO4rl72R7tkToeWo+aSXcvRHTFnyZUMu6dbbjvDcm7Fxx30bW2ue/NiNmpBZI7j7+vs7VjJhPdCm8LRY2wAwkXEY/nqQXE0c645gC8o5ttDsCL+tfGK/HlcjwQBYuvgD0jJbrV3aVqPdO+JWpu72zRGrzVjVve5W59L6H5cRap82mMxln4V4924Y00uRG01oQTKsQjg1pm8jyasar6y4hqziccHEfaobj4nWlmwcRNse/NK2eHULTM86bZvje/CHWJ741/n1DQEHIq0Q2ZvKW4cn/g2+O822/kEcBeHbFBw9DLM0+N/QFsTC9GPAPL2eftv0XYM34oOaXVqZRZ8vUQAUD3oN6qT1+1dzYX/u4wziLPcL9OnEUMWoh9zEjI9EtbZ8Q/abtYw7wsNRnSpVv43lj81wfwlKlq+6t6lJBfNvS9cfxDisFhWl3DChsUlbwiyCiIMqk5OyZf7uY2XBr5V8wynHHYCFGs1tkW9NBxbXFYaEzg2wMNLdZ4/RSO8ZiDEP2t7C3Yp7N5e/Fsa7Vva13fO3uSX2xiklocZ4EozqKnS+3OVW8sLWHop3O7wVX3FaIZVseVlsC6U9om4yzO9YCvZmmcxZ2UdrpJmbeV5gSq6+GeNnvwfsb7p4LUEg0vhTcZSKDEE5HWA8yGOfLs6EDUtKREt/xgKELWWMjllkYkwDgJe4HKuAPsDd1Ba69eVwx78Zal2t7aNHBeIb5/9vnSmv1DSRu+dcTX8esE1D0E1X6cRYBqYpzFfYqw5tn9Ug9Nv710qGa3qlGZf/xSba1AniXJrpIoNxUDBrK/omxNqU1ErKFEelYAF+OSkf6AOMG4Gyqiup13BXtuvuQUaFc+myIbWnHgicOzVFoYot7Z0gY9bu/surPIHSvh56Yg3PGZ1BGl/Vj0f3/rUzrhlOv3T/xSdW2mxQYpxpGW60QtmwrDPcry3uKQEBv7S52bZwiyoGT7m1DHGVUWrkkAnv3TSKqefTvvZQyEJtk87DnRgXaHPSQKyZsoctXMj6gkk+5BbmR/rTiI5W3qVhfyn6vZmHGvRV3tytH5T8mCygWs56a3o/n+6qNaLzf9731371pJhy16rq3ayvdmNUJT4HvjcXfo/AFUDSraZYzLMiTFVfNCYhLierU0U4ORYM8DUYRaXR72TKqB+E6wZ5yuZnxr7coLQsWHP0S77I2gpfflOtobpNKjas1T2ooa7vHnW5/7ixDDThf8/aFLtBl+b+h7gwk7yZms743nfgVs61IPsWGpJziOY8MUOzgNi8xmOGzej0hRpEMW9uKB7Qd7cjrUdbPgKa/93MIot0nbcdrugUpKnP54NS/H/u5D0Mz+OeHjrKu0jhlt4XuLfVRBoHyJxVIICYFr8yFatFI1L8o5e2BnQ1SQM7CLGtYG0CmybGDOE0Y531IJ7MkZfmzRqzhdDGre2QApVTNT2om+Hczv3yx/A2KLSh/lf6l1rTYTtxf43mSFb0HEdiq6oySf2o8dRGJeYe4UnDmxUBeJnxx0LjY8KDGCPSCcbxwlaLvtuN5DtUvc5j3hbANusGqHSOzIZkz/RkEcqUpLJSCkypbSPdChMn9+YyxCRDXTOdj+Gh1sjmcxOzixpGDvHkA49PwzQglXydgBST+Qa1I8r2xLi75ld6dOeMUWY7Ows7wHMYXql1AsPzM7P1yCfCUvrkFlQ+2NcbH4Dk3HBDWTSLFCzNm7p1XcLFmzuMe9t/uKQRzBDi5GtScQNyfFO6U7p+a+/+ebV+WRrP0xBraLD2whAsVVUqLqGEYSwBFZilKsOcSzrwRpi9kKz+29piidElAo0XctVCWD+UJIbyDxJDG901VFsq412woODhoH2PMVtdQG4Hbn7WpBHOJeqhKm9BCQ9sc3L7C2NbVkUK0Hth/vqP1fVRkcmpSCEhcmE/H4BfFspQSKG71dTMbClNrn0pyN11ktADBEspTrAuobJymG01GrI6n+PRJLhvs7crug2vPqZEpfDdJCVEsBWwNpP+b72CiJaOEdmo7yVzx7FEzNhM0l9m9u4wykUwhEn5yYJ+5+x8Atju3UI7fgDODW0ZtKX4QaMPvjNWwvM9Q5mES1/sr/PJn388YXICkiQi/Ds3E0Qc+wxTsskJCIP2V6RgLYjK+3BS0CRMzV86akdOfUZlu/hOzG+R+CuP5Meunpgn9Ppt700qtY96V1u+32nUpOVVS1BI+4XFHvbEp/YPpyYUL4iAunCS1ypP5w+VcoXYGL3hVf3YrVV2R+fxny0kRIm4dq3W/x62zeP+/rXC5om1TyEL17T3zJ8dBTo3CmgfmX9pi4n1wSI5yIZHqJbms7ebIZfZNUtFiQozgHe7IaUA28n1uDpBZCVlK6rd62WLZLwRZNqW45jqkucKU9DjyZt0PbyCnR0DFKERJrPqLIpYHT2Zwuba9RPcdKqhFeXzsJ4W1xhmNh+Gwi8Eevv9JOx/vl0BYNabvTJXvnCr/4fwEGAA0l/mMAfm5CAAAAAElFTkSuQmCC"
    }
  }), _c('text', {
    staticClass: ["rem-content"]
  }, [_vm._v("为保证后续能顺利绑卡，请用银行卡预留手机号注册")])]) : _vm._e(), _c('div', {
    staticClass: ["line"],
    style: {
      visibility: _vm.remShow ? 'hidden' : 'visible'
    }
  }), (_vm.phoneCheckShow) ? _c('text', {
    staticClass: ["phone-check"]
  }, [_vm._v("请输入正确的手机号")]) : _vm._e(), _c('div', {
    staticClass: ["note-box"]
  }, [_c('input', {
    staticClass: ["note"],
    attrs: {
      "maxlength": "6",
      "type": "number",
      "placeholder": "请输入短信验证码",
      "value": (_vm.noteValue)
    },
    on: {
      "input": [function($event) {
        _vm.noteValue = $event.target.attr.value
      }, function($event) {
        _vm.noteInput($event)
      }],
      "change": function($event) {
        _vm.noteChange($event)
      }
    }
  }), (_vm.noteShow) ? _c('image', {
    staticClass: ["note-delete"],
    attrs: {
      "src": _vm.deleteIcon
    },
    on: {
      "click": _vm.deleteNote
    }
  }) : _vm._e(), _c('text', {
    staticClass: ["getting"],
    on: {
      "click": _vm.noteTime
    }
  }, [_vm._v(_vm._s(_vm.getNote))])]), _c('div', {
    staticClass: ["line"]
  }), _c('pwd-next', {
    attrs: {
      "tips": _vm.tips,
      "open": (_vm.phoneOpen && _vm.noteOpen) ? true : false,
      "router": "registerPwd.js"
    }
  })], 1), _vm._m(0), (_vm.send) ? _c('div', {
    staticClass: ["node-box"]
  }, [_c('text', {
    staticClass: ["node"]
  }, [_vm._v("已发送验证码")])]) : _vm._e()], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["register-bottom"]
  }, [_c('image', {
    staticClass: ["bottom-logo"],
    attrs: {
      "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAPCAYAAAAyPTUwAAAB6klEQVQoU32RPWtTcRjFz/nnxiRNKVahg4MvoG2aWFNtaiyIoA6Ck4ij30DwAzhaRAVHFYpTHdqlWAeRDiIqcrU3vXExL2JRFESsLtpK0t7e50gKAQfxzM/h/M5zWAnr8yZM92aGHxcK3MB/xKBSf0PHfhDPRXd7fHSwSjL+l4dB2HhFYgcMAhEBmOtJ2/V8Ph+R1N8mBmH9BcF+ABGJl4p5h0ntUazROEpPl8v7VromVsLGM5A/nXRX+v20VCpFQbV2mnJXBXwjOdOTihcKhcIag7B20clbKJWGfnQjF6uNCUqTpNuAWQLkeznMsBLWrmwiOX/syIFmN65areVjuRsAJKhN4AvBvVxcqj8kkEjQe5DN2lwul1v1/bf7k+nEpEx10q0CyoE412GehdS3VdC5T0zY7C/LNLPWPuXEvMF2kTwKIMdK2LwvWIpw66RWKAsib5vvRVERjiMyDIO60OnDYKl5j842nfAOCa8FxUMwPhLtZGxcdsQEwPOAZTsY10i2JOsDXBKydTjvCWRlSoMiz0L6DGInX4fNQ0ngTCwNAGrBuc5yocXxiCMvAxgQ8FGmm+yw+L6fcantRY8cl+QJXAZwiUARwFRLmjpRyn/dOu6qVlvpXWt/P0xgN8XjRHxrbOzgh+7//wD0B+l1sq60ZwAAAABJRU5ErkJggg=="
    }
  }), _c('text', {
    staticClass: ["bottom-text"]
  }, [_vm._v("杭州银行宝石山")])])
}]}
module.exports.render._withStripped = true

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = {
  "check": {
    "marginTop": "20",
    "color": "#FA5665",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "31",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  props: ['router', 'open', 'tips', 'text'],
  methods: {
    jump: function jump(router) {
      var _this = this;

      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      if (this.$parent.id == "forgetThird") {
        if (this.open) {
          this.$parent.isOK = true;
          setTimeout(function () {
            _this.$parent.isOK = false;
            navigator.push({
              url: urlFront + router,
              animated: "true"
            });
          }, 1000);
        }
      } else if (this.$parent.id == "faceFind") {
        if (!this.$parent.pass) {
          if (!/[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(this.$parent.identity)) {
            this.$parent.tip = '身份证信息错误，请重新填写';
          } else {
            this.$parent.tip = '';
            this.$parent.pass = true;
            this.$parent.noteText = 60;
            this.$parent.color = '#8F9AAE';
            this.$parent.tip = '短信验证码错误，请核对或重新获取';
            var timer = setInterval(function () {
              _this.$parent.noteText--;
              if (_this.$parent.noteText == 0) {
                clearInterval(timer);
                _this.$parent.noteText = '重发';
                _this.$parent.color = '#4BA0FF';
              }
            }, 1000);
          }
        } else {
          if (this.open) {
            navigator.push({
              url: urlFront + 'prepareFace.js',
              animated: true
            });
          }
        }
      } else {
        if (this.open) {
          navigator.push({
            url: urlFront + router,
            animated: "true"
          });
        }
      }
    }
  }
};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["check"]
  }, [_vm._v(_vm._s(_vm.tips))]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump(_vm.router)
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v(_vm._s(_vm.text ? _vm.text : '下一步'))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(4)
)

/* script */
__vue_exports__ = __webpack_require__(5)

/* template */
var __vue_template__ = __webpack_require__(6)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdNext.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-86c0103e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ })

/******/ });