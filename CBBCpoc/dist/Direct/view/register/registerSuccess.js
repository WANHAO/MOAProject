// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 137);
/******/ })
/************************************************************************/
/******/ ({

/***/ 137:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(138)
)

/* script */
__vue_exports__ = __webpack_require__(139)

/* template */
var __vue_template__ = __webpack_require__(140)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\register\\registerSuccess.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-92cc8a12"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 138:
/***/ (function(module, exports) {

module.exports = {
  "empty": {
    "marginTop": "144"
  },
  "top-box": {
    "paddingTop": 0,
    "paddingRight": "128",
    "paddingBottom": 0,
    "paddingLeft": "128"
  },
  "box": {
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "left": {
    "flexDirection": "row"
  },
  "img": {
    "width": "68",
    "height": "68"
  },
  "title": {
    "marginLeft": "21",
    "lineHeight": "68",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium"
  },
  "state": {
    "lineHeight": "68",
    "fontSize": "26",
    "fontFamily": "PingFangSC-Regular"
  },
  "line": {
    "width": "3",
    "height": "42",
    "backgroundColor": "rgba(178,192,217,1)",
    "opacity": 0.3,
    "marginTop": "8",
    "marginRight": "33",
    "marginBottom": "8",
    "marginLeft": "33"
  },
  "center-box": {
    "width": "678",
    "backgroundColor": "#FAFAFB",
    "borderRadius": "6",
    "marginTop": "102",
    "marginLeft": "36"
  },
  "center-title": {
    "width": "508",
    "height": "74",
    "marginTop": "64",
    "marginLeft": "85",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "tip": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)",
    "marginLeft": "84",
    "marginTop": "26"
  },
  "prepare": {
    "marginTop": "46",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "prepare-img": {
    "width": "79",
    "height": "57"
  },
  "prepare-text": {
    "marginTop": "28",
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)"
  },
  "button": {
    "width": "528",
    "height": "88",
    "marginTop": "66",
    "backgroundImage": "linear-gradient(to right,#4BA0FF,#3A6BFF)",
    "borderRadius": "44",
    "marginLeft": "75",
    "alignItems": "center",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)"
  },
  "button-top": {
    "marginTop": "10",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  },
  "button-bottom": {
    "fontSize": "20",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(255,255,255,1)"
  },
  "card": {
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)",
    "lineHeight": "88"
  },
  "disopen": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)",
    "marginTop": "48",
    "marginRight": "280",
    "marginBottom": "48",
    "marginLeft": "280"
  },
  "register-bottom": {
    "justifyContent": "center",
    "flexDirection": "row",
    "marginTop": "124",
    "marginBottom": "40"
  },
  "bottom-logo": {
    "width": "21",
    "height": "30"
  },
  "bottom-text": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(203,205,215,1)",
    "marginLeft": "11"
  }
}

/***/ }),

/***/ 139:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
exports.default = {
  data: function data() {
    return {
      list: [{ id: 0, title: '手机号注册', state: '已完成', img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAiCAYAAAA6RwvCAAACtklEQVRYR82Yv1JTQRTGvy/czaXIwEhmSAqHQuyUQV/AP50O+AQyNNIw2mmrjlDb6djYgL4BMNj55wFEBu3UgrEgjMGBuQW7e3OPs5cESSTJJSQk2949Z3979ux3z1niBCMIgnw6nZ4AUtcAGSMxIoJB54LErgg2AW4A0SdjzEomk9lK6p5JJlprbwB8KCK3AfQlsQFQIrkKyHOl1IdmNg1BROSCMeELEg6g5UFi1fO8ByR/1nNSF0RrPUWmXgHItExQbRiIRLO+7789zt+xINbaZyJ47I6+TRAVN0JiXin1tNbvfwuVIZ60GaDKHYm5WpgqkDAM70aRvOlAJGr3JSLR9NFjOgRxiWltuN7GnGgW1EApb7ySwIcgWttVErfqWYelEorFPYhIswXi7ySRzQ7A66t/20XwzvdVfCNjEKcTInjfaIXC9h9sFYqJICqT8rkscsPnGtqQuOl0pgwSLonIZCOLrcIOCts7GB+7mAhmfeM7csNDyOeGmoEsK6Xu8EC2/V/NFLNTIE6BjdHnaUx4D5DXzbbZQRCXITPU2iyQnO4uiCzSGPsZwNXugmCN1trfIsh2E4RE0UXEAvC6CQIg7B2QXjqaEyWrE6kkw4lfEkEr+1qjMWYBSH59k0BU5iQHia/v6QRtdy+I1x0cqC7kkkq8s02lOHNqid/49iMGGbs0WhWsE4AcSLyz1touk5ho5ad32oiIYMX31WRvlQHlqHS/MHIg+/v7o6lU35ezLBWjqHSlv78/TrKq4lnrcIqUxa4Wz5VE1VrPkSnX03RsiGDe91VVy9K7Dda/yMTH1OaWk7O+7yVvOSswrtcJw/ClSP02I8n5ubYhnfbut9SEH13AtRtRhEflvufsnyVqdxsEkk+nSxMi0XWSl0mOiEj5oYa7IrIpIl9JfDRGrWQyTPxQ8xcOesDqOcSLagAAAABJRU5ErkJggg==' }, { id: 1, title: '实名认证', state: '待完成', img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAiCAYAAAA6RwvCAAAHGklEQVRYR61YW3BUVRZd+9zbjyQ2CRWgIcEQUIIDQtVA/HEG5PGDMlrKR4Yayl+1+J+XM86HM5bz+PTHmk/FqSFVlg5Flf6YZERTanoiEBkFMpCnIYh5IJl00n3OntrnvjudkKmyU+nq2/eec9ZZe+2192nCKl/d3d3ulG5uSbl0VCk86jrqYWZuAVDHdg6eU8CIMfiCCL0a/H5deXzk0KFD5dUsQfd6qLOzM51q3H3QcdTzzDiiHMqlHFJANJQ9JAImeDcw+A7EXVD0+obMbHd7e3tppbVWBHK269oukPkDgMdByMjDrutAKW/NcH25rAQTXDMWmPCeQ/jt0R9vv7wcmKpAOjvZyW64+jNm/IUI+WBw2lWgGIo4EMvHcmA83JMAfj438eDfOjpIVwJaAqSz83K6Ju++yAa/EhaCASlXwbEgYgGoYCUCE0GMwFmgCyD8MV/z4Cvt7ZQIVQKIMJHJX32JGC8CSIUgHAXH8WXhrxGq4R4hSjJlZywx6OW5iQdejTOTAHK258qzAP4KIBuAcBxCynWsNCuYj6S5Wr1E8Sgy8Nyx/dvfDL4Kgbz74bWdSpsuxDShFCGTcgCiUADfFxjRDHH68BMHtvxbwFgghUIh9fXd3N8BPBN8J2tn0y5IPgRs+AH/nsAwg97J18yckNS2q7zTdeWIq3COYyHJph2rC2aGitmGXCdcI5Y69mMFWLk0xkRjkqlW1Gx+8uSBHR+QOOZ31HwG4ONhmqYcqwtmg/Vra7Fj63oon5m4ca1kUME9YxgDg9/gm6k5G+FK8TLj7T49doLOnb+6TZe5nwj18pCkaVp0AbJsbG1uwJameoxMzMJUcbDEBiuNhWDHXx+bweDIlB9mP9+C7GPMaNZ76WzPlVMMvEYMJRmSzUS6kN3IRPdvXIOPPh+B1mHShlurBCLXwVNSCQ4+sgVDX89icHQKFJaFCIxEDoZP0T+6vzpNRCeFttpMCqSijI6A1OPjCyOQa2M1I1kE+zmhl8jtLBjxv8f2RUBkXDX3ZcJpYeQigD01WReumFbMEyQ0rcJIXoCMQmttw7Zn+0YL4tLVmyiVGfIn8INNGM2eyB2Fx/a1YDhgJMjAylJgMCBAbmfSTmM65UaW5YNZCsRgXUMNdrflLeALX01g6k7R7r61qQGtTVZmuDE+g6HxGZt1B6oASQrWBvNbev+jwflM2smKXyQU4FMfMNJ7YRRlbayY9/5gk91x/5fCiFe/6nNZrM15pUnATd8pwlUKB9ojRmxoYqkWC1ORuvtuzBMotPREDWHGFj80vRdGbAuwbXMD1jfUWre9Pf1fmw3zi2WszWXRkJNpvBkkw8Ym72D/Xh/I2DT87qGapxSpuzB0m4DGONQADBtPI5vz9ei9OIqHWtch31iXsI+bt+9iYPAWtjZJmjf499iy9+mlcfzoh/dbjfxnbNrzkep16VvqKdy4CNCeyMcjy7Iaaaq3QD65NIZ9OzdZo4u/JDSfDoxBzFNqUxh/n/f9vkauj017NctHkggRMED/LAydZuDk0jz00lOAeFkz4pWhSndkhrYWHmsdfTQCzIZmYhbXR4WRmA5jzBDoNPUUhk4R8BrDhjCiw5YNtnRvzucgYtUJa413ZJ6BJJog8REiL2sEiITGB1vRy8h+T9H5vuFtmkw/QF7uxcAEQFo2rcHozdlwoZVsPX5POJLyIKnsacQzQg+2/24wA13ea4ueWvPAGbA5XlkqRKzrGmrR1tpoJ4l3UZXPJnYQe1K2++V1r+hFVc9j07ARt367T4+fsHN/2D98hA2fk85syQLSNZCKdWhVbH3lLh6aTYIJYbpcNqKtBa3o2DPSBgiQQoFTdzF8hoCnRXXLUZ+AELivL9O4PkLiYxNZ/2S2vUmpbIQRGfJu831tP5VGOmS767NruxxKfRC0iqsBE+xMrNxmRJVBwXfy7GJZhxUcoEkYOvLUYe+sk2iez/cPP2sMh83z0jAlPaZU0jaTpKVcktaBHCUMmm0pCEARUNQGzz99eMcbgbaSxwlmZ+O/hn7HoF8Hx4lqzMjuZOLigkYm4yAt3Vw84fwLLwzaAom9SoB5tXjroZeXPU7Iw3LA2rDtvt8A5pcEslWsEoxQXFzwzta1Wdc781S0D2Lxi4saCavzDlh/Kt4qv9LRsWsxjm7ZI2d+642TDPXn4MgZgBGhzRe9Q5oMrqtNh84t34nLLpQ8FhKTMyaNwi8WJ9veWtWRM46yu2/wYYfc3xvgcQIyAQgvA7z+tiYj+vD6W9GMgJB7Ya/tsfBemfVLxw/t/CJhmrGLVfwscTmdb8sdLC/oF+bnFw9rIEeAksUyacf+S0dWLJVDFmwfyvKzBLqI1OvFyc97Ojo6EqGoBHRPIMEAceA5t7nFaDrKzI+S4t212VSz0VwnLBjmOSKMs6EBIupVzv/3Q83/AAKbDxoTum+6AAAAAElFTkSuQmCC' }, { id: 2, title: '银行卡绑定', state: '待完成', img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAiCAYAAAA6RwvCAAAGuElEQVRYR61YWWxUVRj+/nPvnYXaTg3KaKtlUdCI+oB9MooCL7gGeahEw6sY3t2XB5e4PfJCfFQ00sSohERf6FRRYmRkCaBCa+lGsQrtFKwdZu49v/nPXebO1tLoTZpM79x7/u983/cvZwhXeeVyOXvK6+xybNpMxPfalnUnwF0MtAAsq8wSMMoaJ4lwyAN/0+KeG92wYYN7NSFooYd6e3sTztK7HrQstYMZm0ih1VaklFLmVTYYzKfofwY0MS6DuA+Kdi9LzuS6u7vL88WaF8i+voG1IP0WgIdASMpClqVgKwrCBhAagImCMq4w4WuL8Orm+1afagamIZDeXrZSy848xYwPiJANX7YUwbYsgNgQEMVvwkw8KAOTAJ6bPX/rpz095NUCqgPS23sqkc7aL7PGiyEL8pJSBMdWIIRsLA6MAc24AsK72fStb3d3U5VUVUCEiWT2zGvEeBmAE6JWBDiOBSKKaAgcsShmAgbLAL8xe371O3FmqoDs6z+9HcCHAFIhCHkgkbCgKOaLYMX/AKYI4JlH7l/9cTyO+fzldwN3KE/3IeYJuZ90LChLRelR48swVxbNjHhGcWLjw+uX/yJxDCP5fN6Z+Lv1MwBPhPfkfsKxYAuIMDmDXP0/wPj+pi+y6cI2SW0D5Iu+05tshf0ck0QACBDZqiJlIPuGiwSJl495mdFaV7I5nmpA0WP96GPrbztAUjEvU+degLeGW7csQjJhGxDXXbsEt6+83nikKvJ81Sn2ndaME4N/4a+pWYRLVIqg2dfnh73xbbT/4JlVnstHiJAxWhGQTjomQ5gZKzszWN7RjpGJmaq6UcdMpF8MBQErO9sxNFbAwNhUsJlKBQ4ILnjsraN9/ad3MrCLAGOGdNI21VNQ+0DacfMNbTh0bAyejslSlTlRha+SSGrP+nu6MDwxg8HRKT/9Y9sxMQANzTvpq9xve4jo6TBDpF6El9AqQOTP9UTn2vpXLXgjtcRrg2PTGBybMsUwLm8oERP2CCPHAdwtVTOZsGKVE9DCSEc7lndkMHp+JmpwdeGbGFgYWNGZwdB4wQAxtahRX2KcoK9ypy/YFi1Np8QXPnPhsyLNCpEm24bvj47BM6zEO+78mSQSP9DdZfxlGAncWgsG4Iu0/9szc+mUnQrbeohEwFSAZPDD0VG0tiRxbZtfdBdiZfpSETOXrzQEUr0Zs1KRDvw4NGdZyqxeWTxwUcRIBj8cG8XN2Qy6bjTJFTzdqHn7q5ydKGB04lIFyPi0yYYmHbtIufzwBQKWxp8Ic4O1L81N2QwOHR8FmEwXbmQ4uSdZJSzKJUYXKSRrRJrfx6frpI8xc5H682ePA3R3nO+KRzRWdPhA6hmpz5GRiYJhIoRKkr7rfCBD49N+kQpMWMPMCfo2P7yHAZO+tawYj3RkjCQCRDzSHnik1iSy8PSlORQuFf2AzIa9+wXI+RkMjQkjjWcZAu2h/vzwTgC7ZPapBSPp6zPSagqa68VcVOVWDvpQsOlgT5KuRhoBItIEXMXHBy0FjbGTDh4eWeWRPgJQ6MLK8MNsynvXjW0Y+8OvIzXx6/SJfy9CSA0aPlcIPNJgsNIowHPXmaan2m7ZC9Zba4OINNe1L8GaFUsNrfEcaVxT5W51Jgmrvw75Ta/S9Xz88h3AnydL49vMW98dGdnEmvfLZFa/Y3G/r1pNglfYiJhqIB0DHuuaEdPPKtf1ZMJ/xIwBslo+z87fGNlLwBbZUjP6qyaRRUzxlarup7ekuesxa+YvO69Z86QM0hGPfT8NrLXIORCOiguCCR7wZM4y40OTPhLjUgaksqsNGwBNQtOmxzf6Z50qQQ8eGdmuNUfD80JgZOFSSZvhulkfCccJ6d4CIgha9DR2bNl420ehvtXHCWbrhp+HX2fQS+FxohkYT2vMFV1YNiHl2A1Lt8igWaNUDlkwYcuAfqf45+1vND1OyFNywFq2askrAF4gkDlm1oIRt88Vy/A8RiplwbGsuiYo9JddL2Ih8LocsN4r/um+3dOzthTP/aZHzuzKs08z1PvhkTM+GvxTLJviJi+3pBMw5/HAvKEZS2Uv8EJkkkmt8Hxpcs0nV3XkjKPMHR680yL7TS2HcCApQUQOV7SW2VIRWlJO1EOEBQEgXogqin/M/Npl77WtG+44Wd+h/DtX8bPEqUR2VeuDDP3sXNHdWHK9VsEgJkw4ygzaAlDMWCx5QZEyZfsyCH1Eandx8mh/T09PlRS1gBYEEr6Qy7E9aw90aY82M/O9pPiudNLptJVquVJ2hYlZEM6xphNEdEhZi/uh5l/gmdNItm4jQQAAAABJRU5ErkJggg==' }],
      prepare: [{ show: true, text: '身份证', img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAdCAYAAADYSS5zAAAEmUlEQVRYR72Wu48bVRTGz3dn7M3au/a+swkUKCBQWiQkGlokikhIVDT8D1SUgERHhdIghIQiUUDJPxAJiQ6BhBApEgkRObvZ7CM7L4/H9tzzoTtjO96NvQ+wM8287pz7u9/5zrkDGTtaLS42m713VfUWwZsQqYrwSIA/K565W6st/AwgHf9m3tcYTrC/n71WXeAXInwLgj6FbYqkELRJpgaMRcwDVfl+dXXxn3mDDeMXgEGQvUroHaG8DDAVQQciqYMUIkEBh5hkQuLQWvlxY6O28yIgQfJKGHW+E5G3RdhxcKVybAsQg0xUEEEYCxHDMzGt7jebiz8B6M8bElGU3bKqXwHMhMgIaQ/hnGIiiEQYgYiLM0ys0LYn9l6j0bg/d8AgaH8rwDtCaRMD5YiYYGwMIlEGqog8D1FOJj4YAybNMg02N+u/zx3wOEx/gUh9UBCJOJ+VngtVNbdW1wB0RZBaaubB61j2+0pkSj6y1s6FETQ5rP0VQZg6FTJxVUskBoxcWknGaZa/n6u+AQhHVTV2TXn2fNaUEAEFv+E4SO/CgK4IlBKDEouRUJVx0ul9ApGanAQZwZ56PmtGF54Iw/S2CG5QGAkKz4XiIaQySbLeZ6Ksj808Ducen76fMSSJIEg/ECMfOjA6QGHo/AciijvdL0W4NJj1BcMV6yf2yaVK1PlcRBaAAi4QZQQfcZz0bpNcnqBUAdvtdhFEiaFSlpdrWq/VZqwoWewkh3H3pm/tRyx3kdAIAlUm7W7vG5KNSYBZ1sWTg6c+jBFTDuBSfVFXVxo6uzwPAF3AKMpeJ/U9GmZiEQBMorR/R6RQcPygo9nZO/CttQDK7dydIcJr25u5MaMt/n+yjgG6SO5vpt6M3xTxVo0yizv9H0RkHLBIoaqVR7tHvogWgAVOAUpe3VrPK74/EcyYQusT9KSQnOaMU4DDqCS9MAwbcca/hLJ2ejYXcOfxga9aAg4ULGa5vr2eG+M9B+jGXd9qVEcfDEb0c6tPDuMpe/oUQPftH3t79TVW74tgfZIcUdI2x8eR51QZQLJa8bm1uTZ1a1lfXapU/JP5Tzs9GyaZnWyK8wClel/4PKBT0KrK4VHg9Xq5cSIC4OZ601arVQ5VnaT884tF6Y6Jx38A7GQZjo5jz+Zu0ZTSV2WROA96xpPa4oI2Gksu/yemtUqh6sC0E4goLh6fFdklAZMkxcHTwC8Lo1x5cV2CsDyVUAtVn+trK67Mi3v3U/Hg71Y1LxY2TTLK4pUFvfHKS/3hgqeKW3hwLMXW5tLa2a+4SihyOg44qGKX5rGKluZyXev1WtEXnS0eth5Xsm7/zB7UWKrpte2N/NKAYZSYo6ehB1MqdkLBEaDrIYWshbouXVsba3a6x6Z571k/ubCCe/sHXpr2zETAUYrHmvYg3Vc3Vya2nfPQyveX8ODD1l6xcziBzkzxAGzox62Nldyf0rjPh7wEYLudYtjvBzqNvH76fpDxQbFUOKlxnw93SQUvFnDWoy6h4Kynvli8MwB3d3drisV7FNm+WLB5jOL0nkTStPaCj43IpxRW5jH9WTEB5CS+/hc6VbV5e71LUQAAAABJRU5ErkJggg==' }, { show: true, text: '银行卡', img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAdCAYAAADYSS5zAAAEmUlEQVRYR72Wu48bVRTGz3dn7M3au/a+swkUKCBQWiQkGlokikhIVDT8D1SUgERHhdIghIQiUUDJPxAJiQ6BhBApEgkRObvZ7CM7L4/H9tzzoTtjO96NvQ+wM8287pz7u9/5zrkDGTtaLS42m713VfUWwZsQqYrwSIA/K565W6st/AwgHf9m3tcYTrC/n71WXeAXInwLgj6FbYqkELRJpgaMRcwDVfl+dXXxn3mDDeMXgEGQvUroHaG8DDAVQQciqYMUIkEBh5hkQuLQWvlxY6O28yIgQfJKGHW+E5G3RdhxcKVybAsQg0xUEEEYCxHDMzGt7jebiz8B6M8bElGU3bKqXwHMhMgIaQ/hnGIiiEQYgYiLM0ys0LYn9l6j0bg/d8AgaH8rwDtCaRMD5YiYYGwMIlEGqog8D1FOJj4YAybNMg02N+u/zx3wOEx/gUh9UBCJOJ+VngtVNbdW1wB0RZBaaubB61j2+0pkSj6y1s6FETQ5rP0VQZg6FTJxVUskBoxcWknGaZa/n6u+AQhHVTV2TXn2fNaUEAEFv+E4SO/CgK4IlBKDEouRUJVx0ul9ApGanAQZwZ56PmtGF54Iw/S2CG5QGAkKz4XiIaQySbLeZ6Ksj808Ducen76fMSSJIEg/ECMfOjA6QGHo/AciijvdL0W4NJj1BcMV6yf2yaVK1PlcRBaAAi4QZQQfcZz0bpNcnqBUAdvtdhFEiaFSlpdrWq/VZqwoWewkh3H3pm/tRyx3kdAIAlUm7W7vG5KNSYBZ1sWTg6c+jBFTDuBSfVFXVxo6uzwPAF3AKMpeJ/U9GmZiEQBMorR/R6RQcPygo9nZO/CttQDK7dydIcJr25u5MaMt/n+yjgG6SO5vpt6M3xTxVo0yizv9H0RkHLBIoaqVR7tHvogWgAVOAUpe3VrPK74/EcyYQusT9KSQnOaMU4DDqCS9MAwbcca/hLJ2ejYXcOfxga9aAg4ULGa5vr2eG+M9B+jGXd9qVEcfDEb0c6tPDuMpe/oUQPftH3t79TVW74tgfZIcUdI2x8eR51QZQLJa8bm1uTZ1a1lfXapU/JP5Tzs9GyaZnWyK8wClel/4PKBT0KrK4VHg9Xq5cSIC4OZ601arVQ5VnaT884tF6Y6Jx38A7GQZjo5jz+Zu0ZTSV2WROA96xpPa4oI2Gksu/yemtUqh6sC0E4goLh6fFdklAZMkxcHTwC8Lo1x5cV2CsDyVUAtVn+trK67Mi3v3U/Hg71Y1LxY2TTLK4pUFvfHKS/3hgqeKW3hwLMXW5tLa2a+4SihyOg44qGKX5rGKluZyXev1WtEXnS0eth5Xsm7/zB7UWKrpte2N/NKAYZSYo6ehB1MqdkLBEaDrIYWshbouXVsba3a6x6Z571k/ubCCe/sHXpr2zETAUYrHmvYg3Vc3Vya2nfPQyveX8ODD1l6xcziBzkzxAGzox62Nldyf0rjPh7wEYLudYtjvBzqNvH76fpDxQbFUOKlxnw93SQUvFnDWoy6h4Kynvli8MwB3d3drisV7FNm+WLB5jOL0nkTStPaCj43IpxRW5jH9WTEB5CS+/hc6VbV5e71LUQAAAABJRU5ErkJggg==' }, { show: true, text: '网络畅通', img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAgCAYAAAAMq2gFAAAD0klEQVRIS62WTW8bVRSGz7n3jmds1+MkTtwAsSwnqUyJ1IoKqu5YVqq6QUIRCEFZVQLxI7LjN7Bkg5CQuusOkQIhQqCKSiVqyjhxiZt+OI2dxvXHjOfOQeNkkrE9bvwRr3yvr+eZc877nnMRXvNZWlpi16/f1BIJoTuOCLlHGcP9TGZ873X/C/oNgzZdwI0bXyUl0iwQ6oDS5IR1G5mJNlTm5qY2EJEGgXWBVlcL4elp9X0JlCaCGuNURtncIS2yB/WyqapqJZVK1QeBuGfbQOvrOzEh5DXkOI4MX0hLPtC06c2ZGWgcPtg9T4NG0wbK50mzqfgRQ0xIhwpky1+z2Td3DYNCiM/jnFNESi5I4435tyaeIKIzSFStiIgINx7tXAVGF8BhW7a5e/uHbLb6yb+P3+CaNu04JAnsBhfCRm5bd1dWthcXF+XAoFxuO0Vc+RQc2rcY3vrxu6nix58/OaeQMuVwrHFGUcb5OUDHVBiXspVy3heHEUlGzh/oKuyzL778kCHMSYDf59PJ1Qf5Z2kFIYNMVEwHtnWN3wLAK309OeAQIvyNhmHoyPWbhFBljvJ9KFS1TCmuADJL4811xnSUTBoIMDYsCABsNP7bXUBpXyOg9fnM2du5QjEjLUorGn+8ZfyzOTN7IaupbBUI9GqtBlL2rwHOGUQjYUBEG4380w8A8BLjYmU2lbi7sfXiXbQpXK2G1jivYCiqpjVNueOCXu5XfEH5neF519tz1wff43rUAz2/Co4zJ8LKz48e3s+lUu9cZsyx6/XSPR5LTjJJyXBY+aUb5DFb1uq05NELHYEebj69zIBllRj76c/lyeLFi88uYYg37/115/758+9NikgkeSaq/gYE8b39SrvDWysPEgQjiOsxQAQbXQ8BgIKIlvsaa2trrea5sLDQWhcKhQlQohtAONaeul6t0w92U3cIOklJ5TKNVa2Xm+TQeHBEQU84hsX1Mwc16gf0ytzLA0FHRF7avE7WmbqD9VGNhgf5xdBqZIeC8AN9NRoe1CnlXmI4ldT5X/MUQe1i6CxvsJeGqtExyJ82vxi6TXxKoJMq7GtBJx11fdQt715C6E7fgY9gVB/5ZR0s8RFT16v9dO67PhpC3q+q7jzyrgq9mspx+oQ7j6KH86jfGnkTlrqujf6N9hmFeLTuv0a9RrkiOETCIag1LGg2e16MRgedTcRACA5NW0Jx1z+B23I1Oig5EQNF4a1oiqURQKVSKV6zMAcAk0H1dAuuqgJM0wa718WFqHHiPFpeJjH/dvkbRPY1YPCtkYjc4RasKwIJQN/+D/7cGkGybtclAAAAAElFTkSuQmCC' }]
    };
  },

  methods: {
    jump: function jump(router) {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + router,
        animated: true
      });
    }
  }
};

/***/ }),

/***/ 140:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroller', {
    staticClass: ["container"]
  }, [_c('div', {
    staticClass: ["empty"]
  }), _vm._l((_vm.list), function(box) {
    return _c('div', {
      staticClass: ["top-box"]
    }, [_c('div', {
      staticClass: ["box"]
    }, [_c('div', {
      staticClass: ["left"]
    }, [_c('image', {
      staticClass: ["img"],
      attrs: {
        "src": box.img
      }
    }), _c('text', {
      staticClass: ["title"],
      style: {
        color: box.state == '已完成' ? '#CBCDD7' : '#44464F'
      }
    }, [_vm._v(_vm._s(box.title))])]), _c('text', {
      staticClass: ["state"],
      style: {
        color: box.state == '已完成' ? '#CBCDD7' : '#8F9AAE'
      }
    }, [_vm._v(_vm._s(box.state))])]), (box.id != 2) ? _c('div', {
      staticClass: ["line"]
    }) : _vm._e()])
  }), _c('div', {
    staticClass: ["center-box"]
  }, [_c('text', {
    staticClass: ["center-title"]
  }, [_vm._v(_vm._s(_vm.list[1].state == '待完成' ? '为保障您的资金安全，接下来将为您开通宝石山电子账户' : '绑定银行卡以完成宝石山电子账户开通'))]), _c('text', {
    staticClass: ["tip"]
  }, [_vm._v("请做好以下准备")]), _c('div', {
    staticClass: ["prepare"],
    style: {
      paddingLeft: _vm.list[1].state == '待完成' ? '88px' : '177px',
      paddingRight: _vm.list[1].state == '待完成' ? '88px' : '177px'
    }
  }, _vm._l((_vm.prepare), function(pre) {
    return (pre.show) ? _c('div', {
      staticClass: ["prepare-box"]
    }, [_c('image', {
      staticClass: ["prepare-img"],
      attrs: {
        "src": pre.img
      }
    }), _c('text', {
      staticClass: ["prepare-text"]
    }, [_vm._v(_vm._s(pre.text))])]) : _vm._e()
  })), (_vm.list[1].state == '待完成') ? _c('div', {
    staticClass: ["button"],
    on: {
      "click": function($event) {
        _vm.jump('photoIdCard.js')
      }
    }
  }, [_c('text', {
    staticClass: ["button-top"]
  }, [_vm._v("开通电子账户")]), _c('text', {
    staticClass: ["button-bottom"]
  }, [_vm._v("实名认证/银行卡绑定")])]) : _c('div', {
    staticClass: ["button"]
  }, [_c('text', {
    staticClass: ["card"]
  }, [_vm._v("绑定银行卡")])]), _c('text', {
    staticClass: ["disopen"]
  }, [_vm._v("暂不开户")])]), _vm._m(0)], 2)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["register-bottom"]
  }, [_c('image', {
    staticClass: ["bottom-logo"],
    attrs: {
      "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAPCAYAAAAyPTUwAAAB6klEQVQoU32RPWtTcRjFz/nnxiRNKVahg4MvoG2aWFNtaiyIoA6Ck4ij30DwAzhaRAVHFYpTHdqlWAeRDiIqcrU3vXExL2JRFESsLtpK0t7e50gKAQfxzM/h/M5zWAnr8yZM92aGHxcK3MB/xKBSf0PHfhDPRXd7fHSwSjL+l4dB2HhFYgcMAhEBmOtJ2/V8Ph+R1N8mBmH9BcF+ABGJl4p5h0ntUazROEpPl8v7VromVsLGM5A/nXRX+v20VCpFQbV2mnJXBXwjOdOTihcKhcIag7B20clbKJWGfnQjF6uNCUqTpNuAWQLkeznMsBLWrmwiOX/syIFmN65areVjuRsAJKhN4AvBvVxcqj8kkEjQe5DN2lwul1v1/bf7k+nEpEx10q0CyoE412GehdS3VdC5T0zY7C/LNLPWPuXEvMF2kTwKIMdK2LwvWIpw66RWKAsib5vvRVERjiMyDIO60OnDYKl5j842nfAOCa8FxUMwPhLtZGxcdsQEwPOAZTsY10i2JOsDXBKydTjvCWRlSoMiz0L6DGInX4fNQ0ngTCwNAGrBuc5yocXxiCMvAxgQ8FGmm+yw+L6fcantRY8cl+QJXAZwiUARwFRLmjpRyn/dOu6qVlvpXWt/P0xgN8XjRHxrbOzgh+7//wD0B+l1sq60ZwAAAABJRU5ErkJggg=="
    }
  }), _c('text', {
    staticClass: ["bottom-text"]
  }, [_vm._v("杭州银行宝石山")])])
}]}
module.exports.render._withStripped = true

/***/ })

/******/ });