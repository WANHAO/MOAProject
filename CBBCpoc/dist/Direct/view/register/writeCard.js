// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 145);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 145:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(146)
)

/* script */
__vue_exports__ = __webpack_require__(147)

/* template */
var __vue_template__ = __webpack_require__(148)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\register\\writeCard.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-80fdcb34"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 146:
/***/ (function(module, exports) {

module.exports = {
  "top-box": {
    "paddingTop": 0,
    "paddingRight": "36",
    "paddingBottom": 0,
    "paddingLeft": "36",
    "marginTop": "45",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "top-first": {
    "width": "68",
    "height": "68",
    "backgroundColor": "rgba(250,250,251,1)",
    "borderRadius": "34",
    "alignItems": "center"
  },
  "first-icon": {
    "width": "36",
    "height": "36",
    "marginTop": "16"
  },
  "line-did": {
    "width": "84",
    "height": "3",
    "backgroundColor": "rgba(203,205,215,1)",
    "opacity": 0.3,
    "marginTop": "33"
  },
  "confirm-box": {
    "width": "193",
    "height": "78",
    "flexDirection": "row",
    "position": "relative"
  },
  "bg-img": {
    "width": "193",
    "height": "78",
    "position": "absolute"
  },
  "second-icon": {
    "marginTop": "15",
    "width": "29",
    "height": "34"
  },
  "third-icon": {
    "marginTop": "15",
    "width": "36",
    "height": "36",
    "marginLeft": "15"
  },
  "confirm-text": {
    "marginTop": "15",
    "fontSize": "22",
    "marginLeft": "10",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(56,148,252,1)",
    "lineHeight": "36"
  },
  "line": {
    "width": "84",
    "height": "3",
    "backgroundColor": "rgba(56,148,252,1)",
    "opacity": 0.3,
    "marginTop": "33"
  },
  "top-item": {
    "width": "68",
    "height": "68",
    "backgroundColor": "rgba(235,244,254,1)",
    "borderRadius": "34",
    "alignItems": "center"
  },
  "fourth-icon": {
    "width": "26",
    "height": "35",
    "marginTop": "16"
  },
  "line-top": {
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginTop": "27",
    "marginRight": "36",
    "marginBottom": 0,
    "marginLeft": "36"
  },
  "box": {
    "width": "678",
    "height": "290",
    "backgroundColor": "rgba(255,255,255,1)",
    "marginLeft": "36",
    "borderRadius": "6",
    "boxShadow": "0px 0px 46px 0 rgba(143,169,212,0.5)"
  },
  "cardholder-box": {
    "marginTop": "48",
    "flexDirection": "row"
  },
  "cardholder": {
    "marginLeft": "37",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(143,154,174,1)"
  },
  "name": {
    "marginLeft": "25",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(143,154,174,1)"
  },
  "type-box": {
    "marginTop": "90",
    "flexDirection": "row"
  },
  "type-img": {
    "width": "28",
    "height": "28",
    "marginLeft": "37"
  },
  "type-text": {
    "marginLeft": "5",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)",
    "lineHeight": "28"
  },
  "card-box": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "paddingTop": 0,
    "paddingRight": "37",
    "paddingBottom": 0,
    "paddingLeft": "37"
  },
  "card-input": {
    "height": "58",
    "width": "400",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "camera": {
    "marginTop": "15",
    "width": "36",
    "height": "32"
  },
  "agree-box": {
    "marginTop": "48",
    "flexDirection": "row"
  },
  "check-input": {
    "marginTop": "2",
    "width": "26",
    "height": "26",
    "marginLeft": "73"
  },
  "agree-text": {
    "marginLeft": "21",
    "fontSize": "26",
    "fontFamily": "PingFangSC-Regular",
    "color": "#44464F"
  },
  "agreement": {
    "fontSize": "26",
    "fontFamily": "PingFangSC-Regular",
    "color": "#3894FC"
  },
  "support": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)",
    "marginTop": "88",
    "marginLeft": "37"
  },
  "bank-box": {
    "marginTop": "37"
  },
  "bank-line": {
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(229,230,235,1)",
    "marginLeft": "36"
  },
  "bank-item": {
    "flexDirection": "row",
    "marginTop": "36"
  },
  "bank-img": {
    "width": "52",
    "height": "52",
    "marginLeft": "40",
    "marginTop": "10"
  },
  "bank-mation": {
    "marginLeft": "39"
  },
  "bank-title": {
    "fontSize": "34",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "bank-subhead": {
    "fontSize": "22",
    "marginTop": "10",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)"
  }
}

/***/ }),

/***/ 147:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

var _pwdNext = __webpack_require__(7);

var _pwdNext2 = _interopRequireDefault(_pwdNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var navigator = weex.requireModule('navigator'); //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  components: {
    PwdHeader: _pwdHeader2.default,
    PwdNext: _pwdNext2.default
  },
  data: function data() {
    return {
      name: '张三',
      checked: false,
      cardValue: '',
      bgImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGEAAAAnCAYAAAAfBybSAAADwklEQVRoQ+2bTWsTURSGz5k008TpxE5ajN+tpRYUBbWIIKg/QHSlC12K/0HwF4hLdwrSnYu4UsSFK60rRa1g1aKo1eJH2iaTZjImnST3yJlkYtpGLWicC72zmsm9Oee973POncVwMZ2myLHj4nwE8CIRbAMABHV12gECgJlqlS49vKddw1mnegpRu9nprCp+eweIxGnMFMQtTYOTyqRwHBACbuOsI+4jwrFwJKisRPBAQQi5DnwIc464D6oTwkPR6IQHiHA0PBVrOzMRjPN2pCCEWAcKQojmB6kVBAVBAgckkKA6QUGQwAEJJPidkClUxzVNOyKBnjUpQQjxUEEIGb2CEDIATq8gKAgSOCCBBNUJCoIEDkggQXWCgiCBAxJIUJ2gIEjggAQSVCcoCBI4IIEE1QkKggQOSCDB7wT1oT9cEurLWrj++9kVBFkgzBXFbQA4IYGeNSmBhLiDn3OLZ3Q9emNNOiDBoj2vchavXn2y7uTp/eciUbyACFvUIZH/QoYPiXyuVejyrfTEdUynZ+Llck5zYl0YcyLY3R1BV9dQdwuo6xEsRSP+yZ0EJDqmrlQqYjzew8KaF//GD+Wo2zg5ZMLePX39O3ckHwH+RgxB4e2H3KEXk9l5AKcZL1YxaHmOji2oEbgABf8uXqmR59XIMxJkeIIWF2tUNmtklqsUiyUFjo19iBlGF7YDwQEYBkBvR/X2toT//t1pgm9NGhTBwEisf2i47zEArF8himDh3cz8wU+Ti1keC0xonVc3xPSBc958/s9LW+289pHywObzWDsArlslvHL3bbeZ6ULPy2rxeBad2Fa/8rgrXDeCySQAJJPgFvO/PMtm9PTSL8dtAGNb67gNANYSvUaPILeo+fEZOvuyFHv9SdfrgPbt2mINDCaeAraAIFj4OD03+vz1vN1qNMBKlwNT/mz/389g43M5AMOoVz9H3B4viS9feD19wklVCdPpl/q3Hh0ZBHeEbWeQYQBsBN6i6kDsf3aY0DQtcpbFM80RcpxcM0cAn8UvL4IA0uEDm6yhoQ0TPgiChffvZ/e/evaVCTcrL6g+jhNcbAbAfOOxH37e/87w1c5bGaNsWr7xvPVYVoXYfMtKEXcAA9hY9IhfzFHLiiODyGW6cAAABgcH4c2bp1oQcvPmv6+IIAKLWB5vaiqLqVQwg+EzkA3+D9yRgVGum2qCYjg7dvdbwzsT4zOfFo5MTWTzS83ONN8xgRGtZvC9bWfRsvqWvIvarXS189r9l9cbXCMjo2J6eho+8ubSAGDbJfoBFwA9rW15iqIAAAAASUVORK5CYII=',
      firstIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABKElEQVQ4T7WTz0rDQBDGv9E2LRUKvYivkIIUmqwUPKhXRZCiD+FTWF/Cl9AeBA8eKyUQGreilwXfwHtbsDYZ2RDtHxOhabqwsDvM/Ha+mR1CRosy4oB6Up1uEG6ZUUgDJcJnMPEvyJPqCkAr2mlYYewvSFhmKpmeVLxeULf7VimWcg6DxoVcZb9W2xn+pzcxo+eX9xMO/IcwOMCREGZHH2dqqa8tYZnXkT1emuu65c18WYLJJwzrtm2PdECvrw6IcajPTOjs1c2nRJDjvG4bReOGGcdEAIMfgy//stHY/ZgF/UjVWcVK86RqA2jO1YT5XtjVswVpoYvudBJoAGBrvrg8ElZ1wTb1WH/7l/3afzJaeUT00BJwB8BYNpvIf8zAear5inswM9A3Zsmvq6vYLfYAAAAASUVORK5CYII=',
      secondIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAACBUlEQVQ4T42TT2sTURTFz5lpVXBXxH26EDNgwEwmG8USVAiuFBQr0orS7+BScOXOL6BoC5FSwVAXtfhn11XeTFY6Tyq4Ei2IVI1UJckceVMrKY2QCzOPeff97j3vzr3EXzPmzXF43kOSx5QbRCKT0HdHSPQAyD0E+pnwlM5hzOsT9P1VCR2IC6BuCkgJNSFvbBuWD4LK3Jvn8yCtlp2irxWAG+O+d6rX6/wQD34VuFQNj17eUTa4msQuCyixlaTrBA8j45y3z/+gbm9SREPErWq5eHs4nDYBhDSJdffYZSTedb5tBLVazd1zj7US+4RE5OAM0jN4uJuJPpBtVcvBGsk9Qf8VN7aPQZykSdIuhPmoEswNyzJUdmyXREy5zL8BNKKweMOY9Ul4/VlkALwBbOc78xei6Mh7k6SLAE87+CeBxUpYvG7a6RmIL/6rgDoblYOXJraPQNUZt21HUjMKg9lRZbdi2yB1zmX+Dmg5CoOZUWGTvF0AdMEVbFPkSrVcvDpM9v7x7kSpVNrc1SSxnQd1ka3YfiHxPAqLV4YV7NDEgTuFQuHXIBwn9oGAaSf7M6BXURhMjy47vQ9wxsGfIKxFleKlkeE4vQfymoM/AtryPH8VyiSxD/ejybw1JfUhz02oSG2PJ1hX3tttOy+hTreXjy38HALyUQQwJoFk7s99bo9A+gc+gwHbCwsOYQAAAABJRU5ErkJggg==',
      third: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAn0lEQVQ4T2NkoBJgpJI5DIxmU/6rMf7/E87IwMBMjqH/GRj+/mdkWcloMfm3AwMDAwgfIMcgmF64QSdyWRvIMchi8m+QvgPILiLHHJgeFIMGmddAYWTR/12BgYVFgSg//vnz4EQh5wOMMAIbhIhBYsw6cCKX9QBWg4jRja6GXBeBXYFsGHaDCIURNFwIGkQVr1GcRaiWacnxDjY9VCuPAJU+bzlXBKIWAAAAAElFTkSuQmCC',
      fourth: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABOUlEQVQ4T2NkoBJgxGWO4ZTv8mz/mJ0ZGBmlwWr+/3/6i+nv3vM5nA+x6cFqENiQ/yxxjIxMH//++3cNpJGZiUnr//9//L8Y/yzCZhhWg8wn/UpiZGDgecPwcOadPNWfIIO0V/1n433xO+M/A8OXk3ls89Bdhd2gyb9r//1nOH46j3UPsgbTSb9dmBgZLE/msjYTZZDF5N8NDAwMB07ksh5A1mAx+bcDAwODw4lcVpA8CoC7yGzKfzXG/3/CGRkYmImJyP8MDH//M7KsPJXDeAukHm4QzDaQS4gxCOQyZFdjGITN2dgMRvc+/QwChR3IRbCwgLmOZBeZTfpdBTYoj7UNLQZRYpag10wm/FAHGXCmgOMmRQbhikGSvUa2QRSnI1BYMDMzh5GSsv/+/bsKFnY4yyMiUzdcGQDwx6kTtXj9QAAAAABJRU5ErkJggg==',
      camera: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAARCAYAAADQWvz5AAAB/klEQVQ4T52SMWhTURSGv/OSSEnE0UEsVikFlyo46CROxSS1Q6V1aZtnoQ6C0A6Ci5o6iUsLgoNF85LqYIsdapOIk7gpOKigUApWqktHsVVs8n55xcBrSEP0Tpfz/+fjnHt/o8FJ5XUTMWGGhWUJYUyVMjZZ37bDGIjJRzroVFhB3Jd4H24woxvjkh+lszxkX3do2ayct4foqkJbIBiMY5zeWKXrZdYqYfOZrKKJDpYRrwTTgRaBXye+sGwpT3mDkR2j+rjFUcs3WjudVwbh1a08a2lPPjAWjzNbE+cH7XcjSK02MKc9tfvmJsPATAASDueLI7YQbh6Y096NTcYMrgR1wd1EnJn5QfuRyum4GQsS/RbhCD5PG4J6CkrEfD4IfgKTJhwZ1w3athy6Y1VGzZiWGCfCt11BaU/3DNqXXDsXnrLX0zPBWtG1y8HDB5+RLqh/V1DK07pj9C5l7E0YlMzplBmLJdf21+pNQWlPlS2fwy9GbS0M6nmo9pjD56Jr0ZZAKU/vBHfKrj0Og9J5DUlcLbl2rCVQ0tOIwVQ0wsnFYVsJmvpm1Vmp8lowUXat0BIIyVJ5rhncALZBQKfgVinDbczUGuiv62xOHeZwdDtHPp+eX7TV+qA2fexmqW4G+gjsM1j/F0DNKwii8N36HuhAJcIFIPE/IGAjWuXJH2zoB3JFrkclAAAAAElFTkSuQmCC',
      checkedIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAYAAACMo1E1AAADeklEQVRYR+2YWUiUURTHf9cxU8mtjULDaJGIEKI0SttTW4igp4J2inpohXoIKnqKIqiol6Ky0peI6kFarQczkbI0yqIFWsSKqDC1NEtnbpy+BseZz/m+GXWch+7rd+45P//3LM5R/D1azctnoCOCXA1rgUwg1vgWgqNoQvNAwSmni+Lrq/mKUloJ2MJ8xrsc7ANy0agQ4JiHUGg0t4GdV1epSjUvXw+KcFCIJgeI6DWw9sAuFLdcTparBef0MjQF0IuK+Soi77dCzT+jS5Riehgo1gFBa+6oBWd1U0iT374KzQKn7duH1vI/XLB6h0S5CAUJ0dAvCpwavv2En63WyD0OJ2ATkmFJOowcAC1tUFELZyoNSH+nx+GSE2D3TEhJaMeQErzyAk5U9CJcfDTsnQ1pA30hahtgUxG0uToH7DHlEmNgQyZkDzcP/uQT7CoGl59G1iNwfRywNB0Wj4NIk2ktRSFg1Z+66VklsZUCp59nkFBik50KW7IgOtI3+K82OP0Qrr3spmqVnJmYDLFR8KYO7tdC029z5xNTYGsWJEb7fpf8EqiCKqNqrY7ls44aALtnQVIMiHritOg5nH8Mv50d3YvtjmmQHN95nh24Aw0tVljGd0u4PbMhM6WjM3maQ2VQXgPufBb4zVMgw8vWffN9A+y8ad3bPCP5hROlji2E1CTfv1Qa6MFSkKqLcsD6TMgZbajrfT40wpEyeP7FnmJuK0vlNk6GuWnmTmvqQZ5pzkhYNBYcJpXZ3ArH70HJW/9twyyCJZzMxP1zYZhHh3c7kif9/MNI/r4mlSmVffkZFD4KHMxWzonR2MGwLRuGxtl/Fullxa/g5APfwrHrxVI5cSR5JPm0LsO8d5kFq3gPR8uh3mK4+wO1BScOJJ82TIK8TpLeM4hUphTL6zq7Gpnb2YaT67F9jM4/JbXzn2r1LXDoLlR97BqY7ZzzDDMkDrZPhTGDfINLgz7s1f+6ghiQcu5AMso2Z0H/mPbQrS64WA0XqqHVa3IECxgUnBTIjBFGb0uJh++/oPQdXHpqfzTZAQ4Kzl3B8j9bbCSIajIxvGetHYBuqdauBgrmftDKBRMs0DthDxeWuxINjWG7ZQJKwnY/pzUrjc2mohAVPptNDbe1i2XhvRN2l/e/3XCe+rdN1xqP4RRoEwjYvhGo0pp8rblxfQ1fQek/uC5eVp9SgO8AAAAASUVORK5CYII=',
      disCheckIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAjklEQVQ4T2NkoAAwgvSeu3TL7h8DQzUjA6MwIbP+M/x/+4+Bsc1MT/UgWPOZS7d2MjEzFhppq14jpPncudta/1j+95voqbmDNZ+9dPuMsZ6qCSGNMHmY+lHNRIbYcAiwM5du72RiZiAqhZ26cFubiel/HzyFgdL2fwbGagYGBqLSNhMDY5sRLG0TGcgYygBsUXAQfN4cDgAAAABJRU5ErkJggg==',
      bank: [{ id: 0, name: '杭州银行', subhead: '无限额', img: '' }, { id: 1, name: '工商银行', subhead: '单笔限额2万，单日限额10万', img: '' }, { id: 2, name: '农业银行', subhead: '单笔限额1万，单日限额1万', img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAqCAYAAADFw8lbAAAHK0lEQVRYR+2Yf2zcZR3HX5/v97p2W1u2BQcuDAR/oDBHTKb4OwqMMBTQJZigwBhNittKr6XXVSDqRaay3q3rtXQ4ZAzUKWzBGCH8cGQkE3RsxsCCbCSCbmiFMWlLu9Fd756Peb7fb6/X3rfXu1lNMH7+udw9v173fp7n8+MR3iUm7xJO/sdB43GH+cOnkDbzQS/EGX6ctwYHmXvqBmB1sEub6DvawryaGkzVMiKRPaSHj3LG3wb42o5suTtZvqI9radjZDnGXI7wcZT5iJ5PTc2rDAx1ItzkQSib6a9uonbg/bjuiyhHEPYhPIVrHmZNx2vlwJYOuv1ql9fftwLVNmAhMDO3UCmgo52FYVR7EWmn8u37uOmekVKASwO1Kma1DWUVUFkwcTmgo4OVDCIP4ZrbadhwaCrYqUFTsUUoSYRLADdvwl5ggfe9dNCxMWPAu3GcVhrb9xaDLQ5qIeEnwMdyk3hK6EOI6ULd58oCleyFqPtN4BvAjDywA7ispCHpzxdik4N2r12AMQ+Ap6RvwhGMk6T/LykWnl3JkHm7LNBhp5azlxzn9X31oHHvIo7N/SzZyLU03/nX0kGfjkfYP9gNUg843kBlPzgxmtp3et+3rK05KdC29kFvfGfLUhB7pBYHIihGH6T/8A3Ed6QnwoYr2hmrQ7gbqAikfBPHXMc/a3YSj5tpAbW+eM7gxTjyszFlNYtx6mlO3Dc1aKrtTNQ8jeg5QecM8B2iyR+OG/zvKjo6WaqlHqQr501UXkVHLqa5c9wRGK+o/ZfzhhpQ1gNVwZbsgBPX0dh9oigo6VfQyo6xyCQ/om92c87h28H2jI5u/ehkds25Q/cAdcERSyPyXfpmJ4nHrUgBRv7qHc3ziLh2K5YFzW+geilNyf0FBzxfUcddTMOdL5Jq/QSO8wVUBbK76avdw5yhRQgveOPDQO3vXS3nofI4cGawzlOM6DXENhwNB/UGOM+Azg06PECaNbQmjxUFRe02PWsdKmJ6yGgG142CBXY+BcExmgy0o3kmEXcjUI/aREn6EPNZGje8FA7aHavDcK+vtRxHiRJNbPHu/ETLVzS/zZiVXlxwdGvBmMlAbcfu2LUY7wJXB7vZQDTREw6ait0PrAgae3HlChoSfyxY0P4w3aAbmz6CG3kS9fIIq9SjRBNXTAb6J+C8oPEg77if5FvrB/4roPX1FSyq3YdyQXB7XqEx+YFw0M7YEMLsoPG3RJOfD4W0P/asriYzy3fe07H1do5UyyMgXw6myxBNBn7cBsV8S8XGzqLKr2lKXDUp6PbtLr3P/RSR3L/2+jrOD7xPY24bP1YPsbjmGr445nIK5k7F7P3w3ZS1aDLHVwSUn9OUtMlDuNkzOph9AuQFJO+yibkXKgyateHXN3uTRS9g2L2swI+OE6plcxC2pwQ9npcQF9/66b5MFq0r9jDK8oD9BNGkH3S8fGj8P3oexD/McJA0S0J9qG2dbtCt8SoGj+1F9aPB+oeJJs8KB+1s2YrIDcF2/R24PDQq/SdA77rlQ2Qdm5mNRqfHiCa/FA7aHfs6hm1Bo3X4jTQlbSZT6PDX31hD1Tw/H823k3X4Xa3Xo7oJAq8jNNOY7JxE0bXngnkG4VTvgijbiFSvYk18qADITyZ+ifJh/xBJxC/6zGa/r2Or0ddQ9RML4c/0VX8lP9HIzemF0MhG0CCEchwjn6E58Xw46MamOTgRq+BXgw69uO6lNKy3gaC49dyykBFnJ8K5wdF5mQqztKSy+K6288maJ0DPCBaZIinx07w6FCv5LH+QbkPSdQVp3kRsC5rxzlh5oF4CfWwLov7dAJtO3sZ7D6XyHyoKM/y7Y/M5Ibu8ytK3DA7f4+bkHUUlPVlFU7GV4CUjfhluE+eIuWhiCR1einS1LEflF2OVothSZAW7D/+GHZM8x5QLapU8ZegihG1IUOTZCtc+CTUlfzxRlHBQ76IM9owr7uAlkFaiicdClS136zvXLkVMAoIkBAzKVvoPrS69uLMkXrms94MuzYEpRxFZR9XApoKnmFIVtU9DvWetweH2CeXyLioqr2fV963/LrDyHyDsmRV5FDXrSMvBXOSaCjQRm80s+SAZvTXwKrnMCDiAmBtp7Ngz2T0o7UkHbEZk6yjrK0ftLeAR0N/hOn/wyg9he+7Ww8s42avRGQ6SXYLhcwjLxqlotxt2IebbxSDtglOD2l5dt74HMm2oNox7JPODwjDQB5pGZQESPNUoaUR7QSoQmQs606+HcjYCugXHvYOb2+2bVFErDdRO4d3SwStxxLopW/MHfnaqJQra3wEOo6yjv/rB0EhV9hkNY/D8rI1cehmOfHrCVhajfhOR32PMk7jur0pRMX+y0hXNH+Wp21+LzDgNxzt/l4DYAHE6wmkoDsIbqPwD0QOgO1F3LzOcIxypGsg9C5WxGScHWsYC09X1/6DTpeToPP8Ca1NrWDqhjNwAAAAASUVORK5CYII=' }, { id: 3, name: '中国银行', subhead: '单笔限额15万，单日限额30万', img: '' }]
    };
  },

  methods: {
    jump: function jump(router) {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + router,
        animated: true
      });
    },
    change: function change() {
      this.checked = !this.checked;
    }
  }
};

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('pwd-header', {
    attrs: {
      "backButton": "取消",
      "title": "宝石山开户"
    }
  }), _c('scroller', [_c('div', {
    staticClass: ["top-box"]
  }, [_c('div', {
    staticClass: ["top-first"]
  }, [_c('image', {
    staticClass: ["first-icon"],
    attrs: {
      "src": _vm.firstIcon
    }
  })]), _c('div', {
    staticClass: ["line-did"]
  }), _c('div', {
    staticClass: ["top-first"]
  }, [_c('image', {
    staticClass: ["second-icon"],
    attrs: {
      "src": _vm.secondIcon
    }
  })]), _c('div', {
    staticClass: ["line-did"]
  }), _c('div', {
    staticClass: ["confirm-box"]
  }, [_c('image', {
    staticClass: ["bg-img"],
    attrs: {
      "src": _vm.bgImg
    }
  }), _c('image', {
    staticClass: ["third-icon"],
    attrs: {
      "src": _vm.third
    }
  }), _c('text', {
    staticClass: ["confirm-text"]
  }, [_vm._v("绑定银行卡")])]), _c('div', {
    staticClass: ["line"]
  }), _c('div', {
    staticClass: ["top-item"]
  }, [_c('image', {
    staticClass: ["fourth-icon"],
    attrs: {
      "src": _vm.fourth
    }
  })])]), _c('div', {
    staticClass: ["line-top"]
  }), _c('div', {
    staticClass: ["box"]
  }, [_c('div', {
    staticClass: ["cardholder-box"]
  }, [_c('text', {
    staticClass: ["cardholder"]
  }, [_vm._v("持卡人")]), _c('text', {
    staticClass: ["name"]
  }, [_vm._v(_vm._s(_vm.name))])]), _c('div', {
    staticClass: ["type-box"]
  }, [_c('image', {
    staticClass: ["type-img"],
    attrs: {
      "src": _vm.bank[2].img
    }
  }), _c('text', {
    staticClass: ["type-text"]
  }, [_vm._v(_vm._s(_vm.bank[2].name))])]), _c('div', {
    staticClass: ["card-box"]
  }, [_c('input', {
    staticClass: ["card-input"],
    attrs: {
      "type": "number",
      "placeholder": "请输入本人银行卡号",
      "value": (_vm.cardValue)
    },
    on: {
      "input": function($event) {
        _vm.cardValue = $event.target.attr.value
      }
    }
  }), _c('image', {
    staticClass: ["camera"],
    attrs: {
      "src": _vm.camera
    }
  })])]), _c('div', {
    staticClass: ["agree-box"]
  }, [(_vm.checked) ? _c('image', {
    staticClass: ["check-input"],
    attrs: {
      "src": _vm.checkedIcon
    },
    on: {
      "click": _vm.change
    }
  }) : _c('image', {
    staticClass: ["check-input"],
    attrs: {
      "src": _vm.disCheckIcon
    },
    on: {
      "click": _vm.change
    }
  }), _c('text', {
    staticClass: ["agree-text"]
  }, [_vm._v("我已阅读并同意")]), _c('text', {
    staticClass: ["agreement"]
  }, [_vm._v("《直销银行开通协议》")])]), _c('pwd-next', {
    attrs: {
      "text": "确认",
      "open": _vm.cardValue.length == 18 ? true : false,
      "router": "setDealPwd.js"
    }
  }), _c('text', {
    staticClass: ["support"]
  }, [_vm._v("支持的银行卡")]), _vm._l((_vm.bank), function(b) {
    return _c('div', {
      staticClass: ["bank-box"]
    }, [(b.id != 0) ? _c('div', {
      staticClass: ["bank-line"]
    }) : _vm._e(), _c('div', {
      staticClass: ["bank-item"]
    }, [_c('image', {
      staticClass: ["bank-img"],
      attrs: {
        "src": b.img
      }
    }), _c('div', {
      staticClass: ["bank-mation"]
    }, [_c('text', {
      staticClass: ["bank-title"]
    }, [_vm._v(_vm._s(b.name))]), _c('text', {
      staticClass: ["bank-subhead"]
    }, [_vm._v(_vm._s(b.subhead))])])])])
  })], 2)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = {
  "check": {
    "marginTop": "20",
    "color": "#FA5665",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "31",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  props: ['router', 'open', 'tips', 'text'],
  methods: {
    jump: function jump(router) {
      var _this = this;

      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      if (this.$parent.id == "forgetThird") {
        if (this.open) {
          this.$parent.isOK = true;
          setTimeout(function () {
            _this.$parent.isOK = false;
            navigator.push({
              url: urlFront + router,
              animated: "true"
            });
          }, 1000);
        }
      } else if (this.$parent.id == "faceFind") {
        if (!this.$parent.pass) {
          if (!/[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(this.$parent.identity)) {
            this.$parent.tip = '身份证信息错误，请重新填写';
          } else {
            this.$parent.tip = '';
            this.$parent.pass = true;
            this.$parent.noteText = 60;
            this.$parent.color = '#8F9AAE';
            this.$parent.tip = '短信验证码错误，请核对或重新获取';
            var timer = setInterval(function () {
              _this.$parent.noteText--;
              if (_this.$parent.noteText == 0) {
                clearInterval(timer);
                _this.$parent.noteText = '重发';
                _this.$parent.color = '#4BA0FF';
              }
            }, 1000);
          }
        } else {
          if (this.open) {
            navigator.push({
              url: urlFront + 'prepareFace.js',
              animated: true
            });
          }
        }
      } else {
        if (this.open) {
          navigator.push({
            url: urlFront + router,
            animated: "true"
          });
        }
      }
    }
  }
};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["check"]
  }, [_vm._v(_vm._s(_vm.tips))]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump(_vm.router)
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v(_vm._s(_vm.text ? _vm.text : '下一步'))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(4)
)

/* script */
__vue_exports__ = __webpack_require__(5)

/* template */
var __vue_template__ = __webpack_require__(6)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdNext.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-86c0103e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ })

/******/ });