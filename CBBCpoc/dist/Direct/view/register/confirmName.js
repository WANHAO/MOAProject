// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 117);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 117:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(118)
)

/* script */
__vue_exports__ = __webpack_require__(119)

/* template */
var __vue_template__ = __webpack_require__(120)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\register\\confirmName.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-73d1e2fc"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 118:
/***/ (function(module, exports) {

module.exports = {
  "top-box": {
    "paddingTop": 0,
    "paddingRight": "36",
    "paddingBottom": 0,
    "paddingLeft": "36",
    "marginTop": "45",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "top-first": {
    "width": "68",
    "height": "68",
    "backgroundColor": "rgba(250,250,251,1)",
    "borderRadius": "34",
    "alignItems": "center"
  },
  "first-icon": {
    "width": "36",
    "height": "36",
    "marginTop": "16"
  },
  "line-did": {
    "width": "84",
    "height": "3",
    "backgroundColor": "rgba(203,205,215,1)",
    "opacity": 0.3,
    "marginTop": "33"
  },
  "confirm-box": {
    "width": "193",
    "height": "78",
    "flexDirection": "row",
    "position": "relative"
  },
  "bg-img": {
    "width": "193",
    "height": "78",
    "position": "absolute"
  },
  "second-icon": {
    "marginTop": "15",
    "marginLeft": "10",
    "width": "29",
    "height": "34"
  },
  "confirm-text": {
    "fontSize": "22",
    "marginLeft": "10",
    "marginTop": "15",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(56,148,252,1)",
    "lineHeight": "34"
  },
  "line": {
    "width": "84",
    "height": "3",
    "backgroundColor": "rgba(56,148,252,1)",
    "opacity": 0.3,
    "marginTop": "33"
  },
  "top-item": {
    "width": "68",
    "height": "68",
    "backgroundColor": "rgba(235,244,254,1)",
    "borderRadius": "34",
    "alignItems": "center"
  },
  "item-icon": {
    "width": "34",
    "height": "28",
    "marginTop": "20"
  },
  "fourth-icon": {
    "width": "26",
    "height": "35",
    "marginTop": "16"
  },
  "line-top": {
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginTop": "27",
    "marginRight": "36",
    "marginBottom": 0,
    "marginLeft": "36"
  },
  "title": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)",
    "marginTop": "76",
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": "53"
  },
  "box": {
    "marginTop": "44",
    "height": "80",
    "flexDirection": "row",
    "paddingTop": 0,
    "paddingRight": "52",
    "paddingBottom": 0,
    "paddingLeft": "54",
    "justifyContent": "flex-start",
    "width": "750"
  },
  "box-first": {
    "marginTop": "79"
  },
  "label": {
    "marginTop": "20",
    "width": "140",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "box-input": {
    "height": "60",
    "width": "468",
    "paddingTop": "12"
  },
  "camera": {
    "marginTop": "20",
    "width": "36",
    "height": "32"
  },
  "box-line": {
    "marginTop": "15",
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(230,232,237,1)",
    "marginLeft": "36"
  },
  "icon-arrow": {
    "marginTop": "28",
    "width": "18",
    "height": "26"
  },
  "check-box": {
    "flexDirection": "row",
    "justifyContent": "flex-start",
    "marginTop": "34"
  },
  "check-input": {
    "width": "30",
    "height": "30",
    "marginLeft": "55"
  },
  "check-text": {
    "marginLeft": "19",
    "fontSize": "26",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)",
    "lineHeight": "30"
  },
  "check-help": {
    "marginLeft": "10",
    "width": "26",
    "height": "26",
    "lineHeight": "30"
  },
  "button": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginBottom": "155",
    "alignItems": "center"
  },
  "card": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  },
  "tip": {
    "width": "678",
    "height": "124",
    "marginLeft": "36",
    "position": "relative"
  },
  "tip-img": {
    "position": "absolute",
    "width": "678",
    "height": "124"
  },
  "tip-text": {
    "marginTop": "40",
    "marginLeft": "20",
    "fontSize": "24",
    "width": "634",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(77,80,89,1)"
  }
}

/***/ }),

/***/ 119:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var navigator = weex.requireModule('navigator'); //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  components: {
    PwdHeader: _pwdHeader2.default
  },
  data: function data() {
    return {
      identity: '',
      name: '',
      adress: '',
      job: '',
      checked: false,
      open: false,
      helpShow: false,
      bgImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGEAAAAnCAYAAAAfBybSAAADwklEQVRoQ+2bTWsTURSGz5k008TpxE5ajN+tpRYUBbWIIKg/QHSlC12K/0HwF4hLdwrSnYu4UsSFK60rRa1g1aKo1eJH2iaTZjImnST3yJlkYtpGLWicC72zmsm9Oee973POncVwMZ2myLHj4nwE8CIRbAMABHV12gECgJlqlS49vKddw1mnegpRu9nprCp+eweIxGnMFMQtTYOTyqRwHBACbuOsI+4jwrFwJKisRPBAQQi5DnwIc464D6oTwkPR6IQHiHA0PBVrOzMRjPN2pCCEWAcKQojmB6kVBAVBAgckkKA6QUGQwAEJJPidkClUxzVNOyKBnjUpQQjxUEEIGb2CEDIATq8gKAgSOCCBBNUJCoIEDkggQXWCgiCBAxJIUJ2gIEjggAQSVCcoCBI4IIEE1QkKggQOSCDB7wT1oT9cEurLWrj++9kVBFkgzBXFbQA4IYGeNSmBhLiDn3OLZ3Q9emNNOiDBoj2vchavXn2y7uTp/eciUbyACFvUIZH/QoYPiXyuVejyrfTEdUynZ+Llck5zYl0YcyLY3R1BV9dQdwuo6xEsRSP+yZ0EJDqmrlQqYjzew8KaF//GD+Wo2zg5ZMLePX39O3ckHwH+RgxB4e2H3KEXk9l5AKcZL1YxaHmOji2oEbgABf8uXqmR59XIMxJkeIIWF2tUNmtklqsUiyUFjo19iBlGF7YDwQEYBkBvR/X2toT//t1pgm9NGhTBwEisf2i47zEArF8himDh3cz8wU+Ti1keC0xonVc3xPSBc958/s9LW+289pHywObzWDsArlslvHL3bbeZ6ULPy2rxeBad2Fa/8rgrXDeCySQAJJPgFvO/PMtm9PTSL8dtAGNb67gNANYSvUaPILeo+fEZOvuyFHv9SdfrgPbt2mINDCaeAraAIFj4OD03+vz1vN1qNMBKlwNT/mz/389g43M5AMOoVz9H3B4viS9feD19wklVCdPpl/q3Hh0ZBHeEbWeQYQBsBN6i6kDsf3aY0DQtcpbFM80RcpxcM0cAn8UvL4IA0uEDm6yhoQ0TPgiChffvZ/e/evaVCTcrL6g+jhNcbAbAfOOxH37e/87w1c5bGaNsWr7xvPVYVoXYfMtKEXcAA9hY9IhfzFHLiiODyGW6cAAABgcH4c2bp1oQcvPmv6+IIAKLWB5vaiqLqVQwg+EzkA3+D9yRgVGum2qCYjg7dvdbwzsT4zOfFo5MTWTzS83ONN8xgRGtZvC9bWfRsvqWvIvarXS189r9l9cbXCMjo2J6eho+8ubSAGDbJfoBFwA9rW15iqIAAAAASUVORK5CYII=',
      firstIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABKElEQVQ4T7WTz0rDQBDGv9E2LRUKvYivkIIUmqwUPKhXRZCiD+FTWF/Cl9AeBA8eKyUQGreilwXfwHtbsDYZ2RDtHxOhabqwsDvM/Ha+mR1CRosy4oB6Up1uEG6ZUUgDJcJnMPEvyJPqCkAr2mlYYewvSFhmKpmeVLxeULf7VimWcg6DxoVcZb9W2xn+pzcxo+eX9xMO/IcwOMCREGZHH2dqqa8tYZnXkT1emuu65c18WYLJJwzrtm2PdECvrw6IcajPTOjs1c2nRJDjvG4bReOGGcdEAIMfgy//stHY/ZgF/UjVWcVK86RqA2jO1YT5XtjVswVpoYvudBJoAGBrvrg8ElZ1wTb1WH/7l/3afzJaeUT00BJwB8BYNpvIf8zAear5inswM9A3Zsmvq6vYLfYAAAAASUVORK5CYII=',
      secondIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAACD0lEQVQ4T41TXUtVQRRda86UQU/2YhJWiEJFVC96ruFvqMQ+ofoB5jWoqIw+fO2xzrm+Bj2Wff6BCoR7bxFkFBaZDz4FIUIEcTozs+J4FbKucPeGWcNm1rD2Zi9iJfoq2hr5fAhkBwCREAIgU5wAQgNloCIFfmJR6KuoK5I7IyKjxwyIQVHfI3E2RDDLJAcTwUJ0DNAuMhIPptoR5E4L+gltuLcFyJboxgl+rJbtw1Vlf2P/3ewUwQ6WEl+WCZvh7XMAPwx/twtmmLIvqmN81Ywcp9lJynaylOQT/z8wi7VFU8EEG/3+E6XEnQC1jaVKfgs+zFm7sRocmAn52/NcaEZarZXS7LhktjNO85sU39XKy7JbioHEHfPSTsZJfgPk+/qofTY4qXbn3f71frCRnZke4VJccUcZ1M1Sml8PCh9el9ue9k2qO/Lu7HpkH9n7b0Y4HydumFQP+9P8WgTOVkftk5Y0A4iTbBgwvYxTP87gPtfG2h63Sh5I3ZAP2l30fBXgl3rZPmomm/m329ULXb/WLEmaDRmZPUXPVxT4tT5mp5oNbNOinX45QbeGnGRHDM3eYkkuy3C+fs5OtSo7vpMdhrX7CtmXyLBQG2170Cq5lLhDog4wrviLFHLBzAEehlEAHOjt8mp648TChg4wbNjSSb2G7GQxOQT1wBhIgULDglQDwZUsrmHFnoQpLPsHohn/7BEnwk0AAAAASUVORK5CYII=',
      third: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAn0lEQVQ4T2NkoBJgpJI5DIxmU/6rMf7/E87IwMBMjqH/GRj+/mdkWcloMfm3AwMDAwgfIMcgmF64QSdyWRvIMchi8m+QvgPILiLHHJgeFIMGmddAYWTR/12BgYVFgSg//vnz4EQh5wOMMAIbhIhBYsw6cCKX9QBWg4jRja6GXBeBXYFsGHaDCIURNFwIGkQVr1GcRaiWacnxDjY9VCuPAJU+bzlXBKIWAAAAAElFTkSuQmCC',
      fourth: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABOUlEQVQ4T2NkoBJgxGWO4ZTv8mz/mJ0ZGBmlwWr+/3/6i+nv3vM5nA+x6cFqENiQ/yxxjIxMH//++3cNpJGZiUnr//9//L8Y/yzCZhhWg8wn/UpiZGDgecPwcOadPNWfIIO0V/1n433xO+M/A8OXk3ls89Bdhd2gyb9r//1nOH46j3UPsgbTSb9dmBgZLE/msjYTZZDF5N8NDAwMB07ksh5A1mAx+bcDAwODw4lcVpA8CoC7yGzKfzXG/3/CGRkYmImJyP8MDH//M7KsPJXDeAukHm4QzDaQS4gxCOQyZFdjGITN2dgMRvc+/QwChR3IRbCwgLmOZBeZTfpdBTYoj7UNLQZRYpag10wm/FAHGXCmgOMmRQbhikGSvUa2QRSnI1BYMDMzh5GSsv/+/bsKFnY4yyMiUzdcGQDwx6kTtXj9QAAAAABJRU5ErkJggg==',
      camera: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAARCAYAAADQWvz5AAAB/klEQVQ4T52SMWhTURSGv/OSSEnE0UEsVikFlyo46CROxSS1Q6V1aZtnoQ6C0A6Ci5o6iUsLgoNF85LqYIsdapOIk7gpOKigUApWqktHsVVs8n55xcBrSEP0Tpfz/+fjnHt/o8FJ5XUTMWGGhWUJYUyVMjZZ37bDGIjJRzroVFhB3Jd4H24woxvjkh+lszxkX3do2ayct4foqkJbIBiMY5zeWKXrZdYqYfOZrKKJDpYRrwTTgRaBXye+sGwpT3mDkR2j+rjFUcs3WjudVwbh1a08a2lPPjAWjzNbE+cH7XcjSK02MKc9tfvmJsPATAASDueLI7YQbh6Y096NTcYMrgR1wd1EnJn5QfuRyum4GQsS/RbhCD5PG4J6CkrEfD4IfgKTJhwZ1w3athy6Y1VGzZiWGCfCt11BaU/3DNqXXDsXnrLX0zPBWtG1y8HDB5+RLqh/V1DK07pj9C5l7E0YlMzplBmLJdf21+pNQWlPlS2fwy9GbS0M6nmo9pjD56Jr0ZZAKU/vBHfKrj0Og9J5DUlcLbl2rCVQ0tOIwVQ0wsnFYVsJmvpm1Vmp8lowUXat0BIIyVJ5rhncALZBQKfgVinDbczUGuiv62xOHeZwdDtHPp+eX7TV+qA2fexmqW4G+gjsM1j/F0DNKwii8N36HuhAJcIFIPE/IGAjWuXJH2zoB3JFrkclAAAAAElFTkSuQmCC',
      iconArrow: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAANCAYAAAB7AEQGAAAAo0lEQVQoU5XRTRLBQBAF4NfjCrmKA5AJG9UHwDaxdQWFC7BwDTPZJSZOZlrNqFIRRej116/6h/BDUTBpygsoJK62BwDS7XugjJdEtPZezs3F7rswolB6wgVAxU3EXGu7a8MnilBzDkUrETGuBV9QgGPNuYrQW1eX25D4hmJixicQDT2waSpT/p+kp5xDvszUu91Pdxrp2VypQeIqc/x48b733QF6clkOxLK7eAAAAABJRU5ErkJggg==',
      checkedIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAYAAACMo1E1AAADeklEQVRYR+2YWUiUURTHf9cxU8mtjULDaJGIEKI0SttTW4igp4J2inpohXoIKnqKIqiol6Ky0peI6kFarQczkbI0yqIFWsSKqDC1NEtnbpy+BseZz/m+GXWch+7rd+45P//3LM5R/D1azctnoCOCXA1rgUwg1vgWgqNoQvNAwSmni+Lrq/mKUloJ2MJ8xrsc7ANy0agQ4JiHUGg0t4GdV1epSjUvXw+KcFCIJgeI6DWw9sAuFLdcTparBef0MjQF0IuK+Soi77dCzT+jS5Riehgo1gFBa+6oBWd1U0iT374KzQKn7duH1vI/XLB6h0S5CAUJ0dAvCpwavv2En63WyD0OJ2ATkmFJOowcAC1tUFELZyoNSH+nx+GSE2D3TEhJaMeQErzyAk5U9CJcfDTsnQ1pA30hahtgUxG0uToH7DHlEmNgQyZkDzcP/uQT7CoGl59G1iNwfRywNB0Wj4NIk2ktRSFg1Z+66VklsZUCp59nkFBik50KW7IgOtI3+K82OP0Qrr3spmqVnJmYDLFR8KYO7tdC029z5xNTYGsWJEb7fpf8EqiCKqNqrY7ls44aALtnQVIMiHritOg5nH8Mv50d3YvtjmmQHN95nh24Aw0tVljGd0u4PbMhM6WjM3maQ2VQXgPufBb4zVMgw8vWffN9A+y8ad3bPCP5hROlji2E1CTfv1Qa6MFSkKqLcsD6TMgZbajrfT40wpEyeP7FnmJuK0vlNk6GuWnmTmvqQZ5pzkhYNBYcJpXZ3ArH70HJW/9twyyCJZzMxP1zYZhHh3c7kif9/MNI/r4mlSmVffkZFD4KHMxWzonR2MGwLRuGxtl/Fullxa/g5APfwrHrxVI5cSR5JPm0LsO8d5kFq3gPR8uh3mK4+wO1BScOJJ82TIK8TpLeM4hUphTL6zq7Gpnb2YaT67F9jM4/JbXzn2r1LXDoLlR97BqY7ZzzDDMkDrZPhTGDfINLgz7s1f+6ghiQcu5AMso2Z0H/mPbQrS64WA0XqqHVa3IECxgUnBTIjBFGb0uJh++/oPQdXHpqfzTZAQ4Kzl3B8j9bbCSIajIxvGetHYBuqdauBgrmftDKBRMs0DthDxeWuxINjWG7ZQJKwnY/pzUrjc2mohAVPptNDbe1i2XhvRN2l/e/3XCe+rdN1xqP4RRoEwjYvhGo0pp8rblxfQ1fQek/uC5eVp9SgO8AAAAASUVORK5CYII=',
      helpIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAABzUlEQVQ4T31STWjUUBCe75ndiIIUQXpfPFhQPCn04g9IS5psNiCabMVL0ZPetCCexUKtVy/aQyluEkRMsy8EiiBCQfTgRUFB6F08WASF/ekb2Re6De3qu82b+Wa++eYD7XlTU9cPm2b3Egma0ClFXzqd6uv19dXf5VKUAuG4/h0IcZeIPzDh0yAH4lNEOMtKLck0fqxb6f/iCafRfA6icdXHjSxrbZa72/ZsTRj8jIm+y7Xw2gCsgY7rzwPC6ne3rDzPO9PTV45WDhrzRDTOTG+zNFyxLMs0qmM5s8plGj+C3ulQf1P1upNZ9lJPshvBhiD6xiTeENQSK76VpfELPbnC7zp/jBrqXtNjojmZhO4AdNyyzAlzLFI9zGVZ62fdC9pM9F4m0QPNzgva2FbLqDf8ewwckUl0f6/CtusHAJ6w4HPZq/hzAWw+BNOvfwIdxz8NAxu0zefb7fjjTlOn0VwA8dY+qsMCL7hAhEmZhAtlJpoqqeWR4hSU/NukMCPTaGYHaNuXa6JSLcQZdQ6trHv1IsSBMzIJFwfxvnPsGiBoATimerg5ygCo8FNi/iHXotmhAYbgsuUYWkWAT/7PcsP9d02OE4XJ+esok/8FVjjpUtZW4mQAAAAASUVORK5CYII=',
      disCheckIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAjklEQVQ4T2NkoAAwgvSeu3TL7h8DQzUjA6MwIbP+M/x/+4+Bsc1MT/UgWPOZS7d2MjEzFhppq14jpPncudta/1j+95voqbmDNZ+9dPuMsZ6qCSGNMHmY+lHNRIbYcAiwM5du72RiZiAqhZ26cFubiel/HzyFgdL2fwbGagYGBqLSNhMDY5sRLG0TGcgYygBsUXAQfN4cDgAAAABJRU5ErkJggg==',
      tipImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVMAAAA+CAYAAACWVvhEAAAGgUlEQVR4Xu3bT2wUZRjH8eed/Vu2C7SG0EYgSpAo0XDyYkLkYIKRgzVqCQdJNFxMNPHqRYoXryaaeDCBBA+EEiIaFA0HNCRevEiUxqihSUXbpqFb2R263Z19X/POdsrQFKxc3t13v3vZpuzyvM/nnfnxzjuDEl49JWCMUUnDx49L/POePe33iS3fxe/Dv5VXPnPkyN59p05dvZJ8Z3p31cTfmdsfv09MSPx+7Fj73b6UUis/9xQuzfa0wMpJ09MKPdL8vYI0HaIDA31qZiavZJfImwd2vpMNgg+iSL/7yaXrH8ofIkNDDVOpLJp0qBKoPXIA0eZ9BQjTHjhA0iFq27Ur0vRq1K5E4xDtz6vybFaVSlk1MrLtjVxOPrILTRExzaZ++/z5v0+EYWSqWyMzVLsTqulVanqFyiq1Bw4uWlwRUKtPNGz8FEgu6ZPLersaTUI0DDcElcq0KpXy6lZfTr316hOjxWLuUxHJpDRa9Xrr6Mdnr53duNg0YdgwAwPDplS6rZOVqg3VZJW6+tLfT1W6QuCOwF1hmj7hQPJHIFmFxnucqRC1K9Ho17nAhmhfX041m4VgdPTRF/r785+JSG4NgWat1nhtfHzy61xuSS8uh2r28S16rZVqXG95T9UfTTpB4I7AXfcKxsZMkPxR+qRLfpfspwHY3QLJTaXkcn51iEZRLTh8+Mlny+XCGREp3qfb+kJ16dD46V++z2b79b1C1X4/2VftbjlGj0BbINnOSnukFwtqfNzEl3Jr3cm1v7cnH5jdKxDfTLKvXSJ2PzS5nE9WojZEW/354NDzu58eHCycE5H+dXRbm58PXz7zzfUfM7WGXh2q9vLf7qvaG1b2ZW9arePv5CMIdLSA3c5KD3D1ky1q7PLlrF21TA/0qT0iktzJtSde8kV7Atqft23b3tHNMri1BSqV2Xj+7H7ohmpWFYsZZUM0n8+oVikfHNy386mhoeJ5Edm8XkMjsjA7Uxv56srUz5mwoRuNlrGhWq+3zO1yZOy+avsf460E6XpR+VzHCdy48Wc8JrtASAaXLBTsImHCPkq4/HSLGhu/lh+cyatyub1qsYGZPvm2iki1uhysWzquVwb0HwLFaiYO0jBcUIODD0mYD1SrGQb5XEa1omxw4Jnhx3bsKH+hlPzv2TVG5qamqi9++8P075lspBvNlsnkSrrU0GZ+/qaUSpvjA7BebhGoHKndKTDXHna5HJlZEUkvEmzQxldh1cjMDzWMOnlysnjw4PD2TZsyLwVBsCnpWGtRwfJuqtZaiaxsrXYnSo+P2ohW7UuNwD5dr8RopUVUMZ89rJQ8/KA8xshf9UZ0OhAxogL7PwKMiI6f4FcSEKIPCsv3OkhASxC0j2WtRYL4FFp5VRYWwi8vXLg5per15nOFQvbzde6VdVCDDAUBBBDoCIFwaSkaUVqbn5SSvR0xJAaBAAIIdKfAVfucabTq4ezubIVRI4AAAu4EtA1T9rXcTQCVEUDAEwHC1JOJpA0EEHArQJi69ac6Agh4IkCYejKRtIEAAm4FCFO3/lRHAAFPBAhTTyaSNhBAwK0AYerWn+oIIOCJAGHqyUTSBgIIuBUgTN36Ux0BBDwRIEw9mUjaQAABtwKEqVt/qiOAgCcChKknE0kbCCDgVoAwdetPdQQQ8ESAMPVkImkDAQTcChCmbv2pjgACnggQpp5MJG0ggIBbAcLUrT/VEUDAEwHC1JOJpA0EEHArQJi69ac6Agh4IkCYejKRtIEAAm4FCFO3/lRHAAFPBAhTTyaSNhBAwK0AYerWn+oIIOCJAGHqyUTSBgIIuBUgTN36Ux0BBDwRIEw9mUjaQAABtwKEqVt/qiOAgCcChKknE0kbCCDgVoAwdetPdQQQ8ESAMPVkImkDAQTcChCmbv2pjgACnggQpp5MJG0ggIBbAcLUrT/VEUDAEwHC1JOJpA0EEHArQJi69ac6Agh4IkCYejKRtIEAAm4FCFO3/lRHAAFPBAhTTyaSNhBAwK0AYerWn+oIIOCJAGHqyUTSBgIIuBUgTN36Ux0BBDwRIEw9mUjaQAABtwKEqVt/qiOAgCcChKknE0kbCCDgVoAwdetPdQQQ8ESAMPVkImkDAQTcCtgw/UdENrodBtURQACB7hUwIrfU4mJ0oljMvN69bTByBBBAwK1Avd46qS5enHxk//7t7xUKwStKqbLbIVEdAQQQ6B4BY0x1aUmfu3Tpxvv/AlM5wfQwYs63AAAAAElFTkSuQmCC'
    };
  },

  methods: {
    jump: function jump(router) {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + router,
        animated: true
      });
    },
    change: function change() {
      this.checked = !this.checked;
    },
    changeHelp: function changeHelp() {
      this.helpShow = !this.helpShow;
    }
  }
};

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('pwd-header', {
    attrs: {
      "backButton": "取消",
      "title": "宝石山开户"
    }
  }), _c('scroller', [_c('div', {
    staticClass: ["top-box"]
  }, [_c('div', {
    staticClass: ["top-first"]
  }, [_c('image', {
    staticClass: ["first-icon"],
    attrs: {
      "src": _vm.firstIcon
    }
  })]), _c('div', {
    staticClass: ["line-did"]
  }), _c('div', {
    staticClass: ["confirm-box"]
  }, [_c('image', {
    staticClass: ["bg-img"],
    attrs: {
      "src": _vm.bgImg
    }
  }), _c('image', {
    staticClass: ["second-icon"],
    attrs: {
      "src": _vm.secondIcon
    }
  }), _c('text', {
    staticClass: ["confirm-text"]
  }, [_vm._v("确认实名信息")])]), _c('div', {
    staticClass: ["line"]
  }), _c('div', {
    staticClass: ["top-item"]
  }, [_c('image', {
    staticClass: ["item-icon"],
    attrs: {
      "src": _vm.third
    }
  })]), _c('div', {
    staticClass: ["line"]
  }), _c('div', {
    staticClass: ["top-item"]
  }, [_c('image', {
    staticClass: ["fourth-icon"],
    attrs: {
      "src": _vm.fourth
    }
  })])]), _c('div', {
    staticClass: ["line-top"]
  }), _c('text', {
    staticClass: ["title"]
  }, [_vm._v("请确认您的实名信息，如信息有误可进行修改")]), _c('div', {
    staticClass: ["box", "box-first"]
  }, [_c('text', {
    staticClass: ["label"]
  }, [_vm._v("身份证")]), _c('input', {
    staticClass: ["box-input"],
    attrs: {
      "type": "text",
      "value": (_vm.identity)
    },
    on: {
      "input": function($event) {
        _vm.identity = $event.target.attr.value
      }
    }
  }), _c('image', {
    staticClass: ["camera"],
    attrs: {
      "src": _vm.camera
    }
  })]), _c('div', {
    staticClass: ["box-line"]
  }), _c('div', {
    staticClass: ["box"]
  }, [_c('text', {
    staticClass: ["label"]
  }, [_vm._v("姓名")]), _c('input', {
    staticClass: ["box-input"],
    attrs: {
      "type": "text",
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        _vm.name = $event.target.attr.value
      }
    }
  })]), _c('div', {
    staticClass: ["box-line"]
  }), _c('div', {
    staticClass: ["box"]
  }, [_c('text', {
    staticClass: ["label"]
  }, [_vm._v("住址")]), _c('input', {
    staticClass: ["box-input"],
    attrs: {
      "type": "text",
      "value": (_vm.adress)
    },
    on: {
      "input": function($event) {
        _vm.adress = $event.target.attr.value
      }
    }
  })]), _c('div', {
    staticClass: ["box-line"]
  }), _c('div', {
    staticClass: ["box"]
  }, [_c('text', {
    staticClass: ["label"]
  }, [_vm._v("职业")]), _c('input', {
    staticClass: ["box-input"],
    attrs: {
      "type": "text",
      "placeholder": "请选择",
      "value": (_vm.job)
    },
    on: {
      "input": function($event) {
        _vm.job = $event.target.attr.value
      }
    }
  }), _c('image', {
    staticClass: ["icon-arrow"],
    attrs: {
      "src": _vm.iconArrow
    }
  })]), _c('div', {
    staticClass: ["box-line"]
  }), _c('div', {
    staticClass: ["check-box"]
  }, [(_vm.checked) ? _c('image', {
    staticClass: ["check-input"],
    attrs: {
      "src": _vm.checkedIcon
    },
    on: {
      "click": _vm.change
    }
  }) : _c('image', {
    staticClass: ["check-input"],
    attrs: {
      "src": _vm.disCheckIcon
    },
    on: {
      "click": _vm.change
    }
  }), _c('text', {
    staticClass: ["check-text"]
  }, [_vm._v("本人为税收居民")]), _c('image', {
    staticClass: ["check-help"],
    attrs: {
      "src": _vm.helpIcon
    },
    on: {
      "click": _vm.changeHelp
    }
  })]), _c('div', {
    staticClass: ["tip"],
    style: {
      visibility: _vm.helpShow ? 'visible' : 'hidden'
    }
  }, [_c('image', {
    staticClass: ["tip-img"],
    attrs: {
      "src": _vm.tipImg
    }
  }), _c('text', {
    staticClass: ["tip-text"]
  }, [_vm._v("税收居民是指在中国境内有住所，或者无住所而在境内居住满一年的个人，不包括香港、澳门、台湾地区的税收居民")])]), _c('div', {
    staticClass: ["button"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump('writeCard.js')
      }
    }
  }, [_c('text', {
    staticClass: ["card"]
  }, [_vm._v("提交")])])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ })

/******/ });