// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 141);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 141:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(142)
)

/* script */
__vue_exports__ = __webpack_require__(143)

/* template */
var __vue_template__ = __webpack_require__(144)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\register\\setDealPwd.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-2cb298b0"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 142:
/***/ (function(module, exports) {

module.exports = {
  "top-box": {
    "paddingTop": 0,
    "paddingRight": "36",
    "paddingBottom": 0,
    "paddingLeft": "36",
    "marginTop": "45",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "top-first": {
    "width": "68",
    "height": "68",
    "backgroundColor": "rgba(250,250,251,1)",
    "borderRadius": "34",
    "alignItems": "center"
  },
  "first-icon": {
    "width": "36",
    "height": "36",
    "marginTop": "16"
  },
  "line-did": {
    "width": "84",
    "height": "3",
    "backgroundColor": "rgba(203,205,215,1)",
    "opacity": 0.3,
    "marginTop": "33"
  },
  "confirm-box": {
    "width": "193",
    "height": "78",
    "flexDirection": "row",
    "position": "relative"
  },
  "bg-img": {
    "width": "193",
    "height": "78",
    "position": "absolute"
  },
  "second-icon": {
    "marginTop": "15",
    "width": "29",
    "height": "34"
  },
  "third-icon": {
    "marginTop": "15",
    "width": "36",
    "height": "36"
  },
  "confirm-text": {
    "marginTop": "15",
    "fontSize": "22",
    "marginLeft": "10",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(56,148,252,1)",
    "lineHeight": "36"
  },
  "top-item": {
    "width": "68",
    "height": "68",
    "backgroundColor": "rgba(235,244,254,1)",
    "borderRadius": "34",
    "alignItems": "center"
  },
  "fourth-icon": {
    "width": "26",
    "height": "35",
    "marginLeft": "10",
    "marginTop": "16"
  },
  "line-top": {
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginTop": "27",
    "marginRight": "36",
    "marginBottom": 0,
    "marginLeft": "36"
  },
  "title-box": {
    "marginTop": "76",
    "flexDirection": "row",
    "justifyContent": "space-around"
  },
  "title": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "pwd-box": {
    "width": "666",
    "height": "111",
    "marginTop": "35",
    "marginLeft": "42",
    "flexDirection": "row",
    "borderStyle": "solid",
    "borderWidth": "1",
    "borderColor": "#D6DAE1",
    "position": "relative"
  },
  "pwd-item": {
    "height": "111",
    "width": "1",
    "backgroundColor": "#D6DAE1",
    "marginLeft": "111"
  },
  "pwd-input": {
    "position": "absolute",
    "letterSpacing": "111",
    "height": "111",
    "width": "666"
  }
}

/***/ }),

/***/ 143:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

var _pwdNext = __webpack_require__(7);

var _pwdNext2 = _interopRequireDefault(_pwdNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  components: {
    PwdHeader: _pwdHeader2.default,
    PwdNext: _pwdNext2.default
  },
  data: function data() {
    return {
      title1: '请设置6位数字交易密码，用于交易验证',
      title2: '请再次输入以确认',
      bgImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGEAAAAnCAYAAAAfBybSAAADwklEQVRoQ+2bTWsTURSGz5k008TpxE5ajN+tpRYUBbWIIKg/QHSlC12K/0HwF4hLdwrSnYu4UsSFK60rRa1g1aKo1eJH2iaTZjImnST3yJlkYtpGLWicC72zmsm9Oee973POncVwMZ2myLHj4nwE8CIRbAMABHV12gECgJlqlS49vKddw1mnegpRu9nprCp+eweIxGnMFMQtTYOTyqRwHBACbuOsI+4jwrFwJKisRPBAQQi5DnwIc464D6oTwkPR6IQHiHA0PBVrOzMRjPN2pCCEWAcKQojmB6kVBAVBAgckkKA6QUGQwAEJJPidkClUxzVNOyKBnjUpQQjxUEEIGb2CEDIATq8gKAgSOCCBBNUJCoIEDkggQXWCgiCBAxJIUJ2gIEjggAQSVCcoCBI4IIEE1QkKggQOSCDB7wT1oT9cEurLWrj++9kVBFkgzBXFbQA4IYGeNSmBhLiDn3OLZ3Q9emNNOiDBoj2vchavXn2y7uTp/eciUbyACFvUIZH/QoYPiXyuVejyrfTEdUynZ+Llck5zYl0YcyLY3R1BV9dQdwuo6xEsRSP+yZ0EJDqmrlQqYjzew8KaF//GD+Wo2zg5ZMLePX39O3ckHwH+RgxB4e2H3KEXk9l5AKcZL1YxaHmOji2oEbgABf8uXqmR59XIMxJkeIIWF2tUNmtklqsUiyUFjo19iBlGF7YDwQEYBkBvR/X2toT//t1pgm9NGhTBwEisf2i47zEArF8himDh3cz8wU+Ti1keC0xonVc3xPSBc958/s9LW+289pHywObzWDsArlslvHL3bbeZ6ULPy2rxeBad2Fa/8rgrXDeCySQAJJPgFvO/PMtm9PTSL8dtAGNb67gNANYSvUaPILeo+fEZOvuyFHv9SdfrgPbt2mINDCaeAraAIFj4OD03+vz1vN1qNMBKlwNT/mz/389g43M5AMOoVz9H3B4viS9feD19wklVCdPpl/q3Hh0ZBHeEbWeQYQBsBN6i6kDsf3aY0DQtcpbFM80RcpxcM0cAn8UvL4IA0uEDm6yhoQ0TPgiChffvZ/e/evaVCTcrL6g+jhNcbAbAfOOxH37e/87w1c5bGaNsWr7xvPVYVoXYfMtKEXcAA9hY9IhfzFHLiiODyGW6cAAABgcH4c2bp1oQcvPmv6+IIAKLWB5vaiqLqVQwg+EzkA3+D9yRgVGum2qCYjg7dvdbwzsT4zOfFo5MTWTzS83ONN8xgRGtZvC9bWfRsvqWvIvarXS189r9l9cbXCMjo2J6eho+8ubSAGDbJfoBFwA9rW15iqIAAAAASUVORK5CYII=',
      firstIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABKElEQVQ4T7WTz0rDQBDGv9E2LRUKvYivkIIUmqwUPKhXRZCiD+FTWF/Cl9AeBA8eKyUQGreilwXfwHtbsDYZ2RDtHxOhabqwsDvM/Ha+mR1CRosy4oB6Up1uEG6ZUUgDJcJnMPEvyJPqCkAr2mlYYewvSFhmKpmeVLxeULf7VimWcg6DxoVcZb9W2xn+pzcxo+eX9xMO/IcwOMCREGZHH2dqqa8tYZnXkT1emuu65c18WYLJJwzrtm2PdECvrw6IcajPTOjs1c2nRJDjvG4bReOGGcdEAIMfgy//stHY/ZgF/UjVWcVK86RqA2jO1YT5XtjVswVpoYvudBJoAGBrvrg8ElZ1wTb1WH/7l/3afzJaeUT00BJwB8BYNpvIf8zAear5inswM9A3Zsmvq6vYLfYAAAAASUVORK5CYII=',
      secondIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAACBUlEQVQ4T42TT2sTURTFz5lpVXBXxH26EDNgwEwmG8USVAiuFBQr0orS7+BScOXOL6BoC5FSwVAXtfhn11XeTFY6Tyq4Ei2IVI1UJckceVMrKY2QCzOPeff97j3vzr3EXzPmzXF43kOSx5QbRCKT0HdHSPQAyD0E+pnwlM5hzOsT9P1VCR2IC6BuCkgJNSFvbBuWD4LK3Jvn8yCtlp2irxWAG+O+d6rX6/wQD34VuFQNj17eUTa4msQuCyixlaTrBA8j45y3z/+gbm9SREPErWq5eHs4nDYBhDSJdffYZSTedb5tBLVazd1zj7US+4RE5OAM0jN4uJuJPpBtVcvBGsk9Qf8VN7aPQZykSdIuhPmoEswNyzJUdmyXREy5zL8BNKKweMOY9Ul4/VlkALwBbOc78xei6Mh7k6SLAE87+CeBxUpYvG7a6RmIL/6rgDoblYOXJraPQNUZt21HUjMKg9lRZbdi2yB1zmX+Dmg5CoOZUWGTvF0AdMEVbFPkSrVcvDpM9v7x7kSpVNrc1SSxnQd1ka3YfiHxPAqLV4YV7NDEgTuFQuHXIBwn9oGAaSf7M6BXURhMjy47vQ9wxsGfIKxFleKlkeE4vQfymoM/AtryPH8VyiSxD/ejybw1JfUhz02oSG2PJ1hX3tttOy+hTreXjy38HALyUQQwJoFk7s99bo9A+gc+gwHbCwsOYQAAAABJRU5ErkJggg==',
      third: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAAB/ElEQVRoQ+1awVLCMBB9Sauf4Xi0HMHgeOMn5Es8cBNOcvFH+ApujBRujr05jJ+h0nUoyBRaIJOmjSnpicNmd9++R5Jtl6GmD6spLjhgtjHrGLOSMSLyXufv9x73hqC4DbALu4DQNxifLuNl767VmDDGlokUp9M3wX1/RETXdgHazZYBC87YQ7N5M1sDm0WPjOEZgG8zMIB+iFivfRu8JMDCedQH8GQ3qG32A9EK+seADbQCjQHwjUe9v/cJOQ5MtAIrjoJwHtEeAQ6YVkXqduYY011RA/5O7ooGctIS0gHTUsYKnZxkzB3QFbKRCeW2+7+SuCuVSR2uOxN3V0w42JdiGEYdcHSUCdLRqsQYCxGM0zkcZyyM+uC7jWYOsIyNMkjVhTEGQgSrpnj7ZIBtbA42mhlg/6PLTg5fecZykq4HsHOWourfoux1Z7rdS0ix7Mqr+rdhV8zseDJgC0uxgpeqJQCTkGKYYyNTUWmbnMNXZq2T4qG2pfS7Ys49UIkxIPVqQEKKMkFM2BSWoomkZWIW3hVlgpiwKdy2mEhaJqaTYqpKNXivWPbhK6MpXTbpDrq2H9dX4xDwvBEDrB6HIGDhp8chRkTeVTLAwocgCACXupRRkZ8vMITLOO59fjQm3e5mgKWi4JWGsWIyQKUiDphK1UyucYyZrL5K7F9+xMVG3aRSsQAAAABJRU5ErkJggg==',
      fourth: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABOUlEQVQ4T2NkoBJgxGWO4ZTv8mz/mJ0ZGBmlwWr+/3/6i+nv3vM5nA+x6cFqENiQ/yxxjIxMH//++3cNpJGZiUnr//9//L8Y/yzCZhhWg8wn/UpiZGDgecPwcOadPNWfIIO0V/1n433xO+M/A8OXk3ls89Bdhd2gyb9r//1nOH46j3UPsgbTSb9dmBgZLE/msjYTZZDF5N8NDAwMB07ksh5A1mAx+bcDAwODw4lcVpA8CoC7yGzKfzXG/3/CGRkYmImJyP8MDH//M7KsPJXDeAukHm4QzDaQS4gxCOQyZFdjGITN2dgMRvc+/QwChR3IRbCwgLmOZBeZTfpdBTYoj7UNLQZRYpag10wm/FAHGXCmgOMmRQbhikGSvUa2QRSnI1BYMDMzh5GSsv/+/bsKFnY4yyMiUzdcGQDwx6kTtXj9QAAAAABJRU5ErkJggg=='

    };
  },

  methods: {}
};

/***/ }),

/***/ 144:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('pwd-header', {
    attrs: {
      "title": "宝石山开户"
    }
  }), _c('scroller', [_c('div', {
    staticClass: ["top-box"]
  }, [_c('div', {
    staticClass: ["top-first"]
  }, [_c('image', {
    staticClass: ["first-icon"],
    attrs: {
      "src": _vm.firstIcon
    }
  })]), _c('div', {
    staticClass: ["line-did"]
  }), _c('div', {
    staticClass: ["top-first"]
  }, [_c('image', {
    staticClass: ["second-icon"],
    attrs: {
      "src": _vm.secondIcon
    }
  })]), _c('div', {
    staticClass: ["line-did"]
  }), _c('div', {
    staticClass: ["top-first"]
  }, [_c('image', {
    staticClass: ["third-icon"],
    attrs: {
      "src": _vm.third
    }
  })]), _c('div', {
    staticClass: ["line-did"]
  }), _c('div', {
    staticClass: ["confirm-box"]
  }, [_c('image', {
    staticClass: ["bg-img"],
    attrs: {
      "src": _vm.bgImg
    }
  }), _c('image', {
    staticClass: ["fourth-icon"],
    attrs: {
      "src": _vm.fourth
    }
  }), _c('text', {
    staticClass: ["confirm-text"]
  }, [_vm._v("设置交易密码")])])]), _c('div', {
    staticClass: ["line-top"]
  }), _c('div', {
    staticClass: ["title-box"]
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title1))])]), _c('div', {
    staticClass: ["pwd-box"]
  }, [_c('input', {
    staticClass: ["pwd-input"],
    attrs: {
      "type": "password",
      "maxlength": "6"
    }
  }), _vm._l((5), function(i) {
    return _c('div', {
      staticClass: ["pwd-item"]
    })
  })], 2), _c('pwd-next', {
    attrs: {
      "text": "完成",
      "open": _vm.title1.length == 16 ? true : false,
      "router": "setDealPwd.js"
    }
  })], 1)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = {
  "check": {
    "marginTop": "20",
    "color": "#FA5665",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "31",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  props: ['router', 'open', 'tips', 'text'],
  methods: {
    jump: function jump(router) {
      var _this = this;

      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      if (this.$parent.id == "forgetThird") {
        if (this.open) {
          this.$parent.isOK = true;
          setTimeout(function () {
            _this.$parent.isOK = false;
            navigator.push({
              url: urlFront + router,
              animated: "true"
            });
          }, 1000);
        }
      } else if (this.$parent.id == "faceFind") {
        if (!this.$parent.pass) {
          if (!/[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(this.$parent.identity)) {
            this.$parent.tip = '身份证信息错误，请重新填写';
          } else {
            this.$parent.tip = '';
            this.$parent.pass = true;
            this.$parent.noteText = 60;
            this.$parent.color = '#8F9AAE';
            this.$parent.tip = '短信验证码错误，请核对或重新获取';
            var timer = setInterval(function () {
              _this.$parent.noteText--;
              if (_this.$parent.noteText == 0) {
                clearInterval(timer);
                _this.$parent.noteText = '重发';
                _this.$parent.color = '#4BA0FF';
              }
            }, 1000);
          }
        } else {
          if (this.open) {
            navigator.push({
              url: urlFront + 'prepareFace.js',
              animated: true
            });
          }
        }
      } else {
        if (this.open) {
          navigator.push({
            url: urlFront + router,
            animated: "true"
          });
        }
      }
    }
  }
};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["check"]
  }, [_vm._v(_vm._s(_vm.tips))]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump(_vm.router)
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v(_vm._s(_vm.text ? _vm.text : '下一步'))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(4)
)

/* script */
__vue_exports__ = __webpack_require__(5)

/* template */
var __vue_template__ = __webpack_require__(6)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdNext.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-86c0103e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ })

/******/ });