// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 41);
/******/ })
/************************************************************************/
/******/ ({

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(42)
)

/* script */
__vue_exports__ = __webpack_require__(43)

/* template */
var __vue_template__ = __webpack_require__(44)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\Gold\\productDetails.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-a5958be2"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 42:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "width": "750",
    "height": "98",
    "flexDirection": "row",
    "alignItems": "center",
    "paddingLeft": "36"
  },
  "arrow": {
    "width": "18",
    "height": "29"
  },
  "titleText": {
    "marginLeft": "258",
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "detailTitle": {
    "marginTop": "88",
    "height": "137",
    "justifyContent": "space-between"
  },
  "basicInfomation": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "basicDes": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)"
  },
  "detailBody": {
    "height": "553",
    "paddingTop": "39",
    "paddingBottom": "46",
    "justifyContent": "space-between",
    "borderBottomColor": "#CBCDD7",
    "borderBottomStyle": "solid",
    "borderBottomWidth": "1"
  },
  "detailItem": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "flex-start"
  },
  "detailKey": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)"
  },
  "detailValue": {
    "width": "501",
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "detailBottom": {
    "height": "88",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "borderBottomColor": "#CBCDD7",
    "borderBottomStyle": "solid",
    "borderBottomWidth": "1"
  },
  "protfolio": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "type": {
    "fontSize": "26",
    "fontFamily": "PingFang-SC-Regular",
    "color": "rgba(203,205,215,1)",
    "marginRight": "21"
  },
  "arrowtriange": {
    "width": "11",
    "height": "15"
  },
  "bottomSign": {
    "width": "199",
    "height": "30",
    "marginTop": "35",
    "marginBottom": "80"
  }
}

/***/ }),

/***/ 43:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var modal = weex.requireModule('modal');
var navigator = weex.requireModule('navigator');
exports.default = {
  data: function data() {
    return {
      marginTop: 0,
      arrow: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAaCAYAAAC6nQw6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIcSURBVEhLzZTPbtNAEIdnEghxSEKSHoA2BceFOsVSDjhIgMoD9AEq8SK8BLRc6d9H4FD3RF3nUK6EIyIoTdQDysFSxQWkUuJlZrMYTBLXx34He+dn68tmZmW4dKC6J8a27avlcnU1QHHecp23FAWcp/mSFCmZmXsJiM8Rcbmmmyf9fqfLz1LyjQSEEsBnKgKRhh9qmUzEklJl/tW/EhRip3XgHKnyYpFlWZnSTHUNUSyrCAKSuK6zoUpJbLMtazVzc+7nGv3aUxWBELDtuXubqgyZ2uxJEtJsea6zpYoIE3fEktskoeVfCYrNw3fOtqrGGBNJyezZOo34iYqoJ7DRcvd2VDmRiIglt2bPXtMZeayiRBImFN1bWbl2N8iso4BQIiB44x3s76oylnD89/ki4qcYRzi1brc7TC8a3o0gZdFfq3KGgI90YzHo9758lC/FEBn/KclyWuOwUPj1gJo9zxlJm7pRJ1knVjZ2jnz/01DTGl5UBk2jZopejGzigQxlxeESlVJGtqaxYELvuNOW9X9MPdlSlm1410lGExjJAO1psqkihmW57BUvX6zUSXZnlKJdM+pIPYvIYkWM7/vDSinvZbXiEvVKyuhu64aZItkH+RJxoYgZDAZBpcyyQp2mqGT4UF8wP/ePOydcJ/qwMe12+/zb6dcXdGrfq4h3llPLZDv6A++sXMq3clr+O4jUEX2X9ikWo6eXC4DfHm23ffE1u64AAAAASUVORK5CYII=',
      detailItems: [{ detailKey: '基金全称', detailValue: '博时黄金交易型开放式证券投资基金联接基金C类' }, { detailKey: '基金简称', detailValue: '博时黄金ETF联接C' }, { detailKey: '简称代码', detailValue: '002611' }, { detailKey: '基金类型', detailValue: '交易型开放式指数基金' }, { detailKey: '分红方式', detailValue: '分红方式' }, { detailKey: '投资目标', detailValue: '紧密挂钩金价。通过主要投资于博时黄金交易型开放式证券投资基金，紧密跟踪标的指数的市场表现，追求跟踪偏离度和跟踪误差的最小化。' }],
      arrowtriange: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAUCAYAAACEYr13AAABtklEQVQ4T6WTz0obYRTFz/liIoio64IgSMAYLBITmVHxAURcuXAhPoJiSkERbKAglhbcddFs+hYuXMSFMZJ/gpo/+ARKENyoJI3fFRM7mUYzSXGWwz2/+5177iXe+fGdelgAEVGJbHFOa9HTQd8+SekEbgESqdyCkNt1EWOo9G5PTQ0+tINYgONMYR4iEZvgUlUf1w1j7NoJYrfARLawBsGyTXCjqD4bEyNnrSCvhvhsBeSmAO6aSFAh5KsZ8u+/BXkzhXgyN07yB4iBF4hA4bcZ8P1sHm7LGI+yxQ9K6z0Aw43OjHlw9yUYDN7//ee4B+l0uqeCnh0AM/bhdmkJT076r2p5tYspnrpYJNWGVUeUqbllhnyHjoDaYmXyYYBLllhYUi79yQj4844WYrlcb/c9d0EYVqGg8MflDs8GvCX7q19ZSCbPBx9dak+EQ41CObjt90TmvN5ys+V/APHkRUgpfhOwrxEfo2ZgJNrqNizASab4UYv+BaCrfg4oK42IERo96GiVj1P5FRCr9c4sUVXD5sRYoV1K1gtip6cDHt29SQ1WXe7vzcPq+BbadXQc4v+Kn+ufADMClBVv8AF9AAAAAElFTkSuQmCC',
      detailBottom: [{ protfolio: '投资组合', type: '约87%为基金投资', jump: 'protfolio' }, { protfolio: '交易规则', type: '买入卖出规则、费率', jump: 'tradeRule' }, { protfolio: '基金经理/公司', type: '赵云阳/博时基金', jump: 'company' }, { protfolio: '风险提示', type: '中高风险', jump: 'venture' }],
      bottomSign: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMcAAAAeCAYAAACYNAohAAAOXUlEQVR4Xu1cbYxcVRl+njMzuwU0bYGIfPnR8rHTlpbtzLCCyFda/KER/UETf/gLQn+QGP6VnyaGSEHjB9VgCRqEVOkGNSJE44YYiFA7O0ttZadFWQh+IQgFpe3uzsx5zXvn3tkzd+6de+92u9bs3qRNu/fec8/H+5z3eZ/3Pctqrf4DCg/Mzp710DXXXHwCy9fyDCzPgDcDrNbqzwC4HOB+AzxQKg3p/5ev5RlY8jPAam3yaYIbRSAAGgB+BuHuSmXoyJKfneUJWNIzoJ7jlwA3AmIJDyD611skHm01sGdkpPj2kp6h5cEv2Rng/lr9SQNsEMC2Z0EEoChQBHgZxA8HcrO/2rRp07ElO0vLA1+SM8DxWv2nIljvUyqAaGksAqDg4UNoxeDOyvDQcyR9AC3JuVoe9BKbAaVVT0CwAYR6i2kBmgAGCOREMCnGPnjWIJ47fpzXw+B6mzcPj2waenWJzdPycJfgDLA6Xh8FUCTRVGpFYFAo7xN8DHbgR+Xy2vd0Xmq1+mct8BUQ74hgz+wJPnHttUP/Weg5q44fvheUEoDRSqm4O6r9aq1+B4gtEBytlIrb59MHbYOU1QLUKpvXjaVpozoxuYVASYT63Z6+1Wr1klC2wJqpgYHZsY0bNx5N027WZw4ePLh6dnZgS7k8pGu3KNf4+OFbYewa/Vh587qdi/LRBf5ItfryGuaat3rBQys/WqlcNtXvE21wUKVczKrHAKQKK7sqlfUvui9Wq/UbYHAvOrEJDhvwsYWWfqu1+m8AbCHl7rhFUCOFUJ+DAcqlUrGWdR77fce/p23f7bY9PjG5Q4Q6B2OVUnFr+JseaIHvAx5oz4647wkeSVe/seu71Vpdv6HfqlVKxbL3M52TlJcRHs06Z/6mtSNubEmfDjaWpOey3I/bpOLacO0GlK1JmyLHa5OPC1gE8I6Ajxz79+CPb7zx49M9C1s9/Ekx+BogxzUeIZAH0CL5TIPy2NXDxdeyDCx2ACnA4RvIOAD1MLvn4z0SwNE24tAEpgBHx2hBudsdoy5EtVY/aXC4C+yCKG3bfp96wK1j67d+IlSvoYDUazcpfXfd8MbmzN1CmEnQRuQmtWDgqE7U90LwJiwf6Jfb2P/ikQrF3ucF7kID2FkBjcYmII6K8BcrCrNPnSyViDJaf0f23KFz6WLpH6UuPZ4jamd3Xz5F4HgHwOqoxamUihrfeeBQo1Y61/OcUMeodC/Saypts4B6TP1Gx2v4m0Uq4MWBIyO4Eg1cxxuabwVWeA3dRwLPp6DrC7zOS8JapTzUtQn161hmz7Fv4qV1I8Pr6iT7Tu7vJ45sMrD3o50snFFGI0CeEF/BohXK66Dsvmp4/YHE2XMeUC6IXNPjs2jTFs8jgDKqFEB5vE9nUjcbXpzwiwsNDodSRfbRBUecSw921yhwhIBx1ABbs1KjvoYz59UUtPONlRS0unZImn+3LxrPCGWv/my+NNnbIBLi1TTgcONKVicmb0NLJsIxRo8xVQ9fDiP3axKEbXCsEDVlzRmKAoTvicG7FPlApbTuttRWDCDB5Y6BslMD4SxtJgWNpwAcXqzUj+Y5u3MkLRGhvt8Tb/mLqsajxrfgwOjyPCm4eNw6uMaXBRzBWoS9YZb19sfQN15NAw6XOlNplQjOIWXPmYPy+Pr169+P6tTExORHW5Yac5wJsgmRvOY9LHFCCxdJmfbzJZ+rlIoXZBlYEjiSKFLab1Vr9XFSRhU4LjjCyoVjxNspPBqoQnExR9ek29zaOBUkLXVxPYcqUzONgtI1vTrA8D2JAmbnYKExugB0NjLOSju3nnE6QklacHTNHbA9TqFM048kMSczOPZPHH4IgstJaQmknhM+HOWu9/3hTxflm82vAlipHsNLFgoOUGQS4FrQy7J/AsCqSql4fprBhJ9xDSGWWlAiOb22NZhv1uKMxKNupvVKoLY4u5VSCG2zszA9RuwbfCw4fBFB2yalR+YM5OJ5e452+6thc9sC4Dn9n4LNbe3Q0pQTH1asAoUOwhqNnRet0qA9CNSTPHfQTd2wfCo2VSkV16bsfuRjCw4OXxa8FKAFxYpghsCTeTO9d3h4+N2gFwqOXKN5DykrBKwb8Nkm7EoDcx2Ac4SSo+ALAN6bLzhc7gnhznCw5RhE9Bz2oQRO257C0WmLGIV4gWJncRwjVv7dUcSiwOEBenZgByg7MNdWV/8CoHfajelnXMyhXqJQaEwFwO/ytJStfu5FY7UsV5wcHdDDLG11nk3rMfQFdxwUbjvZvM2Cg2P/xOT3KLxUVJZtFx5q+UhLiNfY4t5KZeh3OpB9+w6dlysUbqdI1ebsm7S8QcgrKbAQmQF5M4CPAXijUipeOJ+ZdfT74PUuN9u1W3YrGmrAq/tp1x2d3gfdHDhkK4Qqwaog4H3PpVV+3gKwubWaQIrLcwSigmuoCgodSNhzzFet8nl1kEvRiM/bQOYpk8bmajQ5Gl6/tFJuWo/h00L1GnpNgZIpmRuVo1hwcFTH69+B4VpY2wCpwXZLNOAW5EmxVljNm/wjw8OXvH3o0KGVM838pynmU5YYoLVN0gvOzxfw81BZF/x7pTT0kazgCHHrudedXTZu8K6hxyV2Avcd7FDuLt4xaGK0srm4zb0H8A71LB1D75MEdKiCx98j5MxUcmuclBtSxFLRkPnEAVFrl4avp11zf62V4sZS5KS2ojzUgoNjvHb4uwK5COJlyBtiMEuRHIQ5EtYKDthm89GRkQ1vTU5OnnlsOncraCvaeYqZEYqhYBuAc72fAf8sl4oXJQ0ufN/Z/YIYQEs6VL3pBKHzBYcTb2Cw0Dhb6YkLAI1VOkGvza31YxMvCUhrVvsy4xQpu/tlyDOAI5BLg1xNWNvvKZ2JkIpTJcBON3D4wFDqlkl9DNvLooCjWqvv0p2/HWBzmhDjdYR4HdY8bXPN92nNVTaH3+aauBqwh20OM2jlrieVRsk6AEqpgktpVaaA3NlJFBhqKJ6cKdYoVdLMrZcFny84HOB5ibNIsLSTobeqZwk094CmVWt1VYtGITzq96fLMCOSlEFCq1OzpaUoFmjTCN8bOv3qyvIr5XBFkQi6qa3834EjAhhBTNdTjRC1ubr0cVHAMV6b/JaA50F4TINtAMct+IKR1iRghoSoEGSL3E1rbyGxSoCDRuRgG1TcJe1YI7j+kVXKDRbfA4Sj9Q/km7tnGoVXgoTXfMERFJwFtTjObtqpgfI4sM0dVTUoHDjroqq3iVOrUnF+erGNVw8WgMPh3a4YEMQUc+qZD1wvMRoD0DhPndVz+H2KCu47CT6/IqFH0QrXorl98tvV2C7wGF6SNzwn/RjHooOjOl7/hhh82AhmreBVioyLMaspspnkCtEMuPCY5Fo/oeXNEHM2jadqzYrAGmBWDG6C4KZ2zIFM4HAl1sFCY+1Mo6DafScRpvcj5MvoQD1lAitFjVSm2qquDL+urgOCYLG7qFt3HKXce41H4doVv4FhdrxJUPekwW5S38PGlRUcobxDNnYcM/8RMUabCTh5kTSFgIsOjv0T9a9rYo/CSQGPGcqlFrKKQlWvpq26DZFpS/PzHOQ6C66iaBBuVwK8S7TUgBhjO0uudKJQKRW9+CPtFZecCysfHU08tAhpAnK3L53gPKaGKU5yTWuYwfuxAbnT/4hYq6NCJVCLU0KrPENu5nvigbAK11UbpkWJlKl+eaYoCnnag6Nam7xLaPzyantuGxSaEPRO/Z0QEUuyZYlfG5ERCj4IsCGUzwDUwFyfWwHwr4D8C5RGZfO6kbTA0OeUswdZ3lRlHaEsdBgcrrfp2UnnkoGxdTyLCQ7fc2os4pWGULi9n96fFqDBuLN6jjT0LEQLPRlcvX6/LL13BqWZv8Pd8E57cNRqf7xEmPsShOdaSpPi5TtaIBpKndqTxYZB61mhuVK9jFheCOL2Niig5e1agan/fpuCL5fLxT1ZwBHa1SPrY5wkXs9ZiR5wtIvouqpWg284xhUrhS4mOLRfrvdIMrLTCRz+xhZkuCPnu58dnPbg8BZn/JWVwMy11hj1DIOANL1MOT2jb4PE2BeNzV0GI2eI4E4AQaJvsH1QSvZSeF+5XDw0X2D4kx0JDr+0XjPZPec3YsDRk2dwuW/fw1RBhWrG8xyd3dp/36/E9U4c+vVcnVjGj0G8EnVPQZsro6gNFhpb43bh0w4cbU/c9nwRVQ1pwRGbGHUb8Ev69Uf91Cpf8Ywqe08UFfxksErsY101988fOHJhvtW6luCQ96sVKOoVGiLGgjgCyMUUjoDiVt2+QJF7yuV1T50MKBzD6gFH0g7jgsMNfHs4f7uk2TvN1m+HPhnPEcr8dnI2fslKkASckzA98YrbtCbJPathgO1RNW6nChxJJwkTYg4FuSZKdwaxSNIpO28jdAPyjMaTAI6MrUU+3g2O4JH9B15ab5q82gu+KU2ATViZInGBUCtzvYTfn0Xkm4OFxiML+Wt7wjFH1zkGP4PdE0c4hX/+PY+/u0dV3SRaimOomdSqEC1sq09zV6fkvudMSrsWayyIt0JnNqYGC41y2IMkgcM3OPdUX5Bo7HvGIm3FcFqrS1NjdQrBMUZKqt8LEB5PkEro8Rzug88//5czCoXjVyAnG6hny619A8QXBeZmoTzYNGbXNVde/re0k5X2uTA4nMTRGthcOaocPGRU7SgppET54FCZVLlxz/nviLgn8xlynxZ6J950cdxS+E4fiTEXEOF5CUrRNRsfVaeUCA5HcAi13fc48f8cHClk+JOVctPYYNd5jqQX1Dinm/krCPMhiL3F0nx7ZPNQUDCW9Hrm+1G/FUT70GgU1iSdfFPlx5jW6rjnwtWtWTvnnhI7mXMHSd8Nko5Rz6XpQ5AX0fc18akyaxqak9Svhb7vysb9ZOAO5XZOjC7GeP4LvYbpY52kl9EAAAAASUVORK5CYII='
    };
  },

  methods: {
    jumpPage: function jumpPage(Page) {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/');
      // modal.toast({ message: Page })
      navigator.push({
        url: urlFront + '/' + Page + '.js',
        animated: "true"
      }, function () {
        modal.toast({ message: 'success' });
      });
    },
    returnPage: function returnPage() {
      navigator.pop({ animated: 'true' });
    }
  }
};

/***/ }),

/***/ 44:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    style: {
      marginTop: _vm.marginTop
    }
  }, [_c('div', {
    staticClass: ["header"]
  }, [_c('image', {
    staticClass: ["arrow"],
    attrs: {
      "src": _vm.arrow
    },
    on: {
      "click": _vm.returnPage
    }
  }), _c('text', {
    staticClass: ["titleText"]
  }, [_vm._v("产品详情")])])]), _c('list', [_c('cell', {
    staticStyle: {
      paddingLeft: "53px",
      paddingRight: "53px"
    },
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_vm._m(0), _c('div', {
    staticClass: ["detailBody"]
  }, _vm._l((_vm.detailItems), function(item) {
    return _c('div', {
      staticClass: ["detailItem"]
    }, [_c('text', {
      staticClass: ["detailKey"]
    }, [_vm._v(_vm._s(item.detailKey))]), _c('text', {
      staticClass: ["detailValue"]
    }, [_vm._v(_vm._s(item.detailValue))])])
  }))]), _c('cell', {
    staticStyle: {
      paddingLeft: "52px",
      paddingRight: "52px"
    },
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, _vm._l((_vm.detailBottom), function(bottomItem) {
    return _c('div', {
      staticClass: ["detailBottom"]
    }, [_c('text', {
      staticClass: ["protfolio"]
    }, [_vm._v(_vm._s(bottomItem.protfolio))]), _c('div', {
      staticStyle: {
        flexDirection: "row",
        alignItems: "center"
      },
      on: {
        "click": function($event) {
          _vm.jumpPage(bottomItem.jump)
        }
      }
    }, [_c('text', {
      staticClass: ["type"]
    }, [_vm._v(_vm._s(bottomItem.type))]), _c('image', {
      staticClass: ["arrowtriange"],
      attrs: {
        "src": _vm.arrowtriange
      }
    })])])
  })), _c('cell', {
    staticStyle: {
      alignItems: "center"
    },
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('image', {
    staticClass: ["bottomSign"],
    attrs: {
      "src": _vm.bottomSign
    }
  })])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["detailTitle"]
  }, [_c('text', {
    staticClass: ["basicInfomation"]
  }, [_vm._v("基本信息")]), _c('text', {
    staticClass: ["basicDes"]
  }, [_vm._v("幸福如金对接“博时黄金ETF联接C”基金，购买幸福如金就是购买“博时黄金ETF联接C”")])])
}]}
module.exports.render._withStripped = true

/***/ })

/******/ });