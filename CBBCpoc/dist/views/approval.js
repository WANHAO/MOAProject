// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 267);
/******/ })
/************************************************************************/
/******/ ({

/***/ 267:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(268)
)

/* script */
__vue_exports__ = __webpack_require__(269)

/* template */
var __vue_template__ = __webpack_require__(270)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\WorkProject\\Weex\\hzBankOA-weex\\src\\views\\approval.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-09bda376"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 268:
/***/ (function(module, exports) {

module.exports = {
  "approval": {
    "backgroundColor": "#f5f5f9"
  },
  "header": {
    "paddingTop": "68",
    "paddingBottom": "20"
  },
  "title": {
    "fontSize": "34",
    "fontWeight": "500",
    "textAlign": "center"
  },
  "hzTitle": {
    "paddingTop": "29",
    "paddingRight": "32",
    "paddingBottom": "29",
    "paddingLeft": 0,
    "flexDirection": "row",
    "justifyContent": "space-between",
    "backgroundColor": "#ffffff"
  },
  "textLeft": {
    "fontFamily": "PingFangSC-Medium",
    "fontSize": "30",
    "borderLeftWidth": "6",
    "borderLeftColor": "#00a4ea",
    "paddingLeft": "32"
  },
  "textRight": {
    "fontSize": "24",
    "color": "#666666"
  },
  "icon": {
    "width": "14",
    "height": "24",
    "marginLeft": "16"
  },
  "right": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "search": {
    "backgroundColor": "#ffffff",
    "paddingLeft": "32",
    "flexDirection": "row"
  },
  "inputSearch": {
    "width": "458",
    "height": "60",
    "lineHeight": "60",
    "backgroundColor": "#f5f5f8",
    "paddingLeft": "62",
    "fontSize": "28",
    "color": "rgb(153,153,153)",
    "transitionProperty": "color",
    "transitionDuration": 500,
    "transitionTimingFunction": "ease-in-out"
  },
  "@TRANSITION": {
    "inputSearch": {
      "property": "color",
      "duration": 500,
      "timingFunction": "ease-in-out"
    },
    "movelightBar": {
      "property": "left",
      "duration": 200,
      "timingFunction": "ease-in-out"
    }
  },
  "searchIcon": {
    "width": "30",
    "height": "30",
    "position": "absolute",
    "left": "48",
    "top": "16"
  },
  "searchIcon2": {
    "width": "30",
    "height": "30",
    "position": "absolute",
    "left": "48",
    "bottom": "46"
  },
  "question": {
    "fontFamily": "PingFangSC-Medium",
    "fontSize": "30",
    "color": "rgb(51,51,51)",
    "lineHeight": "60",
    "marginLeft": "40"
  },
  "message": {
    "fontFamily": "PingFangSC-Medium",
    "fontSize": "30",
    "color": "rgb(51,51,51)",
    "lineHeight": "60",
    "marginLeft": "40"
  },
  "tabbar": {
    "width": "750",
    "paddingTop": "0",
    "paddingRight": "237",
    "paddingBottom": "0",
    "paddingLeft": "237",
    "flexDirection": "row",
    "alignItems": "center",
    "backgroundColor": "#ffffff"
  },
  "tab": {
    "width": "110",
    "marginRight": "56",
    "paddingTop": "32",
    "paddingRight": 0,
    "paddingBottom": "27",
    "paddingLeft": 0,
    "alignItems": "center",
    "justifyContent": "center",
    "textAlign": "center"
  },
  "barTitle": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "28",
    "color": "rgb(51,51,51)"
  },
  "active": {
    "color": "rgb(0,164,234)"
  },
  "movelightBar": {
    "width": "166",
    "position": "absolute",
    "bottom": 0,
    "left": 237,
    "transitionProperty": "left",
    "transitionDuration": 200,
    "transitionTimingFunction": "ease-in-out"
  },
  "lightBar": {
    "width": "56",
    "height": "6",
    "position": "absolute",
    "bottom": 0,
    "left": "27"
  },
  "line": {
    "backgroundColor": "rgba(0,0,0,0.08)",
    "height": "1"
  },
  "lineLine": {
    "backgroundColor": "rgba(0,0,0,0.08)",
    "height": "1",
    "width": "686",
    "marginTop": 0,
    "marginRight": "32",
    "marginBottom": 0,
    "marginLeft": "32"
  },
  "answer": {
    "backgroundColor": "#ffffff",
    "marginBottom": "16"
  },
  "list": {
    "flexDirection": "row",
    "paddingTop": "32",
    "paddingRight": "32",
    "paddingBottom": "32",
    "paddingLeft": "32",
    "alignItems": "center"
  },
  "mark": {
    "width": "40",
    "height": "40",
    "marginRight": "32"
  },
  "bulb": {
    "width": "40",
    "height": "40",
    "marginRight": "32"
  },
  "answerText": {
    "width": "614",
    "fontSize": "30",
    "color": "rgb(51,51,51)"
  },
  "applyModal": {
    "paddingTop": 0,
    "paddingRight": "32",
    "paddingBottom": 0,
    "paddingLeft": "32",
    "backgroundColor": "#ffffff",
    "flexDirection": "row"
  },
  "seal": {
    "width": "40",
    "height": "40"
  },
  "docRout": {
    "width": "40",
    "height": "40"
  },
  "headText": {
    "color": "rgb(51,51,51)",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium"
  },
  "modelText": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)"
  },
  "imgModal": {
    "paddingTop": "32",
    "paddingRight": 0,
    "paddingBottom": "34",
    "paddingLeft": 0,
    "flexDirection": "row",
    "width": "342"
  },
  "a": {
    "borderRightWidth": "1",
    "borderRightColor": "rgba(0,0,0,0.08)"
  },
  "b": {
    "marginLeft": "32"
  },
  "imgText": {
    "marginLeft": "24"
  },
  "popUpSearchPage": {
    "width": "750",
    "height": "1200",
    "backgroundColor": "#ffffff",
    "position": "fixed",
    "top": 0,
    "left": 0
  },
  "popUpSearchPageHead": {
    "paddingTop": 0,
    "paddingRight": "42",
    "paddingBottom": "32",
    "paddingLeft": "32",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "borderBottomColor": "rgb(228,228,228)",
    "borderBottomWidth": "1"
  },
  "searchBar": {
    "width": "586",
    "height": "60",
    "fontSize": "28",
    "paddingLeft": "62",
    "color": "rgb(153,153,153)",
    "backgroundColor": "#ebebeb"
  },
  "searchText": {
    "color": "rgb(17,120,228)",
    "fontSize": "28",
    "fontFamily": "PingFangSC-Medium"
  },
  "commandoHotSearch": {
    "paddingTop": 0,
    "paddingRight": "32",
    "paddingBottom": 0,
    "paddingLeft": "32"
  },
  "searchHistory": {
    "paddingTop": 0,
    "paddingRight": "32",
    "paddingBottom": 0,
    "paddingLeft": "32"
  },
  "hotSearchText": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "paddingTop": "40"
  },
  "searchHistoryText": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "paddingTop": "40"
  },
  "hotImg": {
    "width": "20",
    "height": "24"
  },
  "hotSearchContent": {
    "flexDirection": "row",
    "flexWrap": "wrap"
  },
  "hotSearchContentText": {
    "height": "60",
    "backgroundColor": "rgb(246,246,246)",
    "flexDirection": "row",
    "alignItems": "center",
    "marginLeft": "24",
    "marginTop": "26",
    "paddingTop": 0,
    "paddingRight": "32",
    "paddingBottom": 0,
    "paddingLeft": "32",
    "borderRadius": "30"
  },
  "hotText": {
    "fontSize": "24",
    "color": "rgb(39,39,39)",
    "textAlign": "center",
    "marginLeft": "18",
    "lineHeight": "60"
  },
  "historyList": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "borderBottomColor": "rgb(228,228,228)",
    "borderBottomWidth": "1"
  },
  "historyListContent": {
    "paddingRight": "32",
    "height": "80",
    "flexDirection": "row",
    "alignItems": "center"
  },
  "historyImg": {
    "width": "28",
    "height": "28",
    "marginRight": "22"
  },
  "historyText": {
    "fontSize": "30",
    "color": "rgb(79,79,79)",
    "lineHeight": "80"
  },
  "del": {
    "width": "20",
    "height": "20"
  },
  "historyListContentLast": {
    "paddingRight": "32",
    "borderBottomColor": "rgb(228,228,228)",
    "borderBottomWidth": "1",
    "height": "80",
    "flexDirection": "row",
    "alignItems": "center"
  }
}

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
    data: function data() {
        return {
            isColor: false,
            isShow: false,
            activeTab: 0, // 切换tab标识
            tabs: ['待回答', '已回答'],
            Env: WXEnvironment, // 获取设备环境变量
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAkCAYAAABmMXGeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NUExMURDNDI0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NkM2RDI2NUU0RDFGMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1QTExREM0MDREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1QTExREM0MTREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Prhhnl0AAAIUSURBVHjapJbLK8RRFMd/w1DYiKyIHRFL8ohCjKIsWLDwimZDyTv/whBbMwkzHiVRkkcJ5RV/gNcaO0xZUCS+p85Pp58h995Tn8U9M326v3vPufe6/H6/z7KsHrAIusGbpRBer/dHLgr0gTjQBTZAgmUYJF0W42pwAFJMpW1gVuTywTHIMJF+gE7gE/lMcALydKUUn2AE9IvfUsEhKNGV2jEJWsA7jxPBLqgzkVIsgHrwymOqjHXQYSKl2AYV4FH8bwYMmUgpzkApuBM52swJ4NKVUlyBInApctQsQRCrK7V4pmU8cztaeJ3jdaUWr20lr7UdNWAvEAgk60opXrgqQiJXCI4gTtOVWly/7VzPdmTT0kCcoyu1u486b9jRfTTjQl2pHWPcEB88TgL7EHtMpBRzEbpvyVRKEWOy+5GCjsxVnqFdIU0m0lEwLSb2AMpxZ+26NWQuLqlekbsFVRDe0EBVGsub0yxyF9RdEH4fPCpS6vM14BG5UzrAIQzrbFQK37JSuMmfHNbZ/XTqFlAgckGuzxedUyqXPzFL5MYdnaQkLebbNFXkBvhK+fxrJr9tVC1YEUX9zs+i0H82IJK0nYs6msfU141gS/c2HeQnkC184hN/S6WY3aJLxh0vlHt+sF2qtpybTxn63FaRv6YadFzPSi+UeYfwPMJ9ryxtEOMdfpk8mL5P6cXxDKb+6hKV+BJgAJ0QdkktcujgAAAAAElFTkSuQmCC',
            search: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAqCAYAAADFw8lbAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RjJEMzg0NkI0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RjJEMzg0NkM0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEMjhFRDE1QTREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGMkQzODQ2QTREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Ppuo7k4AAAOySURBVHjazJl7aE5hHMfPa5eYjQy5hoyokeUdtVrMXdFcZkiay9ay8Je7KPHfFiJGmFCLZmRsrrWVhWjDSjaGWGtJq2EXMjbfX/u99XQ67znPc95z3ne/+rTfOe/lfN7nPM9znt8zT1VVlaYQg8BiMAfEgwkgGkSANtAI6sBTcJ9z0/B6vVIXDpcUnAl2gmUg0uRHEFNBOjgOqBVOgqvgrxZA9LF4fSy4CZ7zxUXJdvAKPAJ3uRW/6j6fCK6AN2BBIKJmLboKFIABwrnP4DIoATWgy+Bzo8EikAFm8blJ4CE4BXaAP0616AFwXZBsAhvBRHCIW7LLz2cb+QfOBjNAhfDaNu67A50QPQiOCMeFYDK3pGo/oz46D2SD33yOBuI9EBWI6DpwWDjeA9aD1gC6Vzc4zy3czOeSuO/aEo0D54RjGuW5mnPxggfUTz5OA1vsiFJH78853eajmvPxmu+QL3Krq6tHqIim8ERO0QC2au7FHeHOxYD9KqJ7df2yXXM36Ho/OM9Cqw6WER0lTMbvQZHmfrSAs5z3BWtkRJcKLVtgMj86HQVCvlxGNEXXf4IV9aCW82Tc/nAr0XjhdtRqwY0n/LcfGGcl6nvDOy34Ua+bx01FYzhvDoHoNyGPlp3wu7TQRpiVaAfnQ0IgFyvkrVaiDTJ9xKUQr/nRSvQt58OsRp4LkcR/22REK4XjJUGUpCdiAucVKPL+WYmWCsebgyi6AXg4vyHzZPrAhRnFdGEV5WbQBL+d8+9c9kitnvKEc3kKZbTdoJpsOOcncNs7ZEVLeAVOMUVXjrgxgHYLRWOeynqU6pocoXjbB9a6IDkGFAt3LBut2a4iSvFSt4Cm4mu1w3NmORipE7dVhR4DFzin/aRrXDqHBSi5EDwzeKjkY3mXY0e0mytD36LWwx2f6vNkG4JUuF0ED8BQP++RkjXagKCJNwvsEvpsAj8YKnn+izX5TtqfmgsugS9gk8QPItlMszd4LLYdaUvmDPAatDwtsut4edip9ezkjQfTeJ4Uo4nvTAvXZBEG16LVWwYGV6EdUV+rp/OGRKLirf8ETnMh55srU3nkK8l6FDdyqWxZwdsz8dwHxaASmLYYH4MyfuJ1G3yPsqyqqD6ieGUeyZIqe1RWsmmQvWU2mFSig8uJRhsbabe1nj3YTj/drQgDLNUp0UDDTJZautgnG2pR+meDjOz8kIsKsivBLz+y+b1ClGVLeXvJSDau14iybLkf2bJeJSrI0iKmhmcSKlMy/wswAM+26JuhLGHnAAAAAElFTkSuQmCC',
            mark: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDI4RUQxNTg0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDI4RUQxNTk0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEMjhFRDE1NjREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpEMjhFRDE1NzREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqsxABUAAATBSURBVHja1FtNSBVRFL7vZYqZkj4jiVJbWIFlUpBRLop+bKUQRRIS7XMjIm3atsmFGynaRAspKSg0DCyIKAtsFf0RWSEqaeFPWGZqZd/RMzKOc9+byXvvzPvgg3hOc893f84958y9kZHKSqEJGWARmA/mgevALDCNSZhijoNfwCGwD+wBJ7w0ktPW5suoFMUiY2AZWMpCIwmet8RTR2yw/T7Lwl+A3eCIKgNVCCZRxWAFj2hE0TsLmJU84p3gG+6MwASXsEEbhT6Q+M3MfrAdfGlacC5YDW4XZkEdexZ8BbaCwyYEl7PYlSI4UEdvZdFdvqaLDy+dCtawUwoTyKm1wFtPe3k46vGlmWB9CMUKtql+tKoqU5XgbLABLBThBdnWANHZyxVMvVbHQUPYQTbWJRrpaII1W5skYu2iayE69X8E14R8Gseb3jV+BZeH1EF5dmQY5XKvgq2gQhf+crIwBs5obKcaonO9BB66ggpKBB6D7x1C8zhEPQiuUdjeStbSHG+ESzSEi9/Ai+BlDv6do0op4X3wPPhIdUSGUS6RCY5wIqASg+AF8KOHZ6kjbrB4laiE6Iib4GLFWc9v8BKvVz+4DX5SnHAUuwmuUNyzD8Gv//H/KN/tUGxLhdNpxTh5V4mnLr+RUzrJbf0BB8CbXN6x4x04zcGPChRhWseQYIxEbQF4RKHYH+yMnDgD7uSQlcRv4/zWbTkMKi4ilNmndKni0R2X7PlbJOHgapffvyu2qdQygqqL+YpfvsplxuRKAh0KRH4qrMbIkI9pnREV6gpvzrW61zGljkqefcuinchRbBPZUJSiYXQtnAYPgKNivgQb8+GRadat1WBTfgqHdrqwMcHe3iHZc3dpmHVzYWw0wHz3CXhXsnYP6cqXSXBWAGJfg9clf6vSOAhZJDjNsFjyyNckjooioiMa204LQvADyR67Dzymue20qGGx5JWfufy+HjxlwgASPGVQ8ADnx0tSOA2BhhumTAvul0Rlpr5RzQkeNyjYbXTzDI3uXIwfdUnNdMLNM8cMtv8lKknjdEZeS8I9g+0PkeA+gw3uAA9zPpwO7gH3G2y/j9ZOD28XEUONHmeaBmnsoRGeMDzKQaEvp61twvKOVCQvMNTwBzF/ZMGqQmwy1O4Le1Whmzd/3dP6kZg/pmCdxKGTOdUG1vEsa1wouYzwWtYJKszdEYuPHdG/bwmPh9CWgR6qWNoFW72tE1T5+CXpiM+a2+60x9IW3khCP1XIFPLPs2s0ttvP2pYIpunVrrHhdMla3S301K8stGM6LyyjFee2LCoVU5hZqLHiQN94qAY9yZUW6oATGp3lK4hdVEZyC9rJi9KhLx3fiEnYAaZuzLCWJfmwE8NuDyYhWjG6w14EE7qsfStJ0Q2xXbKKhwwtYG8Siu1l24VfwfS5stlwvrzsfJdsjnfuMlERj6qLTUkimmxsgti4Xx29VC3peFFjyKc32dYIsWOJHnTuw/Gm93Mux2wIm4MCr0DspJeH/RTPSPRVMX/OKugD4gv7rMwbqxBs37LeiWCuACxEUMLgFQArOCEPbuKShzMRCOSSh4WX3Nuqr/E4k/fQXOOxDHrN9HtRK947Q3tRyw4y7B7TyFU8v/gnwAAbSxgQOZk8HgAAAABJRU5ErkJggg==',
            bulb: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QjI0MUEyRkQ0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QjI0MUEyRkU0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCMjQxQTJGQjREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCMjQxQTJGQzREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pgz+BRAAAAVmSURBVHja5FtZb1VVFN49vaWlTWmhFOsAFDogEgSNhVAe4AV4sfKAjG0gERNDUBmi/8DEF001ThitGgISo5FQDETgoZjSlLGEIQzSURpaSYQWWqaS8i3uOsnx9px7zz573d7b9Eu+BO6w9/7u2ntNZzdF7fpXxQnpYDE4BSwAJ4E5YAa/R3gA3gdvg91gF9gBNoMP/UwyWJmvtaiQsEgSNB+cC04DLR/zZ4F5YJHj9ccs+ix4ArwjtUApwTPBJeBLYIrAeKlgKfNN8AJ4CPw70YJngRVszXiBdsnLTBK8H7wy3ILHg6vBV9TwogTcztv8N7BnOASXg2scjicRmAfOBneCZ3S+mKLhpceA68AFKrlQB/4Kbz3g93z4AXnSbUkolrAYfC9l980MKcF0Xj8Ep6vkxYvgBxCdbSqYLLsFfFYlPyaD78eytBXjzL47QsTaoKxuE0SHggiuSvJtHG17r9QNS+WcIkriP7AFvOlIFcdxjk0/bK6kI4OVr8Bzn/EjeCK4VmhiKg7qwWNgZ4zPTgUX8o+dJjD3eoi+BtG9sQSv5vNrChK5VyPxb2ce4Py5zHD+sby1a6KdYTtnNbXqDs6CglQ5VCp+D/4APjLNyGDlUi/BVOW8YTgB1bbVYJPADjkOfikgusJL8CyOZSYgy7QKOp/L4I+GY5TCyiVugpcZDnwUPB+HMHMabDQcY0mk4DwuvUzOba3hojLB15mZEe/tAwcMxp5tp5224PmGnYqT4F2D72dzvl7B3OgSw038Aul8zSl4rsC2MxFLRf1zjteeF56D8KotOItzUBM0C4olNAjOYWM6tnVaiM+uyXbu5TMcCUoVF3GoqnP5jJfYv1S4b+U1T9BOC2ktCglY1ytObnaMTdvpM/CeD7E/g4MeYz40bC1NoS1dYCjYawEvOP5dCG7ldC+oWEKG4VoLyMLPGA6SxWGk3yVTWuAiekxAsbkCRcUki0s0E9D5d+tL7wLPRbxWGFCsEqrNcyyBbaI8KhtKFL51ER1ErBKonp4eCUuoFCSnlKMp+k9wt0+xVKPPEVhnuqVkQI5rucd7tuhTjtcOgr9rjL9ChZ83mWIwZBjbnFjIxUOTh+jvwCMcWjo1xi23sySJDozlkTQEBeXAxVHeb9UUSyVrleD6ngruFRwwjUOPhEXKOXlJFVxfDwnuEq5fSfQ74CqDMTYwU4XX1h0PwTZmJOi70dBFgjvU6EE7Cb7qMxaOdFCR02pxBTMarNw8WJn/yE48mkaB4CZni6cxDtvaxMOmCa+FEp+TTsG3VLgHLIl8rn21SziBCi4S57Gd+5yCCYeFJ6G09W1N0blqaMdSAoeci7JxEWzjmlUK9Kz2Y04p78X4bCbX1dK3Ay/Dui32fyKrpdo4/Lq3fYhV3DH5Jw7z10ZuOxVhZfJmUhfOboAfKb2nBuu58pJAA6z7v/auWz38i2AF1a/0H5H0CM3d51Zzu52XW9yJeEtg0iJ2XG0+w94EFe5lS+AnWPeOH8GE45zAS2ytMiXTj9LBYYh17aVFa/HsUQLXdRMA8kN7vd60YiTbX2l2KBINCn87YN3HQQQrDid0heH6CBBLsfYLiI36pwN+upZ08D/hMjJZQTfmq+300VSwbenPVbhxnmyglPjrWJaN5aW9Kg4KV5c4ORibYKF9HHrO6Sb4uqDrfNdU+NLXvASJrWdPrH3NImiiTq3dGt7idLerdBhDzh/soAKXcCagOP0pZ1RLVfjvEKRbq3SUzvJZbZOoWSVACfo3KvysuIyLjyKDzsUDPjZUyJxWQ589J1yw05HUMWlseqZL1x7ooXsBNwPSHQ6vn8XREelmtrMlB+JxJp4IMADW3jkvqWXy8gAAAABJRU5ErkJggg==',
            docRout: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUExRTU1NEM0RDE5MTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUExRTU1NEQ0RDE5MTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBQTFFNTU0QTREMTkxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBQTFFNTU0QjREMTkxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpoS1jkAAASOSURBVHja7JtrbBRVFMf/M7M7S1toCn1gDbQ8BLE0MZhIfJHSoEaikGB8gDExfugXiUqJBPhEYgJICGlI9IMBNQRQIBEMoLwSF4walUYxFIwfgBYqBNpAFrbt7jw9985sS+vuug+n7N3xJP9sOjN39/7m3nPPObczUt/6JqSxOtJrpGZSA6maFEJhWZTUTeoinSTtJV1KdbGUApiBfkB6haRALLNc6DWkyyNPykkaLCZ1kJYJCJtgYn0/536mBX6XdIA0DuLbWNJuUmsq4FdJbSlGXVSTSFtctmHAzGe3uxcUm0kuW93dwBvdKVCsxtg2JYDrSUtR/MYiTr3swso+AOassptU+MWaGXCjj4AbGXCVj4Cr5QLMjb00VYbPzHfAgawbzFmM4Lw3II2tTH+hHoN54WdohzfB1vrFBVYXtmaWgQbHQJnVBLWkHPG9VKkZcX6z5EkN+fU4FoXevh/2raujA5xtuq3Uz0FoyTrE96+D0XEcoYZmfiwfUxoWYOAjqgcMbTSAc+jgjCegvrAa2sENiO9bC/WZtyGVVuRXEZSOh337emEC8x+a/TRAvqwdaYP2zWZxfDivH2M+XDuLwAfy/i4rcg36qc+yHuXAaN9h+b6Z/1E8fRhyxf2I7XzHP3FYntSYw40Sei9DFgDYtmDf6YEdId+zzOHHI9e9X0dGi9O8+AuM9gMwL7XTH7obr4I8JgcefRHGr4dg3biAkuV7BAemFDNO8df88ztnStVMg1Q1xRnRnk5+I5gGpygdz2WqFgawZSD2xXuwujugTHkEQUo45Oqpw8/vWsHPD07rvpuUp1eJCayHt3GYQOOzUBet+cfIxb96fwg24cq3ezwF9mzu2NFenuRLEyZDfX5V0mkqqWU8Lktl44fakQsI6cPm+TBfnIKPL+OLU9LKi/LroQY67IGIp6Pr6Qibf513FuJpczOsMIKew3obh/tvOdN2XGV26xzdKC/jsXfAaqnjk7G+zP2+P4LYjuXQwh+LByzXTHdGrPts5qN75XeGDbmyTjxgZeZT/NOglTpTS1zLtobEG+HaB6FMf4yyqNMwfjv077Cnv4TZdQbKjCeHJyfC+DBz44UreYzVjrZB/2Enz6ySZWP69zugnfiQr9LqcyvEzaWl8hqElm7mu5b6qU9gnPkagYfmD+XSvV0w/jjJdy2kilqEXt5Aq3q12MWDPPEBjGn5FPqPu/jU1n8aUQ2pJQjMfQnBeW9CCpUVR3kolZRDXfAW1PktMK+cdfahbJuPqsJ2LVJkYkLXw4P1L1VN93RbKJeST2TLGtjq7SqYzufSl6yntHZsK0Ks3CubcE9h2b6YdmRL9utJ3/omGz6y//8h7gfguI94NQbc6yPgHgZ8zkfAHQz4Wx8Bhxkwe1ze8gEsfzWAAXeS9vkAmDF2JsIS2yCOFjFs1GUcjMPs7Y8WlrEVIaztsl0emXiwyry1yPyZwa5y2ZJmWltJS0h3imQavw7nJY+0qeVBOM9Q7xF0tC2377NJnyerltI1vvtVPPYFNaRggQGyxwluuAlU2F2NL6a6+G8BBgA7pmS8qoWB5AAAAABJRU5ErkJggg==',
            seal: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTg2QkI1RUI0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTg2QkI1RUM0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxODZCQjVFOTREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxODZCQjVFQTREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Prml4GAAAAPgSURBVHja5JtbSBRRGMe/2Z11XNpd72YRWhJkZhB2By/YWw9FYReVeop6Kkwo6r2ggsQujyW9ZGkUltBLUWo+dKELlFJU3hYhNSN118veZvs+dzZM3I3dmbM7s/PBn33YneH8ON/5zv+cPYeD26MQJnJRNagKVCEqCyWAusKJGkINojpQLaj+UD/mQgAT6EXUAZQRtBWiBH0WZV/4pWGRB3ajulHVGoQNMlHbe6TPsMC1qFaUFbQfFlQTqi4U8EFUQ4he12pwqHqJ7R9gGrM3pR8kWnASW+584AtSCiRqENulYJXOw8++BEvlUNU7nyCrdAAbzOYqg2Qq9BIVBFykI+AiAs7UEXAWHytvbDZyUL1SgMpcAYrTeMgxG8Al+qHXIcLLMQ88sLvgyQ83+PxMm5FEVdrPGpYgr2+ywDJz+Nr43eGDU++n4NGQi2nlYjrjXy62wP1S239hKVZbjfCw3AZXNlqYOSCeJfA17NXja8wRP1dbYAZKu7p3Tu30cA2O12hgg3ESoXcuT9IGsGDgoL5YvlOl4cBpAbgyN2muCsuNwhQjlGSb1A+sZCoqndZMgNemKFcLN2fw6gfOFJQbeVmCQf3AMz7l3uVV2BYxAf46qRxxr8OnfuDOUbdi73o27FY/cFO/C2YVWAVMePzQPOhSP/DIrAj1n2dkv+f8p2mY9PjVD0xxDhv74bc36uc7RjzQ8GVaO16a1rp7OidgeEaM+Nl+pw/2dU0yWRszXR7ap0Q48TbyFc+x10745RKZtIn5bmVqUuQmJF1g938AU+AdOaa5NXGkcWOrFbZk8NoCzrcYoa08ZW4vK9KwmTh4XJECS5MN2gE+vEqAJXz0qZmJHnp/nqAdYCX8tItBmWYG3Ng7C73O6Kk/jnuhZVD53Uum27SU0qXZJkiOcBw70V29QD/uZjAzxWRfWk0hq/ZvSOOhcZsV1qfyYIrR/4/k4N6MeeHIKwd8i2LpKKuZd0tsUJweO1gK2hGlYXJre3THUGQ1tcAWv0M+Ral87IG1GLoDllW0Okc9cWu4M8qNAdnTEk2xVlPsTjv5/YGtn7j0MMWuFQK0ltli2rtlT8ehK8rs0uQYNnIQvx72oBGY8MTWrHllWE4aw7OgvjPQrMJNKT2mo1npJwH36Ai4m4Cf6wi4nYDpuLyoA9i5qwEEPIC6pwNgYhwIzsNnIHA7JFHDKTH+NR50++MoObcEhPVLbPaFTqsZAhcixASDPS2xLWotr6L2ohwJksaHIHDJI6yXboPAGepmjfa2KLV9HerOYtYy3MPzr+LRC7JRJpUB0rJpVDJQ7VI17gv14z8CDACz/gdRe6dbMgAAAABJRU5ErkJggg==',
            busApproval: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTg2QkI1RUY0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTg2QkI1RjA0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxODZCQjVFRDREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxODZCQjVFRTREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnaiDwYAAAZESURBVHja5FtrTFRXEJ69C6ywKC6FhYKwoIBVFmktak2aVBpiqcUmmhZBTe0Pa2yTPlPban80fSRVU4omTdv0kbY/ULS19KmpMVjTJi1gLeAiIlbW5aFQ5bkgD3e3M4cLRZYle+5jBXaSCWTvveee78zcmW/m3Ksxn9gHk0g86kbUTNTFqJGoOphaYkdtQr2M+ivqIdQGTydrPAAmoLtRc1G1ML3EKYJ+DdU2/qAwwQWPolpQ86ch2BFMNPca8e+kgJ9HLUGdDdNfQlGLUF/0BHgDaqEHq09X0aAWiNhuAUzP7GfiCTNNNCK2+LGA3xVdYKYKYdszAtiEmgczXyjjmAQRrOAHgBlWQSQV/iKZBNjsR4DNBDjCjwBHBviCGy8MjYCV4fGQFhYNCSEGiNKFQog2EFx4rM8xBK0DdrD2dkBV1xUo62iEC/Zrak0lKECtkWcH6ODxWDOsj0kFE4L0OANBC3MDZ7FFeSgqmf12qbcdSlpq4HDzWbYgiiZlLB5cSg4YoBFgi2kpbDVlQGiAPOfpGuqHT6wVUNRYCQ6Xc+oBTkYr7TVnQ5L+DkWtUocuvsNyFBrQ7ZXITYoIuePBZXmKgx2JAYeW5UNm5PypAXjDvCWwN/Vh0AnqVZPBGOT2peXA2ui7bi/g7KgUeH1hJgga9esOusc7i1fLsrQg95mlCfiyxCLQe1KzIVFv8C1gSicF5jWquvFk7v0e3lurEXwH+Mn4eyWvshKSgt61Ke5u3wAOQ6KwNSHjtvPE7YnLGWNTHXAeRuVgjhuVtTfC+rIiONfTNuFxp8sFb50vhW1/l7D/edhcbmyauoApQBFd5GJMN/uhHsnDM5XfQ/ONbrfjuy+cgq+RRhKXHnI5uMZeH2NWFzAVADGz5nBds9qYzLzi+mAfbK/8jlHGEfnKdgYONlWx6LvXTLmcj95THCFiohpgqnqkyM6UVXAfXmvt64Dnqn+EQacDjrfVQ0H9b+z4S0n3wwMRiZLGXhEep6KF50RLzp/vp61h5eGZzhZ4Gi29s+YXViI+hlXVlvilkoNXetid6gGWk4ooyHyQvhbm4N/yjiZm5RWGOMbU5EhCiPdz4q6HI4P0Xp9L1qvpboV+x81xgSYVvsRnl8rH/Lh0qOy88v+EBAG9KIqLVBh1oeoB5klHVMS/UXvC43H7zQF4ofont9834iLQM++t6DnmpFrHg2QJRvQsY5Jbbm3oa2e1baRO7xYTKO09GLmA6z48BT034BuOIa+tTLVxYdojbr9/1FAGH176k4HdvyRH9sLytIG4g1bbQC9MNaEmoGoWJnc0hcz1uid1BJ/jAeetQauio2l0LLL2WNFi+sqJXoTkxvsdWytH64cb8NmuVlgV4V0BfrS1Dgov/u558XCi5Nrjhejnm4uyvJ4TUVLVAP/RboNnF6z0rhtiTEHrOlgnc6zY+joZnaQGwnheLmDYyjLyBS3qZasG2NJ9FVr6u73i04agYKyb3RnU6Y5mBjguOAw2S6hpxwr1sHka99xBi1LAty01UyZglXDORVI9fKCxStaOgFZs+Mlt/HViUKTdCdUB9yBD+tx6WvJEzUgdiU1tmifPnT+1lnMvvOSe1he2v9jzI0UCBS2jjhmGWMlga3v+hSL0NJ/0tEiGMPq+bDnGorCvhaxKWy9S9ptk9aWpbbMLa1qePpRcoXu9ggt9GVObpLpc7gSoa/F2XalPQNM9dp07DqeuNUgeQ5G9pW+aLehix9zqXqXdmFpDP189L2scxXYPydL5FcXMzZWW2p42yC0/IMuyo+Wn0hvi1Kl4Iv4e2JawTPaGOOXZj7G4KG6qVnRDnHqmir/nQTsC1CRfh1x5vj6c69qL9uuMzR1psSj9ysMgAaZaLVbNYEN94+WGONbTTgwxgFGnxwUJYsd6HYOsxqZSkb3U0t4I/0jM715IMxUPNWoDplcWSKERbrdYKGiVgv/ISQJMr8s7/QAs+zSAAFtRD/sBYMJoHcnDr8Lw1yEzVewixlHiQV9/PAV8Ld7pIi4Rm2080yqG4Q8inDMM7A4R24TUcj/qOqrxZ4gbb4bhjzwm5dI/wPA71MXT1NpOce7UDj0w/qCnriX5e774oI98ikcDGKlhMcUAEvdsEwnUSTEaX/J08n8CDACnnxT9097bcwAAAABJRU5ErkJggg==',
            crm: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NzcxOTk5RjI0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NzcxOTk5RjM0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxODZCQjVGMTREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxODZCQjVGMjREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pm/P028AAAZ8SURBVHja5JtrbBRVFIDPPHbb2kKptKUWSkMBtQ8KRlowEBUUBEyIBbWFgIk//KOiqEFJNERNiDTGaEP9oVESQjCFIJqKRFFaqRpU0EJfQnn1sRRKK6+20H3MjOfcmZ1pu7vVfXd3b3JSlrk793733Htes8MNbH0IRmlTUdaiLELJQ0lDiYOx1fpRLCjtKD+h7EG54Kkz5wGYQLehPI0iQGQ1WYPejNIx8iLv5gsrUZpQ1kQgrJOJ5t6s/R0V+GWUr1DGQeS3JJTdKK94Ai5F+dCD1iO1cSgfaGzDgOnMfqZ1iLbGaWxThwK/p22BaG3EVu4EzkYpg+hv5HGyeQ2WjwFgxsprQUWstEUEXBBDwAUEnBpDwGlisGNjbkImCDPmAT85H/i0acAlpQIXn8iuKYMDoPT3gtxzAeSLzSCd/R2U613BnI6ZYmkl8OZBADFvMYhzVwGfmetdINz1NziO7wdHSw1+kAKvgEADC9Png3npBuBSJvt1H+XaRbAd2g7Sud/GKLApHsyPbQSxcFlAJ+ho+A5s338EYB8MmG/yf9XGpUH8M5UBh6VG96R70xhjApgblwrx6yqAnzQjeBED3pvGIIMXXmDcxnGl5XheM4OfAeAYcWXlbMywAZuXvwZ8+vTQxYY4lnn5q+EBFmYuALFgScgjB7FgKRs7tMCCCcxLXgxbuMTGxjn4tGC++dp5eKh4UG50hyml53EOxSC1/hoaYKn1F7iNEqk5Ykw10eddhYGAOLcEBEwKwJwAyq0bIHc2gOPEAVAGrhnb/+6FINyz0PUGdhsoN7vBcaoOlKudxoTyHwU+Z67R7cgO7HfFuF60GviMmfpn2zfbQnCGpxWBefU7wJnvGP7/OUUgzisF6743Qe44qQcN4izPEZjpwWex/1uYKakxM59577D+cvsJFl7q/eevYcGOr8Beb2lufLoBa7uNGvicTZj+0mcuPgniSt5mWh/ZbD9W6uL4Yx8GylacgQjmFZuYIRqePcjqBKfONsZOzlBhZUfotrRpfpmuWeuXW0C6cEw3ZDJmOHFPbAEuMQXE3MXgOPnt8ESAIIcySTYwPbAWQ8aJTJS+HkOzVy3AT7gLhCwDWMgqVK/1tvsc8HgNzFwSDdrTpsPq1hvPo3X3RvX6/3FZTq1KdrQB10e4AjvmxqeAz5qlLkb/P7q25c7G0AFzyZPUQbvPuMneHSDhmRs1YHC2hGQQ89T6oePPrxmgi/uzNDJgApVaavDfhSxNdDt20IwWr33FYfN+sKInXdcItWWr/dR99QOtPuCWp60st9cDPzELpLa//KqEeA2sDPYzw0TbzI3+jXoVLciIRSFj5ewnZOaCkLeYadC0YD3Y63a4AlvoAaCCGp6D/eqNRQilH5a7WtD9FIOQPQe4hPGg3L5pKD+rAOLXb1fhDr6PPtmz0XIgdAKdT/TnvGaMXBe3D20FGqi0bPTnC7RFaPSrGOC1W6LAQi/prHpXT8qpOmlepqVu6J6k0z//117RY3EuwfPTWdnSwHYEBSTkqqSLLaHVMFliqfkwCPmPMC0nbNgLCvnfuERj6x6qGKZ5j8jOLc97ngadcbhvJbPo8qXTbDFDHlpaq7eC6co5EIufYj7XCcsqjTWfoHbrDChrv8esSr6EbidlMnCiCbg7p2CIacGF6mP9lb5edYER2Pl96exR9Z4I7Wum5l/VEledw+CAgEmjyo3L0Zs8OMM/0qoCkdN8z5YwvBTmrAjLpJn19/Es+wys2G6xZ0Xi7MdDDHvAL8PlVwGAHoXIl1tDBktj2X6oDGPFA+Na657NeI67gg5LY1irXvf7kYvfJR5l4CoM7nweA/qzwdMs+l8awyWjCldNiyYyuPMFzH8PBv7M1lfD4K6XAgLrvx/2UP4xYRrIp2b7p1XMt+2HPwbp/LGALmBwHohT3Tj3YTDdX8KyIa9ALU1gP74fQ9gjkfFA3LVgkMGyK35KPvDpOepPHijLIuODsTSVdeTeNpAxKZDOHGWVzKDOB4HJ7I2130AHq9nIaPVC7LQeAm6OIeAmAq6JIeBaAqafy8sxAMteDSDgNpS9MQBMjG3OSOsNUN8OidbWrzHqoSW9/fEcRYlRCKtobB0jY+kqUF+IkKMMdpPG5jZ5qEApQemLkm28DtSXPEbNlqpB/Q11VYRqW9bmno/yhbvQcrQvD30Vj26QjmIaY4D0FO6KFkDVatb4vKfO/wowAOHXXZ3Gu6ifAAAAAElFTkSuQmCC',
            code: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NzcxOTk5RjY0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NzcxOTk5Rjc0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo3NzE5OTlGNDREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo3NzE5OTlGNTREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnvoxhgAAAXbSURBVHja5Jt5TFsFGMC/tkCBco+bAcsGG+GYCENYcBfJpnFEMzMEtsKiEd28MXFo4tTEP9BENxf/MjGLmRkOBgLOHclwzCEjTjZgK8wxzsK4GVe5ofX7Hq/YctbZ93p9yZe27+r7ve9+7ROEl3wDK0gA6gHUXaihqB6oYjAuUaC2o7aiXkPNRW1ebmPBMsAE+gXqS6giMC1RstAfosoXrhQuscPzqDLUVBOEVTPRudeyrysCv4taiOoIpi8OqGdQM5cDTkY9sYzVTVUEqF+zbFrAFLPfsxuYmwhYtgBN4GzWBcxViO1LNXAgagqYv1DFCRSysEILAGZYhWxTYSmyi4DDLQg4nIDdLQjYQ2iEvTGXYmPQZCX1j4QfoveDm409r5nLYBLt4sfoTzEpvEEbDNhT7AAhjh7Me19bR8h7KhUcrMTmCexr6wRFcVJYa+cMk8oZZpkXXgCC5trSvAP7oDXzYw+AI1rz7Zrz8F3zzfl1/ngBCNrF2tY8gMmNC9GyBPtGdTHIhrsgee1mrW3I0gWxBzlzbyGfliUQicgG3r97AWQj3VC6LYMBXOrCnOPI0rwAe7GWJQBy41uDHXB922sr7kPxXRArZeLdpIC90bJFcWmMZd+q+QXqyLJPv6pjCEjgyPpYvZ6PFdeWnYtHG3jnznm4jZYt33FYp7sMV3sb8eL0wI1+uWkAk2UL2eRDblyLltUVtqSnATIxzrkQvQNbC0Xgh3F3OjqJgT0qu4SWfQhl21/XCbasv4UzWL0DP+e9CY5tSgAJujDBffWgDH7va4aKHUdAKFgZd1alhOLOe/DpvRJOc4pegRUzU0y8qiXBYwO8FxS/KizJ2Ow057B6z9LX0ZpZtZfnP0e5+IKVQLevoGYkF2uvE8f99GMDE0igveui5Re77sPHdVd0Pk55fysMTU8w70MdPZmsTsOE0QGvk7jCr1vT4ZOQhEXrijvrIEt2WadsfLi6CN7E+qyZ3fMROtiBmxsxIs/0Zz97nB0p5oIl7rDXO4RpG0v7mrTWPxjth5axAdjtGbxq6emeVEAzbruH3VYstIIkvwj4c6ANuiZGjAN4BrPqpe56Zpyjk/Ozc8JmQRu6AaFbxwYROkhr+W/YVCwsPbTt6OwUxK8JZD4LMNHt8w2DuzhgyMeHDA+sWTftMTOn4NQT7uQFF7vvr2jpBkU/vHw7f8lj1Qx1wZRqFuLcAuaXJaIHkfXpghgFMEnFIznYYMNBFtnjFQy57XcWWa9nchR2eqyHTnTR/A7ZsseqwvbTxdoOIpy955eRq9N+fyt6jacOn2y8AVPKWYh29VtyfQFCto0PwsjM5KrHyq6/Bs7WYiY/qOXz0N1MQ3Omrfp/nSf9A0AFRirHI/Yuiv9TrZVwoqHccI2HGF05SLKGsUbGuhjYIHHTGzDdKKAJS1NeCdwCRzdu59elX/AJhRcxXiMwSdGwoCkUa42jj/QGfejWOciJScbv+jem0/yfhBmlEo43/MEPcCIOCdQ2UsmpHGyHekUfyPF90+gAdEwM6921pZV58HOsVMt7DvpH8gecUVUItiIrmJid4SWWlSoVpP51Fi5sPQQeYsl8ZeA1S/MFq5Zx7Oz238xByz7BVIMf5VWGHw+zw56BSGcfUIL+Er8QJ2ualansXcF29NvGCuOow+7YYiZq1E19C92/JmCDz8MiHBM/2riT+UGMSyHPyQyKN2ynRXcyTm9Jgs0aJYMroQmKanCUix+kYdY2iIWp0eADdqGl0wOiDGNh+nXg4fgwMyryJXSnxYstTbwDH/sPt3KMRcilJ8FyZIqA+ywIuJeAay0IWEbAVy0IuJSA6e/ySguAZR4NIOAW1DwLACbGFnXjkQVzT4eYqyhYxvlOi4bLDFSVGcKqWDb5wtbyLMw9EKE0M9gPWLYle+mTqPtQR8zEjaUw95DHisMD/bIVzl4VU7S2kj33MNQcXXtp8vdUNtDVj+LRATxRrY0McBq1h22gStls3LTcxv8IMACEbd3/Li2JWgAAAABJRU5ErkJggg==',
            finance: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NzcxOTk5RkE0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NzcxOTk5RkI0RDFBMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo3NzE5OTlGODREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo3NzE5OTlGOTREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoSDwiAAAAZsSURBVHja5FsLTFV1GP/u+8GFi1wgHwmEk5cEOnNpq4hes7Ysexg2V2a52qwRpKN0bbblhGYZ5taaulojtDbTlSzLJbq1wgKjBBUCJBIxQOHKfT/7vnPPjcu9514uFw5x7v22b/dxzj3n/zvf6/d95x4R1PRDCElDfQa1GDUPNQVVATNLDKiXUf9CPYX6BeqlYDuLggAmoJWoa1AlICxxsaDfQO3x3yjm+MEq1BbUtQIE68VEa29lX0MCLkU9ghoPwhcN6ueoZcEAP426O4jVhSoi1PdYbGMAU8zuZ3eINhGx2NJ8Ae9kXSBahbBVebN0Or52RZkrB8vemQSyJAbAer25RMySiliRYgKcH0OA8wlwcgwBThHPQG7Mp8jFEGMSc4Cl0+VHazOUsHq+HO5KlUOSXAQ2rIodI044fsUG+zrMcPGGc5poV02/m88TLNNJoeaOBMhKCN54OXEFH1w0w9ZmA3MhBOvSRakyOP1AYkiwJBJku6/nquBIkZb3rMLb4WerxHD47gRQSTz9iBnNaHUFOpPe7gbvtw/PlcOOxXHCBPxWvhp0itHDH+u1wa3HhsDhg/nDNjMsPz4EvtehNFsNmRqJsADHSUWwYYFyzHdPpSkgQSaCAx0W5rMFLb6z1cRYVOLTlMpwRS8tVAoL8L03yUApCWytK5fEwY4WIwP24z8tcLNaDI/PD+Q9D86RC6ssLYjndsn7Z8sxgUnh/Qtm2NtuZrI3lyyMF5hLx8sCrdsy7ICyJgOgt8O2340wgsnqm14r7Lpg4gwJQVl4wBKYjdUIIiNOwmiL3gEPYUZePEsKSYrAaz5odQkL8B9oTX/pM7vg6GWrpxTZ3PB9nw0arzkgG2v0I/PGxux5vVNYgM8M2qHf4oJU5aj1chDY9gJPjX2xYQTWZyrhTiQmGg73Pfq3VVgxTFSRSo6vUE0m5lXEgsxPlDDvlyZJA9x5H1u6BEU8Pmq3QNN1B+e23Us1cLtOxrmttNEABodbeICJRq46pWeys7/cg3WaqKe/VPxmhNpu/tyZ9+bhCiaqFd8NQzV2QvYQiZeS1MqTenj3vElY7eFctFqOVgIGrLHNQ44xrd4c3PYE0ssVyWhdTGYmDPRO7IfrsB+uv2oDhxumRSYNmOrrugwFPI/ceXnyaFwOYPKpwsS1t83C2SUFk3Ss03RxGq/ZmYtAx89C5kVl7R+L6/8DTNbclKWCl1FpghFMWpFkPPvTCJwNksB8gVYh116TrmBuBp3D2D/RZ4dXslVMj0zXbGW9HtpuOBiuTZ+/wvI1bHPzC7gQ2RE16yXpSqazCUcofolO7sIY9T8ZNRl0vG3YTqokoSllr8nFJDvvbkRclmF7ySvggSd1kKyILNed7rfDpl8MjNW9s66qJRpmDDQLP5CnpCjFIAmTSlOoKA8O8gv4ncI4xhqRCp3sW0xUh3us8CtaqNfkhOs+bkmNw/YCNWzOHf8cW84aOZuPKQVMLtXzmC5sdw5XqHuicc8PmLE3IPXsejSJiWsuMWI2I4JyoHPijGzCy76K2bK2e+qpH7WUNBB4Djm2Gn26y8CdkX8csENh3VBEYCNuHqiBp4WFIxSvn3ZaYRFy54JEKXJqEZOofCciDoxFopN0MfcjEDFuotbRP17fRCZW3WYG1yQKacRl6cR9WmaCEY5QDd3abISDSBvHq8lUy/fc5klkvt3X+p9HpmRYHzFgGqnWFWsn9BsiEu24aDuCpji0+5yZhiQUs/PUo1FGTO3tc0aGwDiniIlF3A9Tpm3DxVMDT0ajmCIiQJYpRbKg4yhd1PrmacObV5FVX2gYLWEzglrSOLUsR824WwMu0CvU7xJD2pzLDTyUmNDyRFL2TDJWeQHsvS0S7H4QzaHLsZ6+lqMCrWx8NkElaeMZA1wy8Dfi4f1mGkkiMqiKPDW8ilbnmkhS/S1vMsAnGBZ8L2ZaAHuFKGk5w8MVcItGAkPIsD7rskAlcmwqSYJoD4UmFIXWGMJrI8CDMQR4gAC3xhDgFgJ8MoYA1xNg+ru8KwbAMo8GEOBu1C9jADBh7PbyvgrwPB0SrWJgMf43AKCnPzayE5hoEzeLrcd/4nEIPA9EuKIM7BYWG+eIpxp1NY2YosSN14HnIQ8IBpjka/D8h/qQQK3tYte+CLWWi0uH+rHvo3h0gFQaTswwgNSI97MEqp7Nxl3Bdv5XgAEA0BRixx+CbYwAAAAASUVORK5CYII=',
            accountManage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjkwOTNFMTE0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MjkwOTNFMTI0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo3NzE5OTlGQzREMUExMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyOTA5M0UxMDREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pg8xdtMAAANKSURBVHja7JvPSxtBFMffbkwTrIkITVEqRjzWHP0HxJMHhV76ix7t1VKkWPyBvwhNQS2992ptRSh4FKk9eRBPEkEEWyOKWClqNlWTmN2+NztGY35Ia012Z/fBl+xmZ5b5zI83b4Yd6Vd7OxSwOtRTVDPqPsqHcoGxLIbaQkVQ31CfUT/yJZbyABNoCPUQ5QBzmcqhX6M2Lz+Uc2SgGgijnpgQ9oyJyr7CfwsCv0B9QXnA/FaBmkC9zAf8CPUuT6ub1STUGGfLAKYx+4EnEM0kzlZ3EfgN7wKiGrG9PQP2ox6D+EYzjl/msLIFgBmrzIMKq1gzAQcsBBwg4DsWAvbJBoyNb9JuyWAxK/uXTN5gEMoCxRn6p+EwRHt7/6urtpTZwPYYzrXFMDoK4HQWp4TJZOmB1f19u0sL1aXl6mpwNjYaosBJnKbU3d2bBfaGQiBXVRkCWN3bg4OOjpvt0kaBZWXx+ewxbAOXBDiVAu3wMP9YPDhgacQARpAYOrpofz9o0WjWY/pP6enRAxhNMz/w8fQ0JBYXIRWJgDI4CNrR0TksXkf7+iC1vQ2JhQU4npoyP7C7tRUcfr++vFtfB2VoCLREgsEqAwOsIsgc9fXgbmszfix9lUleL3hGRkDp7obUzg6crq5CDO9ZBayt6bC1teDF/6TycjGcllxZCZ5gMD1vJpeXmRhsTQ14hodZxZhitVTItJOTdGuy1s61qnI44Pf4ePq2Ap2b5HabtEurKot3CzrxrS2mi3nswMMsLUxOqGpyMnOFs7QEsbExvft2dYGzqSkrj7m99GUAlyvjutieuahOiw1RRTkPSiYmID4zk/FcaKdFQUcqRx7badlOy3ZadpcuWQtnRZIYT7taWtLX4gM3NMDtzk7Bt3gMajaw4e2ae2AEHL8qUXxurqjRUKEoLj47e503JOh7aVqY3rNIj96mFl6x0BAOE/BXCwHPEzB9Lq9aAJYdDSDgDdSUBYCJceNsWuoG/XSIqBbjjOl5mE5/PKdZTkBYjbNtXg48PoF+IEIVDPYVZ8sZab1HPUApgnTjZ6Af8igYWtIOW4DXihlbW+Vlp69wPmZtTvzFUTx6wV2U02CA9OXaTx5AzXNv/D1f4j8CDAAcDQJTvj73SQAAAABJRU5ErkJggg==',
            purchase: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjkwOTNFMTU0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MjkwOTNFMTY0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyOTA5M0UxMzREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyOTA5M0UxNDREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PhtsmnUAAAQ+SURBVHja5JtLTBNRFIZPyxRqAUEEjAmiorhAjCYGN8ZEXLjExwLRuGBjom6MC4NRSEzEqAvjI0ZjcImKJEZDFDUS0ERiQoyvlKhAsFRQeRZsS+m0TD2nnWIpTHl0pjOd+ZM/TWc6k/vNvffcc+/t6KB2ACIoF30IXYwuQGehk0BZcqB70T3o1+iH6B9CP9YJABPoJXQpOgHiSxwPfRptDT+pn+WCErQZfTAOYYNMVPZ2/jMi8An0Y3QqxL9S0PfQJ4WAD6CvCtR6vEqHvsKzTQOmPnuX/4HapOPZckOBL/JNQK0itsvBKL0aP7tV1pSFonceQZZpADbYmsv0fFKhFRUTcKGGgAsJOFNDwFl6BebGUipRDxoTs9ALKgpMULXJBMnM9BzFPOqFsrd2aB/zKj5UL0jVW5JnwPqjQToDN4uUn7ssGHiU5QTP7cg2gDFBpy7ga99cgueIdX1qgrr68AXzOLT0eyDbGHhWRRkMnCk0TZ3PR2Dqz8qdSdQO+KK5AdVoZ0nG1Pe2YS98lThwebHE3Y5JqOmcgEE3F1tg6rFjpZmQaoh93/0w4oWtz23S9uFw0dP6ZJOnCecvIl6IknjIBfxmwCMP8EeZgOt73DIBj8QeeGLSB096ZQKmdNLDxRb4aR8Ldo9PHmCCjXUO/cDiXtR1os2WYhm4/mLNPsMa1gzwI6sb3JxPZuAYBq56q3vR18ZdDfeNc9D0m5UfeAz7lcU5KTlw1RenP5eO2Wxprma9Jvl/unerwyXacOVEysZfLLQOeqK6j6jAlHHtXZUUAjyhuCUfURfxwjOugjTlLQaIXsOh2rnCAJ12cfr1EM57e8ej7x9Rz4fD1bUnA9alSFOzR9vscAcn/Ypp0qSaKAsUSduzDMrqw6Qb313Q9IeVBLixj1Vekw6Khqf0RPGWfaw4xo+w0ReVkQLWxOjg+Aajf4iizRwaPys/Oedd4PI8I5SvM8JSgw7ahrxw3uwUBVayGn65Kw12r0ycduwzRvBtL2zAzhFozxaaoHpz8rRjFJ23NNpg2M0prw8XLWdmwJI2L2OgJCfyRiVt4VSGrHEHlWPSw7F8o/ISD9LaCENSaNo5m2iNW2irhvauFAlMC+SLOUfqwcAkNDGY61rZgN8Pe/3rTeGiRfOGORbdRjEw0YQjXDY8frvDJUr5EmD/qXNiQz/+yfqf5MolenBgtllrmYDyd3YYn0clvcK57qSPmjcDSXiT5n4PHGy1Q5dIKapk47BSRRXh1hAvS8BDGgIeJOB2DQGbCbhZQ8AtBEx/l+c0AOt/NYCALeh6DQAToyWYeFRA4O0QtcrBM05lWvT2xxEIbOirTT6ezRqeWtZB4IUITmWwp3i2WXPp6+h9aLtKmvFhCLzkEXHy0ACB/1DXxWltc3zZN6Lvz5ZLR7o49FU8ukE22qAwQNp7GeATqBY+GncL/fifAAMAHb10zCUpm6IAAAAASUVORK5CYII=',
            hrApproval: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjkwOTNFMTk0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MjkwOTNFMUE0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyOTA5M0UxNzREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyOTA5M0UxODREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlXAeG0AAAMgSURBVHja7JtNaBNBFMff5DvRimLrodAoRVDbeOvBmy1Uj6IINhXxVk/F0oMouZZWixQRr8WbEnMRCl5E2py8eUutILRRtIopVkgakm131/c2G5vUbLYbXdjMzoM/IdnZZH7z8ea/mx22NX0emkQUdR01hOpDdaGC4KwooL6gPqHSqBeRRHrNqDAzACbQB6hrKC+0VygEjbqH4J/3HvQ0OOESKoMabUPYKhPVfbk4MzhqBjyBeonqgPaPg6hnCD1pBDyCemTQ6+0aDDWH0CN7gWnOzusFeAtimkfoaC3wfX0I8BrENlvN0sfxdZWzoWyUvXsJMu4C2Opojvt0U2FbeE+eg8CF28COdDdv/vUVkF7NgpLL2lmdIaKO2dakR3sgeHXKFFYr230GgvGHAL6AncAx6uFO23r3FLo4rx/klTSUF6YB5O3GaTTUAcGbT8DTeQK8PWdBXntnV5W6PHZ6Y3bgsPYqf80YwlKopTwo3z5U3vjDdvZwwA3JytRLC2CubBcaD9XSJLg4Ad7YMJ5p3laMMi4mLdiRQG0yh7WyfkwlHsyh2yVQFdm8IqoCcuYNSK8fWwL2WW0h38Bl65Ybwdl+lxt/aN/fTnWxCtzCkHbS9QUTc1gA/+scbvlSZSO7ay7+spV9aEOj/ACrW5tQenpLy9ZGiSo8ngIWPsQL8E8Nlryyp3eg7pj88S2om+ug5nP8AO8O3dMQGB6v+6z86zvICOxY48FCLdwJUhRQpaJmQjSDUdv7UgmP7wALRLBFrOdQtVSwt4et/kD9+N02dFxagzgxaUUSaUctM8WZQbEOC2ABLIAFsAAWwAJYAAtgASyABbAAFsD/G1h+v+iYyrdSF8v3tMSQFsDOBy67iFci4A0XAecIeNlFwBkCXnQR8BIB0+Pyigtgta0BBJxFpVwAnIok0tnqsnQXKrtDeI2CzvhnHabdH2MoHl0XMY1Vd7jUGo8kapKz+UywdxA2aeS06KGnK6g8J8P4BsLOmVnLBag8Q51s095W9Lr3I+zzRldLzU6u3YrXjzqG8jsMkP5h/6EbqCU9G68aFf4twABoL9kZTyI2dwAAAABJRU5ErkJggg==',
            party: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NUExMURDM0E0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NUExMURDM0I0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1QTExREMzODREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1QTExREMzOTREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pg9+9egAAAZ7SURBVHja5Jt5aBN5FMffzORsTFK1iqJYVBTUIlgvVBS8RVEQxd2uq/+oIP6hCC5bFEG8qui6WlH8w79E167X4iJ4oS2iouCFtOCt1Xq0tdI2aXNP9r1fJm0mmUnTamcnyYNHaTNJ5vP7veP7fk24hkWLIIkNQv8FfTr6SPQ+6GbQl7nRa9Cr0SvQ/0Z/q3YxpwJMoHvQl6ELkF4mStDF6O/jH+QVnkArUIlelIawUSa69yrpZ1LgDej/oNsh/a0H+in0jWrAP6H/qbLr6Woc+h8SmwyYcva4dEGmGSexDYoFLpFCIFON2PZGgfPRf4bMN+o4+bwEy2cBMGPlJVGRLTadgAuyCLiAgPOyCLgPr0Nt3J1m4iHLLOuADdq9kwHM06aBefZsXGYeglVV4L93D4IvXmgru3A8DGv7jhxYFiwA64oVwFksEKquBs+ZM+C/cwcgHM5AYMmEgQOhx+bNIAwYwH4PVlaCu7QUxNrazMhhIT+fhXXUQjU10FxcDKG3kcMJQ0EBOA8eBGNhYfrtMJeTA8YJE0D8+BE4hwNM48eDec4cCDx6BK49e3A7g+3XOp3g3L8f+L59pZUIgfvAAfDfvp0ewARg37oVDMOGKT4eePAAXCUlMmjD0KHg2LcPw0Bog3Zt2waBp0/1HdIE69ixQxWWsWCRioVl+fv6NXguXIiJfwFsmzYBn5urX+AoLMtVFfOePw+tJ04oP3buHIRdrvYbw9fLWbVKn8DfC0sW9nrBe+WKXAdi3xaGDNEXMN+7NzgwJ5PBBh4/TgobNX9FRcLfLMnPzbUFZrC7d7f1UjUzjhkDloULO3w9alViXZ18lydNAs5k+v+BObudwfL9+qV0fc7q1WBZsqTD64Jv3sjfB9WYkKQIagYcdrsh8ORJp56Ts3Jlh9Di16+JMnzwYB2ENOrelmPHwBdXaMhIG4sNDV2CDns8iTeZl6eToiVBUwWOGv3uOXUKXFu2dAmat9kUJy3dKS2CCNXXg+/y5Xb90L8/2FExqeW5p6wMPKdPy6syLUQgEOnJOEaCKELo82cIPnuWHtNStJKrQcf2Z5qgrMuWMR3OWa3yCo7FzHvpEvhu3vyuMVKT8bAjaDfqaAEfsy5fHtnVZFX85UtwY99XSxddjId0c804+4pfviiKEgOqKToQ6AiWpTO2KAdOXHzPnhr3YQw5Y0FBp6FDnz6117zWVhaiqfRn2U3jKGlbv147YJp37aid7Tt3gnnu3M5B09BPExOa7+pVsC5dKpeX9++De9cuCPv97X+7dQvcmBIy9VZYCMbRozUANhrBvn17ZATkOLCtWwe2DRvYIqTUyZqaoBnnZYIOvnqVoMGNI0eC2NjI5mEWAeXl0HL4MBsk4s00Y4YGwNgyRGwTsWbGN3Zi/yWhT1IwFWgXQvO9eilKVvO8eRDC4tS0cSO0HDoExnHjwDBiROLajxrV6dvvUkdvOXqU7Uzs7kTnV2tREfjv3oXAw4esorJqKoptYySdblA40k/F3hoMgu/GDeBxIAl9+MDylfXh58/BNHmyfLcUFuzHA2MlNY4dqxiO0fw2z5rFPHpcE/b5gDOb249wKJ9rayGM0aKkqmjRKJxbjxwB89SpIAwfDkaFHQ7HnZx0C7BtzRowz5/fieNKQTG/SR+LqMyUdrj1+HEGLOKA4rt+HQCLVg62rfg8VmpzPxzYNHNmQlWlfLOgQurU3IoLQVBMNXGcbIedmLdkjbi4NFaaJk5ULieYNt1etOKnGcotz9mz0LR2LXgvXmRHNSkv3pQpLN9V2x9Fh5oYwVTxXrvW/dKS8pfaEO9wMJXk3rtXBklVmq5hlRVbFw0QbdMO7mYIw5AO32mWDmB0AOa2s7RUsbpTsRLovFpI/HwcLbLn5EnttDSFb6w4SHotFSwMW7XdpyMg+rdLqilBUUH6O1r9tTnxSBGWXYtVOlmoU6S4SHZKCizZe9I42VVYzaal1O+GY3lN/ZbSgcvNZTWDWhgVKB/mrPjtm77nYb0ZhbQvi3j9BPw1i4DrCbgqi4ArCfhmFgGXEzB9XF7MAlj21QACfod+JguAifFdVHj8DpFvh2SquSXGNqVF3/5YQ2ImA2HDEtv7eGlZBpEvRIgZBvubxKaopWkQXYzuypAw/hUiX/JIOjz8C5HPUJel6W6L0r3TCd9fSlo62ZNjv4pHL0AfpjLqDJAOxuokAVUuVeM3ahf/J8AAB3J+NFoSVzAAAAAASUVORK5CYII=',
            hotImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAYCAYAAAD6S912AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NUZBQjQ4RjE1RDg5MTFFOEIzNzQ4ODg4MUU3NDYzMTEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NUZBQjQ4RjA1RDg5MTFFOEIzNzQ4ODg4MUU3NDYzMTEiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSIxODhFMDA3MDg0OTQ2QkZDN0U4QTE0NUJFRkU0OEVGQSIgc3RSZWY6ZG9jdW1lbnRJRD0iMTg4RTAwNzA4NDk0NkJGQzdFOEExNDVCRUZFNDhFRkEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5NSvRjAAAEkklEQVR42pxVW2xUVRRd9/2eOw/6cDpQUsDUBoGkYqN8YERTQ9EEYjTBB0k/TCTRmBj94EOiRv8k8UcTEklKQGCU+giEVEEtoXwoID4ofU1aaCdQaDszncd9zJ1zPMW0QmwLupOTnHvv3mvvte86+4BSioXWEPHF2b33e5954/Dx7Yv5zyzczeEWWPLQK+fad+R/2r6V+n2/1i3mK2IRC0Yvh2/uP3KgePrHtrhAUJTyCLLDGySsSy4Uwy8GeCX5RYfb9X1bgyqgmvNhZCdRGk61LhazIOBk5+c7/bPnnombBni/AoETUWPH4Zwf/B+AvX/UZ78783ZdhYeiySCCAmJUMeca4PJk3dV9+3b9J8Dspb6Nanq8PhRwoLwMP2QhQxQEpRBiJIbcmXOvZY5++fJ8sfP+FDo2vdZCDL5RxqReQBkBrGwetLoGvuxBdm7W0kNHO6AIKrZs3TtvhTcuXKyd3Wem8isljwPxAY6TILC8EcWEIwOeGiBhqIhOFzFwpPPj1NcHd/6rwtyJno3Xenreqjb0F6fLHopixZVFDkIxQJQncMoBSx1CzifgOdZXl/XV1GBSWb127OwuV070q5s3npqrMP9Dz7tKb6rNm8pliEgyLld6joQU9lWEVBZhiRZQFiCtWA7OrmJVhFEkQK1goGHCrct9deKzOcqlY6dbubHrLRFWa7G/H7oooEwDFGSO/QwFjq4hsG04qoaa9etfUNaseXPEqwBGGFzWRQQSpPGJ+sHDB16/BeheGGrXfaKGQwZKl/oQjGeQqErAqVTg2jpKtomcIKAoSJgifCHWvmOPu64pmVW06xzhGL0CLEkEvdC77W/KYxMtGnPmAwpl0gH321VEihwk9sxpBsqKDI9JR5Ys0Iy3fCao8Z03nncioZQHNgwYA17joaWnmjPd3c285NN6hQgQHIpIgfG+OAL9Sg5RhCCXKFSXh001GIEI2xNis70KrVu9v6ApAV0SRpYVavmyaY/7zTzvEnCsuTxnQKQqSjkXnkdQIYxGicAKFKgOD6fgwTP19Cxg7Utb9wbVS/pnqoeoQykr4PNBHS8xnVHWC6pb8MJMGhETrDkgNqMoyhAJC3AIAlVGblXN8Ts0V7+su5hnoqcmeMYCmpHjZcKyU4IJnUchYnZXtT32FGlatmekUrhOZYkpmUBwfdhL7+sfa6oShwB1DjAeOy+xZLLPtMkYIWSPiaMNZlc0zbfK0xTphJFqerqly0JL18CnnbmBwdHNvBaooYdWdMc3PfJ+i1U7cccxU6IFrmKCZkWk43o6sWVtUnSbG/aV0pdawq4clqLR1Kzz/a9uey/be/PDSqUsxh6Mu/OdeadElqhaCJVAAGlc+s2tJKuefSIp8lzAOy5qy2xO3WbhpqpgIbAZs0XF9U0BjsUVEmsf6Jw7evrDqz+5IZThTmQb0ZcJ4x7NzebqR50caPPKvfyTjafmhoOy8/HdFa9ojqQGW830+AqtMXL+XgCHrwxvsBqXfRtqe3T3P7Pvthtr8ODJ9l8+6Ojwfr5ad7ebcOhYz6Y/9yQ/ogPT6u3v/xJgAFzybVMiIHX3AAAAAElFTkSuQmCC',
            historyImg2: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAcCAYAAACQ0cTtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NjA3QkM4RDc1RDhEMTFFOEEyRDRBNzgwMzdCQTk3NzAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NjA3QkM4RDY1RDhEMTFFOEEyRDRBNzgwMzdCQTk3NzAiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSIxODhFMDA3MDg0OTQ2QkZDN0U4QTE0NUJFRkU0OEVGQSIgc3RSZWY6ZG9jdW1lbnRJRD0iMTg4RTAwNzA4NDk0NkJGQzdFOEExNDVCRUZFNDhFRkEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6uTrq+AAADOUlEQVR42rxWTUtbQRR9SZ6GJC4kNSBZGFAXWpAK6tK46KLZFYRCf0K7a7vuXyjtf2jXXUVtF4IhK4UgIZBAQo26kWDS+JUXTdT0nMeMPCfzkijUgeG9uXPnfp1774yn0+kYjx17e3sBnLc8Hk9ocnLS6sfvcVOWSqW+XFxcLPp8vjqE9ZLxGvtp/tze3gYTicTig5Tlcrn40dHRe9M0W1DWZTGEN29ubp7xH/s1GgNFgUajsRKNRj/Pz8//1CkztRZA2OXl5dvR0dGv8Xj80yAh3d7e/liv19/BiDE3Hq/bht/vN+BZbVD8wF/iGRjqip1ZKpWip6encYaBhEAg8AfrZSgyLMt6sbu7+4aeyn0OhD6A/b/X19dh7nF9dnb2cmhoyEAol7PZrHV1dTXh9Xot4giZhbm5ud+edDrdOTk5oUUGQmAMDw8bYLK/rVaLgm26JtQ2nbzAjZjZZ8jPc1wLLA0YkWHimOPj45FQKJRwJgr+g2A4AGOIGNBCVRn5yQdFVaE4yDXDSG/guU2H4gny9Ez9/zG8xhOOLmUbGxv5zc3NX07azs7Oh2QyuZ/P5xckDYkVW1tb21d5t7a2vq+vr+e537POisViDBkWYbzVZGg2m9y7qyHighlD1t3DE+tZQDPL/Z6eCYDHVBy5ZqY52xaFiaw9vGe9MLSvMmYda0XXogS9quFtOPmwPqYRg3hmuDXddrttOFuRLAekdkyNzkAJQmuoULVK1BPpIScvi1rFl5jTYGcUtMrIJKalhlfQGwq9i8azpA9cZ2oopHInlsRGGqjrLn2vGArlYZUZeMVkn5QDIYyQBi8ijypqN3CByzHxcXpBzzALOi/cksxUhQL4jMq8tLT0LRwO/5iamroDfXp6uor5XBXIUlAN03pGYUwGnYdORb0Gau9Q3Aj9E8SN8SED6a+FxKsJg4Xr/aBcLgcfowjJNAEoqrrnQdeDh5cgsuwVlBmVSiVD5TxIS6XH8pKUySD6JF4R1gKLmsbOzMwU+irDrZ05Pz9nR1+t1WqrVMAeyOvdGWbRJeyJl5j9QBI9MTMyMpIe+N3Ie4sPFwm4rqk6vZP/8png9m580mfBPwEGAIn3C01dl6zcAAAAAElFTkSuQmCC',
            historyImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAIAAAD9b0jDAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkY4NDZFMUI1RDhEMTFFODlCOEFGNDY4NkZGOTlDMTAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkY4NDZFMUE1RDhEMTFFODlCOEFGNDY4NkZGOTlDMTAiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSIxODhFMDA3MDg0OTQ2QkZDN0U4QTE0NUJFRkU0OEVGQSIgc3RSZWY6ZG9jdW1lbnRJRD0iMTg4RTAwNzA4NDk0NkJGQzdFOEExNDVCRUZFNDhFRkEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4ONt4hAAAD2UlEQVR42nyWh04qURCGXVgUOwpqsKJGY9T3fw6NscQQkBK7goiC0u6359/M3QD3TuJyysw/fY7eYDDo9Xqe53U6nZijfr/v+75OPj8/W60WDN1ul6upqamZmZn5+XnWE4644huPxyci5APKDxAA/f7+8k0kEh8fH/f3941G4/v7m0MpYwFEMpkENJ1OLzvinEPs8BwJ1JOZEBcoaLfb1Wq1UqkAwTUyk5OTMhxjYYAN2znH5JWVlYODA8zvO4JtGJQNdp2dnb29vc3OznK+uLi4tLSUSqUQU5TAJSDPz898WXOytbW1t7e3sLBgHgffgSM27+/vl5eXOI5pMB0eHgJnsYsSTry+vj48PNRqNexA9+npKQExBk8auDs/PweXgG5vb+dyOWInjq+vL1CwHWVyS5oIVKFQIFA4jkMYQYhZB0ETaD6fB5Ekrq+vcw2iwvfz83N7e3txccGtsmy2w3N8fLyxscFhvV4HQR4jFePn6ekJd1hks1kcoZLMIjzgquFoKAiyhkSRLhbg3t3dKT0x4l0ul/ER7wCVANqAlmYsIlEqhiipHrja398naNj7+PgYWkNmSDfVsLm5ubq6Cp84pLPrCB1D5S1LRWSVGmg5wq2gpAFFBjNBlH60qdq1BU7tNFoGaj8WpB4EUkc9BOfNZhOUubk5+iSaCoVMMqNmWkkSJb6IU3/gEEYEg9bkh8DhtbgNQs2n7rZuiZI4bSyA0HHka46MynAoW2QyutWm2to5XmsBouqJKvSNaSwBJDH6R6VqpNBT1EoGPDDLDh/HBT82ZHiAK9ziHfFibaOo5whE6hREUq+CwWQfQ+DgCAH2qk3JoCyTyRwdHanZdGig8h1EThBHMbcYEUy16elpBKgGyoAMRtOtQGcd/Ss+4kdW3aH5HWPGSJgxik6NuOgc+w/JLb4vLy+YlXIUJJl+YIUeLtRaY6tnLCEl31XzjG3CFYASAhqUaGIgo0xFN7YkNLeM4EQ9E4cpTECRWltbI27BlOKPcYcGZFB4dXVlL+DYLgrnkCttFjc3N7iICHEHJyw1MVFuDFoU8N4xGdFvAVWUo0+bNej19TX1y5b88KjIjqBr7DlhYqJW0WHO7+7uUoPWu0MdwTNVLBY1ZEE8OTnBJnSrbMLnRLiUN3MedyQMtx4+PNVbyxcnYABOMwGvd3Z25KU9qOGgs17GKWZ2qVRiET44sZhGlw1QJZNW5C3Ba5uNloa/T7TGFRxoo0p40XghsIu10DVbCQjBITLkmhqyqlCgwyksL9ioLBBTt2nkNB2pzbkFEQMpbdjs/yXY1N+QQP8IMABW7WNtat3eewAAAABJRU5ErkJggg==',
            del: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAUCAIAAADgN5EjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDQzOEQyREI1RDhGMTFFODhGMkRFMzg2QkI4ODM3QTgiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDQzOEQyREE1RDhGMTFFODhGMkRFMzg2QkI4ODM3QTgiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSIxODhFMDA3MDg0OTQ2QkZDN0U4QTE0NUJFRkU0OEVGQSIgc3RSZWY6ZG9jdW1lbnRJRD0iMTg4RTAwNzA4NDk0NkJGQzdFOEExNDVCRUZFNDhFRkEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6Bi5lBAAACHElEQVR42pSTV4/CQAyEsyH0XsQhioD//6cA0UQXnQsk9yUTwt3j+WG1xWOPx17jed53aJlMJplMWpblhZZIJIwx1l/j/nK5pFKpdDptzufzfD7n/BWabdty8n1fyNfrxUogYIvFYrVaAev3+3jaJHw+n+v1ervdxrFBPh4PYArBCgYfLnk1ujoej5PJxHVd9t1ut9lsypsbVsdxOMJrs9kQiISdTqdSqRgF4BYmbmgwabVa4glJNrvdbjqd3m63bDY7HA6LxSI0DdmpmGeowud6vRJImVUtlwS93++lUomguVxOEhoJgAEQmNiITFrAFLZcLgmXz+cHg0GhUAAjOkGdmDijFsRwJQNguIGBGHl6vR4kI22MCdgCEyvCIAb7/X6vzOqEJAEmH2Dcs7djqpiYUA/eGgaO7IHJR92W8p+cQRjbBkC1ECY8R/qMfo1GgyHRYKi0IJCQMvzgSYfoDXlQhdmi24Db7TY9VDmRtjGSg2B4g0GScrnMbI5Go0APYwDX63UNRtQVkRQMJ96QBCdFPBwOTBhqk5k+geeSKQhGDNfZbEZ5nGkGfYOY/zaynU4nZohXcuhjRHrSBk0JYjJckKRg/Y/4o0EHUhSC1KStVqsOvR6Px8CojeECpgYAVlcgxQZ5SaiK1GSYOrVajWDUoGwwxBXaUp89YIrEDTCvzFbws6WtdFJ740nUxEi/eFo+//53P/9lPwIMAN47/XXap/yzAAAAAElFTkSuQmCC'
        };
    },

    methods: {
        // 调到用印审批页面
        // jumpToSeal () {
        //     var urlDetail = weex.config.bundleUrl.split('/').slice(0,-1).join('/') + '/approving.js'
        //     navigator.push({
        //     url: urlDetail,
        //     animated: "true"
        //     }, event => {
        //     // modal.toast({ message: '跳转'})
        //     })
        // },
        // 跳转页面
        jump: function jump(index) {
            var urlDetail = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/' + index + '.js';
            navigator.push({
                url: urlDetail,
                animated: "true"
            }, function (event) {
                // modal.toast({ message: '跳转'})
            });
        },
        clickPopUpSearch: function clickPopUpSearch() {
            var _this = this;

            // this.isColor = true
            this.isColor = this.isColor === true ? false : true;
            setTimeout(function () {
                _this.isShow = _this.isShow === true ? false : true;
            }, 500);
        }
        // jumpToCeShi () {
        //     var urlDetail = 'LocalAssets_approving.js'
        //     navigator.push({
        //     url: urlDetail,
        //     animated: "true"
        //     }, event => {
        //     // modal.toast({ message: '跳转'})
        //     })
        // }

    }
};

/***/ }),

/***/ 270:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroller', {
    staticClass: ["approval"],
    attrs: {
      "showScrollbar": "false"
    }
  }, [_c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env.deviceModel === 'iPhone10,3' ? '116px' : _vm.Env.platform === 'iOS' ? '68px' : '28px'
    }
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v("工作圈")])]), _c('div', {
    staticClass: ["hzTitle"]
  }, [_c('text', {
    staticClass: ["textLeft"]
  }, [_vm._v("杭银知乎")]), _c('div', {
    staticClass: ["right"],
    on: {
      "click": function($event) {
        _vm.jump('workspace/commando/commandoIndex')
      }
    }
  }, [_c('text', {
    staticClass: ["textRight"]
  }, [_vm._v("更多")]), _c('image', {
    staticClass: ["icon"],
    attrs: {
      "src": _vm.icon
    }
  })])]), _c('div', {
    staticClass: ["search"]
  }, [_c('text', {
    staticClass: ["inputSearch"],
    style: {
      'color': _vm.isColor === true ? 'rgb(208, 208, 208)' : 'rgb(153, 153, 153)'
    },
    on: {
      "click": _vm.clickPopUpSearch
    }
  }, [_vm._v("你感兴趣的内容...")]), _c('image', {
    staticClass: ["searchIcon"],
    attrs: {
      "src": _vm.search
    }
  }), _c('text', {
    staticClass: ["question"]
  }, [_vm._v("提问")]), _c('text', {
    staticClass: ["message"]
  }, [_vm._v("消息")])]), _c('div', {
    staticClass: ["tabbar"]
  }, [_vm._l((_vm.tabs), function(tab, i) {
    return _c('div', {
      key: i,
      staticClass: ["tab"],
      on: {
        "click": function($event) {
          _vm.activeTab = i
        }
      }
    }, [_c('text', {
      staticClass: ["barTitle"],
      class: [_vm.activeTab === i ? 'active' : '']
    }, [_vm._v(_vm._s(tab))])])
  }), _c('div', {
    staticClass: ["movelightBar"],
    style: {
      'left': _vm.activeTab >= 1 ? _vm.activeTab * 166 + 237 : (_vm.activeTab + 1) * 237 + 'px'
    }
  }, [_c('div', {
    staticClass: ["lightBar"],
    style: {
      'backgroundColor': '#00a4ea'
    }
  })])], 2), _c('div', {
    staticClass: ["line"]
  }), _c('div', {
    staticClass: ["answer"]
  }, [_c('div', {
    staticClass: ["list"]
  }, [_c('image', {
    staticClass: ["mark"],
    attrs: {
      "src": _vm.mark
    }
  }), _c('text', {
    staticClass: ["answerText"]
  }, [_vm._v("请教各位大神，手机下载杭银联的二维码可以在哪里下载？")])]), _c('div', {
    staticClass: ["lineLine"]
  }), _c('div', {
    staticClass: ["list"]
  }, [_c('image', {
    staticClass: ["bulb"],
    attrs: {
      "src": _vm.bulb
    }
  }), _c('text', {
    staticClass: ["answerText"]
  }, [_vm._v("OA主页右下角，it栏目内")])])]), _vm._m(0), _c('div', {
    staticClass: ["applyModal"]
  }, [_c('div', {
    staticClass: ["imgModal", "a"]
  }, [_c('image', {
    staticClass: ["docRout"],
    attrs: {
      "src": _vm.docRout
    }
  }), _vm._m(1)]), _c('div', {
    staticClass: ["imgModal", "b"],
    on: {
      "click": function($event) {
        _vm.jump('workspace/approving')
      }
    }
  }, [_c('image', {
    staticClass: ["seal"],
    attrs: {
      "src": _vm.seal
    }
  }), _vm._m(2)])]), _c('div', {
    staticClass: ["lineLine"]
  }), _c('div', {
    staticClass: ["applyModal"]
  }, [_c('div', {
    staticClass: ["imgModal", "a"]
  }, [_c('image', {
    staticClass: ["docRout"],
    attrs: {
      "src": _vm.busApproval
    }
  }), _vm._m(3)]), _c('div', {
    staticClass: ["imgModal", "b"]
  }, [_c('image', {
    staticClass: ["seal"],
    attrs: {
      "src": _vm.crm
    }
  }), _vm._m(4)])]), _c('div', {
    staticClass: ["lineLine"]
  }), _c('div', {
    staticClass: ["applyModal"]
  }, [_c('div', {
    staticClass: ["imgModal", "a"]
  }, [_c('image', {
    staticClass: ["docRout"],
    attrs: {
      "src": _vm.code
    }
  }), _vm._m(5)]), _c('div', {
    staticClass: ["imgModal", "b"]
  }, [_c('image', {
    staticClass: ["seal"],
    attrs: {
      "src": _vm.finance
    }
  }), _vm._m(6)])]), _c('div', {
    staticClass: ["lineLine"]
  }), _c('div', {
    staticClass: ["applyModal"]
  }, [_c('div', {
    staticClass: ["imgModal", "a"]
  }, [_c('image', {
    staticClass: ["docRout"],
    attrs: {
      "src": _vm.accountManage
    }
  }), _vm._m(7)]), _c('div', {
    staticClass: ["imgModal", "b"]
  }, [_c('image', {
    staticClass: ["seal"],
    attrs: {
      "src": _vm.purchase
    }
  }), _vm._m(8)])]), _c('div', {
    staticClass: ["lineLine"]
  }), _c('div', {
    staticClass: ["applyModal"]
  }, [_c('div', {
    staticClass: ["imgModal", "a"]
  }, [_c('image', {
    staticClass: ["docRout"],
    attrs: {
      "src": _vm.hrApproval
    }
  }), _vm._m(9)]), _c('div', {
    staticClass: ["imgModal", "b"]
  }, [_c('image', {
    staticClass: ["seal"],
    attrs: {
      "src": _vm.party
    }
  }), _vm._m(10)])]), _c('div', {
    staticStyle: {
      height: "30px"
    }
  }), (_vm.isShow) ? _c('div', {
    staticClass: ["popUpSearchPage"]
  }, [_c('div', {
    staticClass: ["popUpSearchPageHead"],
    style: {
      'paddingTop': _vm.Env.deviceModel === 'iPhone10,3' ? '116px' : _vm.Env.platform === 'iOS' ? '68px' : '28px'
    }
  }, [_c('input', {
    staticClass: ["searchBar"],
    attrs: {
      "type": "text",
      "placeholder": "搜索知乎内容",
      "autofocus": "true"
    }
  }), _c('text', {
    staticClass: ["searchText"],
    on: {
      "click": _vm.clickPopUpSearch
    }
  }, [_vm._v("取消")]), _c('image', {
    staticClass: ["searchIcon2"],
    attrs: {
      "src": _vm.search
    }
  })]), _c('div', {
    staticClass: ["commandoHotSearch"]
  }, [_c('text', {
    staticClass: ["hotSearchText"]
  }, [_vm._v("知乎热搜")]), _c('div', {
    staticClass: ["hotSearchContent"]
  }, [_c('div', {
    staticClass: ["hotSearchContentText"]
  }, [_c('image', {
    staticClass: ["hotImg"],
    attrs: {
      "src": _vm.hotImg
    }
  }), _c('text', {
    staticClass: ["hotText"]
  }, [_vm._v("杠精VS杠精")])]), _c('div', {
    staticClass: ["hotSearchContentText"]
  }, [_c('image', {
    staticClass: ["hotImg"],
    attrs: {
      "src": _vm.hotImg
    }
  }), _c('text', {
    staticClass: ["hotText"]
  }, [_vm._v("杠精VS杠精")])]), _c('div', {
    staticClass: ["hotSearchContentText"]
  }, [_c('image', {
    staticClass: ["hotImg"],
    attrs: {
      "src": _vm.hotImg
    }
  }), _c('text', {
    staticClass: ["hotText"]
  }, [_vm._v("杠精VS杠精")])])])]), _c('div', {
    staticClass: ["searchHistory"]
  }, [_c('text', {
    staticClass: ["searchHistoryText"]
  }, [_vm._v("搜索历史")]), _c('div', {
    staticClass: ["historyList"]
  }, [_c('div', {
    staticClass: ["historyListContent"]
  }, [_c('image', {
    staticClass: ["historyImg"],
    attrs: {
      "src": _vm.historyImg
    }
  }), _c('text', {
    staticClass: ["historyText"]
  }, [_vm._v("降级化妆品")])]), _c('image', {
    staticClass: ["del"],
    attrs: {
      "src": _vm.del
    }
  })]), _c('div', {
    staticClass: ["historyListContentLast"]
  }, [_c('image', {
    staticClass: ["historyImg"],
    attrs: {
      "src": _vm.historyImg2
    }
  }), _c('text', {
    staticClass: ["historyText"]
  }, [_vm._v("清空搜索历史")])])])]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["hzTitle"]
  }, [_c('text', {
    staticClass: ["textLeft"]
  }, [_vm._v("行内应用")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("公文流传")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("用印审批")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("业务审批")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("CRM")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("法规会签")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("财务共享")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("管理会计")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("采购系统")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("HR审批")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["imgText"]
  }, [_c('text', {
    staticClass: ["headText"]
  }, [_vm._v("易党费")]), _c('text', {
    staticClass: ["modelText"]
  }, [_vm._v("合同审核不可大意")])])
}]}
module.exports.render._withStripped = true

/***/ })

/******/ });