// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 319);
/******/ })
/************************************************************************/
/******/ ({

/***/ 319:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(320)
)

/* script */
__vue_exports__ = __webpack_require__(321)

/* template */
var __vue_template__ = __webpack_require__(322)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\WorkProject\\Weex\\hzBankOA-weex\\src\\views\\personal\\signIn\\attendanceRecord.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-ebd2514c"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 320:
/***/ (function(module, exports) {

module.exports = {
  "attendanceRecord": {
    "backgroundColor": "rgb(245,245,249)"
  },
  "header": {
    "paddingTop": "40",
    "paddingRight": "32",
    "paddingBottom": "20",
    "paddingLeft": "32",
    "backgroundColor": "#ffffff"
  },
  "title": {
    "fontSize": "34",
    "fontFamily": "PingFangSC-Medium",
    "textAlign": "center"
  },
  "more": {
    "width": "40",
    "height": "44",
    "position": "absolute",
    "left": "32",
    "bottom": "20"
  },
  "userMessage": {
    "paddingTop": "24",
    "paddingRight": "32",
    "paddingBottom": "24",
    "paddingLeft": "32",
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "marginTop": "8",
    "marginBottom": "1"
  },
  "userImg": {
    "width": "120",
    "height": "120",
    "marginRight": "24"
  },
  "userName": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgb(51,51,51)"
  },
  "userFrom": {
    "fontSize": "30",
    "color": "rgb(153,153,153)"
  },
  "attendanceRegion": {
    "paddingTop": "30",
    "paddingRight": "32",
    "paddingBottom": "30",
    "paddingLeft": "32",
    "flexDirection": "row",
    "alignItems": "center",
    "backgroundColor": "#ffffff",
    "marginBottom": "16"
  },
  "timeImg": {
    "width": "40",
    "height": "40",
    "marginRight": "24"
  },
  "countTimeTxt": {
    "fontSize": "30",
    "color": "rgb(0,164,234)"
  },
  "attendanceRecordListItem": {
    "backgroundColor": "#ffffff",
    "marginBottom": "4",
    "paddingTop": 0,
    "paddingRight": "14",
    "paddingBottom": 0,
    "paddingLeft": "14"
  },
  "timeHead": {
    "flexDirection": "row",
    "paddingTop": "32",
    "paddingRight": 0,
    "paddingBottom": "16",
    "paddingLeft": "16",
    "alignItems": "center",
    "borderBottomColor": "rgb(245,245,249)",
    "borderBottomWidth": "1"
  },
  "goWorkSignIn": {
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "space-between",
    "paddingTop": "30",
    "paddingRight": 0,
    "paddingBottom": "32",
    "paddingLeft": "16"
  },
  "bottomSign": {
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "space-between",
    "paddingBottom": "38",
    "paddingLeft": "16"
  },
  "greenDot": {
    "width": "10",
    "height": "10",
    "borderRadius": "5",
    "backgroundColor": "#2cbc90",
    "marginRight": "12"
  },
  "styleRight": {
    "marginRight": "18"
  },
  "signInTex": {
    "color": "rgb(102,102,102)",
    "fontSize": "24"
  },
  "goWorkSignInLeft": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "bottomSignLeft": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "notSignInTxt": {
    "color": "rgb(237,78,78)",
    "fontSize": "24"
  }
}

/***/ }),

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
    data: function data() {
        return {
            Env: WXEnvironment, // 获取设备环境变量
            more: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAsCAYAAAAXb/p7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpBNDhCNUY4MDU3NTYxMUU4QjQ2NUM3RDZFRUQxQkE0RCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpBNDhCNUY4MTU3NTYxMUU4QjQ2NUM3RDZFRUQxQkE0RCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkE0OEI1RjdFNTc1NjExRThCNDY1QzdENkVFRDFCQTREIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkE0OEI1RjdGNTc1NjExRThCNDY1QzdENkVFRDFCQTREIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+lyjhmQAAAxJJREFUeNrs2FFIWlEYB/CcS8VkSa6wHhYhOUiEEfYg1EMPg2AwFgZJsrmFbTkdwSTaYA9t7GE+iAtkMmjMokG7m4R5F6MRDYSEGWL4kpXIJNhgMBy7TJGsnU86ID6turrvoe/p+CD84N5z7v9/BAcHBzWY50wN8jkFls7MzMyQzWZ7FwqFLqID+ny+obW1tcG9vT3R5uZmNyrg3NzcQDgcHoS1TCb7OTIy8goNEHDkkV6nOJfLdQvNO8gwzBWKk0qlv46KqyjQ7/f3rays3Ia1RCLh3G73DTS7GHDLy8tWipuamjKhOWZYlu0+xF0ViUTZk+B4By4tLemDweA44Gpra9+Ojo5a0BzUgAsEAg8ozmq1WjQaDYcCSDbDJYoTCoXv+cLxAlxdXdWS4+QxxVkslrt84U4MBNz8/PzTUlxnZ+cPFGGBfLraKE4gECwMDw/f4xt3bCDgSDJxUZzZbHbodLrvKOIWxZEk3k9xer0+hSIPRqPRxtnZ2SKO/Fw0mUwPK4k7EnBjY6Nhenr6xf7+fhFnNBof9fT0JNAkahI4nxcKhQFYGwyGJ729vXFUkb++vh42wSKs4/H45WolccFRaufExMTLTCajhLVarQ47HI5nqEqT0+m8I5fLi8fJ1taWnmS8cXStrhQJ5cfj8djRPOLSIY/3NcdxDbDWarWf7Ha7B1Uvhn4BJYhuGq/Xa0VX3AFZV1dXRMZisT5yTt5E84hLZ2xs7E0ul5PBuqura4GkGh+qqw/oHVCOYB2JRPrhFgHd3QwgoSRBwoFbBCjs6C6PoCRBHwEkFHY+kMLJyUnegE1NTfnW1taPJPWoSKgwpNNpSTab/U0qwDYKYClyfX1dDbEslUrJ8vl8pqOjYwcFkCKbm5s/k6OnDZDJZPLccZEVAcIQ4J9yJEnguyRkpFEAKVKpVIbgnSQ/r5GAcZ60v6/t7e27KIAwLS0tnEKh+EIS+QVAJhKJRrFYvK1Sqb79t9ut8oHeAuUKShYcQSzL3v/X/56tVjI+LFcOhmE40p8/VPVbjCqwngLL5q8AAwBoMnaljJ91xgAAAABJRU5ErkJggg==',
            userImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB3CAYAAADIMoQHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA39pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDozZWYwODg4MS1mYjc3LTRhY2EtOTgzYi1kYTlmZTc3MjRlYzAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NjJEN0ExQ0U2MEIxMTFFOEFEQzc5QkM2RkM4QTBEMzAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NjJEN0ExQ0Q2MEIxMTFFOEFEQzc5QkM2RkM4QTBEMzAiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjNiNGQ1YWRlLWZiNzctMmQ0Ni1iNDc0LTkwZjZlZDkyNTIxMSIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmQwN2M1MzFjLTVhODktMTFlOC1hOGRjLWRjMjEwNzU0ZWQ1YyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PolZNtkAAIZ2SURBVHjalL0HkCTpdSb2ZWZ5313t3XT3TPf0eL876x0WC+9BEiQA3okQLxQUj6ROJxMhKeIUUsTFHSUxdFJI4lGkJBwtcCThCGAJEiABrJk143ZMz/RMe99dXd5npr73MqtnhgTE0GzUVnd1VVbm/9z3vff+l8bvfv5XEbBMBEMBRMJBwHDQ+Re0gv5PJgwXsPgwXJO/G3BdF65jwLH57Br6qNSqsG0HtuugbrfQsttow4UjDwN83dW/ua4N+a/NgzqBJqxUE5Gsi3a0ASvSQiBkw7Rsfk0bjv/9+h0I8DuBerOORqPhnWMoBNMwMnzPyUy671Cw3TWZu2MfuP/O6lBvNJu1EMpaoVDcMNxAEEYyGLIQDIZLhmG0HcepBELB3UAgsJuIp9YMw1rkIe+bLuZMx7hWLpbyTpvnwGsy5fp53YbhrYhcT5N/c/W6HF6LqdcJWZ82H3xjrV1G2+J6RG2U3Dw2yqt8XxsBM4okIphIDMFsmGjzUmQdi4U8zhw/gS9+5vNo8Nhr+QKsaJzfym9utWBVioiZDhLJGGr1Kr+rjQbXuVopodiow01EgFAYoUAIIcouEYsh4PBMbS606TjgBfPU3X0BO4atz4ZcFV+2vaXW3ykrvUjX+5P+/vA/0zR5TFkSG7ImJoVq+cJuGJanNPzZbPOz+QBabYq824FhchEDFLBZ4xfLN7b1veBnGnyPg0CQ77nAF58IBAPP8FTOUsVGTb61tVNGoB5CvzuM4ekDODQ8hmalhkKljGqjiqbbwubuFirVWrK7JyvH7eJVj9itBqrlMs8nqAps6sXLmpjLpmu9S8H/kKf1ut1svcW/tER+rq94jig6z80VC+AVyoIYJq/bEXWkQooQqlVEE2FEQxGU2xUaDpXMCOg6yprbbSpIvY3BZBZPn3sSGxS0zfXLlyqIcg2DFJjh2AhFePxoBHmnhTpayGa7qNxcESeJcKtJo3LQ5KPdbNPYalhdXUXAbasIRQ35ZY9KSazGe/aEFDSDosOPKIGatv7qveZ0Lt1Vsei79dpd72daG5fBVGUR4cPlMdsG2uUGWkaQguWF88NUcn6kzd/1BPv4/4+1HfcDXM738/ck15CWAD0WalGYtSAOjk7jYPYYssZBmJUwWuUSAj0Gj8EjcSGKdg3b+V3cu3cPaxvrSHV16UEiVoiewVWF0xN1vXMOBq1Rmvloq9X8uN1qwzLMMtfke3x8i8r9dX5iixek3syA58Vc1XqepRxEBC1X0XAQSUUQCYQpgDosfmfQv06aLpyWDZcC/uCHPoChvhEU3CZKlQqS6RSSyQyC4ShcesO9rUUkIjG0+P5QNIkqhVym8jo8h3gyjkwwrOsnDzGhZn0E1qeOP0kXzYW1AnqinvWpM+K189lwYXRcNgXs+kJ01HTF3VJjeJJtupR6s+m7MEMXSFxyiK4rYHoX5MC/cAN6XEuEzZPjD7xYXnAriHZFNNJEKpZMBsPGF223/a9qTeffVBv2x2st4ygsKxzQkEGXZTVgVi3024fx3JGPYrL3JFCNYm+9CIda3K43UaGbq5YraqGVeo2v23jyzGNoVxtY3dxQa3Op9W6b1yQeQjyJPrfR5PXItekl8TzbrhFyDHOGK/IxXsF/QvV8zrXdMH++y2VoyloYcpWOhCTXC0/UvwAFxBVCKh3iutV5TIYhXm+CHw00edxiCx988UM4NDVDl+vo2kroidI9i3dtUriu3UQ0QPfREttlyAiaaNSoLEHKLhKl1wvxNEw10kqljhKt3+Dv1s8/9pIeLMgIZVKzLC62yZgsD8sQQZsqNNMw1f2oiGwugtNWN9bml9vy4ILY6uIN7yFa7Lq+e/ZfNU0vpvJ1PZ7p24wInJoftuJwW8Zj7Zbz37Xa7d8NhkOf5jsnHUdskIoSCjLEWKIPvFIHtb0qus1hfPSJf4RYux8Ohd0otDDYN4BMIoEyL9Lkd3Rl0nR3eWp7he6rgRgX48zpM7h09YrG8hAtK2j67hWu76WMB55Mw5D54DVj30lN8A8fpQz+qeE6U7y2TV71imOIb/Jwh1iXfp4eJEwXW62XuV5VxIMhxJwIartVfPajP4OpyRk1CEfxjKckYqniOdqMvw4Fa3Cdg8EAQsmohqwQjTIUjsCIhmEEQxoWWo2WysiStaaiWp9/4kUEAuLGvBOX2Gmp5XqBxtj3sV6cdfjlVDIK2fUtVx6eFesCGab/Gfgx3V8Rw3uYGol9K5f3mqo48pdP8dJ+hwL5b3nI0826E3KdKGNVlNrOEyboinCBaL5UEl5EPYhodRCffOlLSEdG4DQMEG7RhdGCd3epoBYivPAa42ud5xZPJRmXyojQjRV2c4zFOxiaOEArXqekTFUEveb98/UCjnftHaV9FGd0IhP/EuKinqYr+hJ/fT8PlqeIbqPjs1TAvlZY8koJFq3YrBg4f/wxzEydRKVYZWymkGzX+1pZTlloPgx9tPk9Lf08YQgsgkX5UYxS4aN4QtqceB8xQFNdrQPrF554wQdS8ACOXKTh7itqR8AdrVJQIPFaBC3apRbM33lQT2Ad4RmKqDs/Gz4AEc0z9kGWOulP8fc/NlzjV2vV5mjQiqg3sRmXmzUe0yW6D3mCNQ1bT61F/GUXwvjMC19CX+wgF0cAoqexe7mcLmadlirnODA4gHxhj9gkjHQypeFC4liZKDTR043pIzPY2d5GIZdHmNbwsIm6oqCu97vr4BGrxgO994Uta6JvGeXV/iyX+VN8bZOXfFsvm/9Vq01kaH3hUAuNUgkjmVFcPPU0r0vca0BdrBzE8b1fmOug4U3CnAU1QlfxhKEesEkgZYvVSjigR1WlkHOmTAJc46a48F+4+Ny+gcH0n31VDYQ6uNe7ANcXroAuR6yXrkNPXbwt3YX3OeMhrfdcsVitCDlghjW+iwD5kC/+KgX763z0W2YE0XBSlcDhydIbw27SM9SaaHFhiGgJX+t0pRGUtgy87+IncajvBIqb5BgGqQHjVYNWbsaCwu/UgrfXN7G7uY3yzq6ee1dXmoImYBGvQ4kFSCtM0sPB/kG1liIX3fRdmYQco8OJ4NMFp0MZOpb74D8Hxj6+EElYQaufF/+zdA4foeHM8rVF05awZ2NoIAWb6H4kPYaTR84SUDXJEDxrt4WmCq6hkMrEDY7gGj0s2UeLrj0VhxmmZyItCtPirQCxU8ATuKBxwQ42XXuzXIPF363PX3zRt1TjwUMir+kLygdeYg2iXa6PMkXDHPshFwz37zwbHiL23bKplivwKDDIFfht0pH/gUoxZHbCgWiXY2nsF5csqDFoCggjInSC1MYWYxg1vRXGWPYwLh5/Ho09k3G2SfCSVkGZIX4XY1RAaAhVfnriINKMxalkErntHYbtNvp7+3FvdlZjY2agF3ki7TpByfTUFLa3dpCjew+IyzOMfdD5wD0/asEPv+K6Hh/2XaHnDQkWXNMk2TX/EQUwQzD7eiRolYJWGxGyh2y0B4cPHUGdggjH4kh1d9OQLcUydWKDYqGorCNCzxLWcEOKFQ0hQKoUoaLarZYHei3/FNuuumVx8+LWI6JoX3jqhc45eRb8CLfxRea7DA844acIGH787QAU0+ODGtPFHfNUDes/dl3rz2mx5/b1XxcjoKgaflpEFMlSFG/TPfFnmwjcDtHtGqiVTRwemEE23McLtEj6M9grbBN8UPZEzRKXYrz4ZDzBBSowXIXQM9yPnv4+Cm9HwdTm2hapB4FVMqxG2Z3pQq1a4zFCWCd9EiRqGh235sVPwzV+ooA7V+90aKXpX7flezYBWZYazPFIIPDLdHslu9Z868lzT6CwkUNPdx+iqRRi6SSxQhsOvU+QAkyQIg2ODCMai6qwC8U8mrRgiLWGQl44cBxfwIZ+v8hD4q9ycAo5KM+m5WelDKXpfuzxTtvxBfkg8fF3QIbxaBwyXTwCqswOsIBFQmb+UbNlPPWTFsj0F1GSBRLnbdcDOyG6Y0HoEl8VaJAnNwiUumMD5K5xbBaLiHIVo7EIrdZALJpQSy/x9VydFrC7p4tfm6ugh4t45NAULv3wdXR39Qj9pjtr0W3HUau00eD7h/t7sUELX1hcAQ9KJQj5ovOu3XTxU/9pSsePtaZ6QZ8xmK7ydflHBJ9s11v/pied/rlj0+c+lzDTyw5dbDiVQIEKls72qZIYXHOLH46QvgYZqyKxBIFsHcVKjovi8W1B0LVqleDTQoDr1ZbEn4RPsV56BFO8bYPc/QtPP/8wyNWH41vzvmD9GGy7nntWgCXpRln0fdBpKD4OcCUkwEsCwuSJ8Po+T/f8Lb46ZWsmz3gQ8x96GJqO7Lh/zwPYjqEgS/5q8WIjpDc5utHTp85iaJg6Q02WlJ0geKELFkNAIZ9HnkBrc30DXXR5fX39dMFlLN+9hwzPa29rG5l0FhNTk4zbFpbWlpV7yvdGiGx743H09Yxo+jBHcJagixc7EbdtOnJWpgrw0YflkSsxElqvGQgo1giFg5rUCAnuEE9G99ukl/nY+z44xvX4pUAyvRLOdl9ziOwdM6BapDxcslF8rlMBm5LyZdy2qcBBMgHKjhyXis1rT8ZjVPgaivkitbgJh7GcH+BrXBOGnd5IHIFHsk8/xQX9PWrw0D+noxiu+3ddWIgC+1951C8p+nbpfuUi3J/sDR7YsiYA/QSnpcolAEJ+i3Ih0tGM5lnFjVkEGn20OuG3a6trGOgfQLPZohds49Tp07zwPBeqhYNT0xilZTY3t7Cb28UOKUnf+DCFRkQdItAKx5Elwt6Zu0N020Q8kMH40Ai/voXd4h6sSETpR9AK+CDMv4b9zJ8kXhzJdEnM9aifZXpoRL2Ro0vTbDR5jn30NEm+J5wywuaXGVSeI8D7FRpQ89GlMPTcH17nSqEqh0VPLy291aRytzTEJcNRzQsIlY8TbOZLDtLxNAormxSw4TwkNfchmzXxUxHF333Z7SQDeFKWEqIBvvz1luNc6CAQ1/AtVFy/8ZMFLMcxXf/b3b//vULRArSOZrOtacASUWaKLiyWiKPX6MPy6qombY6cPK5eJEQXW9zLE6WWUa+WMXJoArHlFTgtF29dfocxr4yXXn4J4WACl19/Dct37iCb6kaGaLvWrKE/k6a15JTM2Z3Ej2s+lG9392OV2ynCmF5WzjE9Kul0Qhmf83t7OHXkGMFUBnUKKELQWHPsL9nN5imi/o/xLRsPr4dtO494USkKBYKWXnu5XmM4sxE2DaVSsujkKGQddZiSo6g1cPv6Dc+CDS/xpFz3pwnSeTgO+wISly0X72iex1CIZBnOMYK5V/nykN1B4UZHyK4qgdNJEPzDDuORf3VJNRJEVKsVUqgmknSfxWKB6DqCnr4eWnWMrrWJbSJhARqSyksz9ooDjcejEBPJ9Gexfn8JH3j/K3TPC7CrLbx56a9x79Yt2I0yGvkSBMsIX48kAozdcVQkuAYiStse9nIKcDrJEV9jNeFgmn64k9fs/fdH6OYH+0jJ6KotkxSHoK4m7tR2LpDrvgNJkgA39tfcth+5fuG94WRCQVe6q4vhalMyHZ735LmJh5A8QFc2g13+rZjbgfWLz77YySx6ENqH0Y4v+U6lSOo4QsBtao2UASWWaO6zk+HSKhNeclzje3wp25Yg75oPHcdS8NZJTe5XqfyFEk7dQevuT/EcNl1WibQmHAmjt69XOaBJ+Bzk71K6a/HcqlSCbE8WUcYnWeyG5pPpPkmfMqRTPXTF965fx9T4AaLvMr7/nb8CzQFNArNqpYA63XcmliQSD/L3vB6jTTuwyOEFWVgEekrd5EElkFirr1nez2Jdrk+bo/GITzd5fY2WxuInn3wCIVIig+fd0ljraNyOhiNJ03C/SIFfajUb8/K91VpFr1sqbWEqQyIYoVLH1fLb7SZ2drZQzZfRRXDZJq+WREdU3sdrX19aQpiWZv3j519+AHr2c63GfhrOdT1wJdxMc6W2CFiAQMtzuw+U96MU8tf4e9R2fWSuSNzU/Kooh+Erz37kMr2yo8Qax/GIvuO4vlK5DxIN/pNQBombFVrwJBGxCE6LAVxcOQdJvIs1F0tljWFx8t8IY3WDyrNFHpxj/E0n49haXSTlaMMmnTo0cwwnj85gamIcMbr7Y0dmUC+USLfoAepFVaYmpFBiKph6GFx1mILxEBOA79G8RFHAc9Py4PdJGvX0mTP0pSE0xbu1bS/3bRoK3nhtglt+zrbtawRZs4KgtZTGmBvldclxdmiVO+UivYCJsYkJFfjawhKiBKBRK6yK1G6RVhFvRCSZY9CvG44fcd0HiFlOvO0LVzmvxhNXk+i2H1vMfZLv/Ayf/oDvswzXS2sqZ3A7lMv10Kf1sLPHI8L1ELrxky3YDxJVxlHR3gJRZLlSxPjAlGZ0xGXVyk2E6YZ7e3sUopUo5Dzdt6QnJebFyTPztNJNou5WxEDf9AhiIwdRIj1xxTPUcjh25iS+//VvIUb32RbrsWuISbwkp9qruwgFAvtXrQl9XxEd/7V2s6XXZHoZx30K2aGLMSJ9AWpl1/M0AoyClvdZ8TSlUlWSGqFILPanVO6f5+f/JEMcsLOTQ7HsKW2km9fTk0IwFkKdhhaNdMNaT2BxcQ3j3UOolKic5FjSvOFU6B3+gxdf9mqZmk70rda3mLbtCVSt1tdEcRct6XLwKhPy3o/yZL5C7bXEKhzHz8mKW/erS4aWFaFZGtHyTqOAx3Edjd2iCaZjPIhwxgMXrnhacrPRsNIOsUgp/R05dkwvWnK2wgvDtNb1jU1EKGgBSj3kuy0uXKVcVsAi9d9kMoa1+TuoEEQliJRbvMbN+/fRotW+8YMfYWb8EPoSXbjyztt8bxzJriwtmIvZsglwwvvJHAPGQ8+mV1IXizQ6iSHSiFDAz+FDs04xnt+hw9OM6U0VmNuWnLGpCYpGva4ZOPEYROomBf6p3cLelc2trVnxTKKkvcODjMGkRgSyoiCi2PKYPDBOxY9heWGZAi4iwXWKSOjg8U1BhYbHV/2UpNeq0WEAcrItLQsyiJNjNYWnoQOy8BIfX20pofCM1vYsWmmRRGDTbWkVRApoTVpKla5d+J08qi0p8ns23aL2C0G3bKkcmZpkUGWQ76vwQgoV1IhCq7wAm4R0bXsLuXwOhb0c8msbCPJ9Jj+f7eqmMG1sb++iXqtpgkSSBk7DxtriKhyCquH+fpR4rG5ad2FhBV01nukGLaQRwujYYazcX0WoHYLZJO9e2dO4LLEtQCUSLisPcZ+dhxihqUUeC/ssSnL11QacWhshwR5S+hOaKFkqUpl0LKGWrTVnPgSsCucOUphy/tVmw4qlkl8ZOjj+Uv/kKFIDWSplVT0WNRxBl8ogHSNc+E0Cwyivaej0UeTJtnZ57RaPIXIIyJd0YqahBXiv/0lMTPp9JNZqSdD2BCQgy/VA2WHq6Z/RAkMdRZA8siBlL43m5UVFfJbvFSyCBNew9p1unPGjzhOWFhMNFbbHxeuE+CI48QaxELlvKIoo3xtNUOhco3uL84ikaLFEjuFAGKVWHTsUcizbTWtzNS8dpTCa9YZyZPmXoDDF0iqFAsrFMgXdICxvkEs3kKJLXt0t48SJs/zOIL3ALt1xlEKjm3MCfpryH/jHONfhyBp6TN+Zcwksv+oYpUdQz8iw0CxVECGlk3q09nsRBEqBoUhhGfw5252VVF6IIPLPy/Xq+Xq9Piu0z3Aeyg8p7w4qFik060gN9uKVT38S8/RERrVKpJ2RLJepWSlxu6bvhv2SJFp0Ky0KVYSrPVOuJ1zqQT+h/vfsZiup6Navothezo4n4exnqvRYWlvmse2mNs6JW5V/OxRimbGySU4YtYIoN9qoUSARxsvhA2OYOjityLefF5sh3cnlN9Egij4+PI7c5jZ2rtzCsSceR2Y4pRmpjeVldcldkRRCdGnNLmn9CSAYj9P1JcgZQ0i6SfQdPYUvf/n3UFhZQdiyNFuWY3wenIpieWMNBX7HQCYF5+FC0k/ODDwE9A21ZC1S+A0M+38zPQ8Zkz6sWkuzTAaVusrnBBXXpEeoUdAbqzsYGBlGd3cXjbRJzEFaFHUTNJzv0Xucd9rO5sOp4Tbfo0zCd/PSyxVMpHBg+hBu//jHxCh9DAE+Ce6U/7ScqDDF9YVL99zyOzi8erFkI7/BN444WuQ3/ZjtPKA/XiJGtTjg+FUWvlprNjyY5iP2HlKdl155GYODQ6iXK9gVV8vXu7NZDA0NI9Ddp1aGMokpLX1wYBT1+UVslrZxZOIoUfEeFr9/CUhFceDUcYzx76sL8yju5JHj4hQ3HAxMHECE6LKQ34C9V0WOCnRgZhLdiQyW7t7B9Pg0du5vMrxQa/sH8beSHEhEEe5Oq3U5f7/O/5MFbHh0yNwvujiP5PG1EkeM4pJeGgRXwokl8yVClPUIMm4enjmMWrupiZkArVuPqbV3e4S8/xv0EE9JObyjaJLESafS6gklPEaCMXpbV5sD1PXTKAMiJEc7Kl1PuPK761MZx6/pUksCjlfG4tv+d6rnBaU1kneW0hq8hrUKEarj1c1oSQ3ljYNZxrtSQbtAEI3SOmr4wue/qJSnJ0v4L12C+SKSRL/dI4MER1ns3p3Hwu072L7/Vwgw3nTH6F4lTjU8za8TbV6/RZ4XDqOLbsgpNHH1xrdw8unzGO7rQ4LxrUblNHZzaC1tIsxA35fJohxysbK8BmwVcPrwSdy+fxsnjp7E5avX8OLzL6jbvD93Dy+8/DxyO5vIz81Td7rQlH41X2n3YWCnq7TD3X3072EYRxMt9ZYHnKTQL38PCBqhwu7t7OrBYhlyWnJlqQalsl0Ukul1a5heN40AJwFnYWmms4wL9K7/G7n2l0TxJB9tE1ekkxk9doMGYkVchqI0P0uMQMPb2dqh3KSP2W+ZtTvC3qcrf093P8vHL0lJSktq4t6lIcw01dV2cRHFGuPRmDa5nZg5wS/ZxNytOyiQu65t55Dp6WF8C6KvpxeVShWlfEF7rSqlJiKxMO6+9x5aW3sIVynI9R24JcbowC5cAp0UQUyA5xuioqTiSc1o7RIkxaNJFFeW8G0KZ/jIIZx66Tm4NS5uqa4LsXd/hWg4g01yyF3y4VuvvY7RwT6UNrbQphKMkwNHhwbwrd//QzzxzBMYGxvFW2+9jjgV26ElRlOeZfy9zLn7MOnDfieG19rk7qdX/TShrkuKoWZzexsJ0p9gNMgQEpYGJNgBSah49LODxKUiJFYYCjjasBAKBn+JVOy77Vr9K3adip/JMOxYKjfJR8eIcepL67j0ze9iNJ0i0CL++eJz79NYK27Y8QXXaRtxOo3qnt4O8fdXKfswXYbGZQFDEhOqdCkHJ6bwhZ/7x5gYmWLMHMTUgWk6E7p2osjJsYMISUssterDH/gwZq/fRJraKcg4Tu20q3W+j8fixWQIqMqLG0g2+d27JURaBFrSuSDN53RtrZbXVCYJfSl6iyuK8MIkRWlTi3NEyguXb6KfQk/FE4jzIdchAEY6SYaoYI2iKFYOc3duIsNFGps6gnevXEXP6ACOnTyFa3/9A1hVacIPY3ByHE4iQtde1oqW4Vr7XRyaEPIaMHwf5u4zD31Ib5QwATEi/lwm8BFq50pzQjgAW/qqaMFSKXICXmFF47VPv4R2SdiU7lZJlChfrtVfabWaX2YQKEnNWwo5dYnVBK9hM4xr3/4ruDSkdCCkiSTri0+/pBRFFsD1ka/9UIKjk8Pk//+CCzVlG220XS9KR+giy1xUaXn5yAc/jlq5xROg/yet6U51oSvbQ+4X1wzL5PgkslzcqTPn4OwVkKSAQ1Im43dLl+PO0hoMuu+9+VVUVzZQ28jBzpV4RW0qQUTgPt1bXbsKBfBJ/FWMQAFLsT4sYYTvTYeoxUSid2dntYwmmSyhHnKNMT5LEn6AoK3Mzxd3d/Dmu2/glVc+gdGz59A/0Iv33ngdpYVVonci9ngYJ559AjsMPes5Imsrokr6cAq1E2eN/RRRR8D2ftOhcn3+vpPLYY/h6uILz2s/M6Mkwoyf0lPVUnhs7vdaaE+b7JwQ+meZ+nOD515t1MKOYTwejcf+r+5styvxXoSfpkLvXZvFGh/TxC+WNOkRz1pfePp91H6v4C05UdfflqIuxjU77UfSLfgrbcP2+45cCifERWtrCas32YvpicNYX99W3hrkuRaJSgNEwy1xLYzF5UJZmD955SqatEzJ98azKU077hA4tXcKsHZ40avb1M066ruM27UmYb6/ZcSRZHpDCw6pdMb//haSRI05QeOFIlqVGmyek3RrxGJxKkEOObrg4zNHuABxCnQXi3fvoprLw6DFj3MhQk0Tb/31DzHTS8RJj7OxuIQJhpnb9+7ifb/0eQq3jJvzc4yntCQj5Pf5+9k8x9uGo8kM8n5bFd9jEBJG23bLE7rj9VjJ55ZWlhGKBHDsyYta+ZJsVCST9Nad5x5wvXYpsSjtkPQLOeqi6YG0nh4PjZFprNKm35XvExpZ5brNv3kFKUq1l+FI8thr66uwPvfE85rE15hqmQ/acVy309s7wMc3eaZhOX3Xcv0dGnQNjHNRWteFUxd5UlxU+n1H4jG1c4eLGQ3HEJc21p2cpg63dnZwmNQnz9iXpwVJksBo2ghLu2e+zDi6iUC9jZgZ0KZ1aV6PhMKKTAUMBqUAUKuqy4vEYxomvAyS5FwJWih02RIi1iI0Y3h8TMPN1atXVNg1AT30AALskqmkpjJLxZKCsvk7d3Hn+nUqQgyXL13C2aceR3woi6LTwl3GduGA0sTgdoCo7LsSD8IHXabXQiw1b8Ewmqu392vGUtazfeoYjEVwnTjD5nunZmaQ7s1q7ly8kJT5JOFhOn5lT2zJ8XL4Xtem4zEZrpsbNJ/nz79HZF5OkXus35xDYX4F3dEEOTUZS6OseWvrs+ef7aBjr8vfa/VRTmt7pOb/5GtnRXM0Hhuuvw9HYm8VF848hmcuPguDcTI13I+hqYNI9GVV+5av3kAfwVDP8CC6CWoqe0XU6D4nCWquMeaJtlU3dxk7a6QyBDS02M3VNfT192s7Sp3CFEts++BPNsjJuVZpyYKgJaMkKbsQLSEsfUq24zc/GopMt6lQk4enMDAwiBoFNTZzCF0jA3DDRJlE8QPjB5Ae7Ec7aBLdFwnkKqgwfkmvcTdBV3p0iO+L4T4xQ0wS+fYDK7Xdlld8EYu1vH4nLRVarn5eHs2WJ2Tv/F2liQHZHBYJ4RZDyDUKevrQFHqJ/LfW1xExg5q61DhuO51GdKVTuqeLSmOFvLQsrzVsWxgLW6GvBgo13H33GtealInrUqmVVI7SpEgBP70PDEzTO5BkRuS56djn+affkhSxpib9RvCOgKVF5rHzjzOelZGn8KJ0ydJ6Kp8f6O1FVyCKu7dmkW9UlJCn4ylsr2+o65oYHsXsu9dlqyBcUiGHqDDqd05s724r2iwQYYu1ybfZFJBsBxF6VSVdEtcfoTXIjsYIUbWU0/L5vGp6SLsOw5rjXl5ewejoCN12EN/+/qtE8d3o6u5Cm9dTY9wr87iJ/h6cv3AeZrGObXLxCk1o/MQMX+/Fn37ta5oRE/Nr+cUWvX7ZXCdpy6DXmiOewdLnoLpeea7zc7rzUCxa1pNxtEaFDFNpstluhjMH71y5TH4/gN7ubl0Hwwe5XruxZ22m6TVLyI7NCNcoTA8nMbnpOse6k6lvLb7z3pqbrzDEOPjWN7+JcxdOo1Qt6vpYnz33rO59cbR6RTcjLkULAib5ePvrbZPoudMMa3jNeZ1CgPC1kYFRxkoHV9++Su3PY3JwlO61oXngnpFhOHQXO1vbXrou6O1PWrwzT02LoJcgbH15FZVcQd1SuVxCiCcllri5s804a2mmpyWxTLbTSPmM1iItplKq3KPVmQRoAWqtpPFaDBnaWsP3iRIk6IblPJcXF3Dx8cdRpTteoLs9PD2NFi2x2K7j2GNnkSJ6TjG8/M03vqOK+MbeLbx7cxZ35u9jcHQY4W4CmEZVFU8EKseXrSi6h0ZKnrr50SuiSHiQlK6ECDUWumKtn+vvjrfNx2kr3pHulCoN4urldzWdOzY4QlAqyhqkUtUUscsWXmmkE7bQQ88mnZ9B3bIS1rgddcxza1du/1uTNDPb1YWl5QWsrK5i5MAIhUzk/5mzT3kW4hP1Tn2Xjw827NY/7/zu+pVc1d5Ovkw254xMoiedZfyLYeX+ImOqg2wqjaZkaPhfX3cPgk1NZxE12pg6PKPF+lt0T9JjZRABylaSFGOq7YcBrevSGqQvWBIp8k8qRdIGo2VFw7Mecc/yXrEeoRJVKojGLtPLicficVKjgHqG5eVlJPm7ZH4arQZ2iRHEwgcZPsSy7t26jQPd/QRpLZw/e5FxLoAlus2bc7Pq7qUUOTI6qokJaZpXR93pbhHBGY/ugRa3LGla2/YaJNq688NLHDm6N8vrYxYmYtFq7hED3KPySRNdJplGV7pL23+F/kkMTyVTJBRe2rjtb5CLUxkSCA5tXJt902q05yrEPjPHjuD+wpxmww7PTMH61NlnPIKOB604jgfi/j0tuL9T2kNnx8NDLrpG1JpOZNCX6cMEuW6MJ2ZRIAs3ZmGSLgUo2HquiD5aaouuNNrbjdjYAEI9GaQSMdx95wr6kxltICgybkiRQHbeRWghUqprtZuM8xVE+V7xVxJrbdfbYC6hqsrFkFgnVRypq+YLBdVwsRbNvsnf+CzufnlpAeOHDuLuvXvKTzNcsBC91e7SOtp75Nv02Q5pSGl3D/GGi6OHphEiyu9hzN7a2MbGxgbWtjcQla7LgT7G0agWSDTe64Z2R0uZjn9+2igholcw5vFgR8/b1J2YsoKy67FJ6id8Vrok94juF+bnSfHuoEWc0cswl+rimmW6+HtdFdL1u15FHKlgFLcvvYu1W3OnuhOJ/yMYtlzxeIemD+GNS28Q2JYo4HNP+7lh2fNiq7uhAF92TPufNZ32A1Bl7HdT0dK8bZGSiRpjLE0RuZVobb19/Rg+MK4LKuVA6U9eXFriAq2jymNV0FD4fv3aVS0a1HfyKDNuJkgTGm5T+5Oi8agKRUCVxCmhW9o2Iw13Uk2RmrLrcfU6F0fChKQDBYjIBQmAqfJ1iYuitmLdlKhqqLT0DA4NYm7hnn6WsBqr9xbUynupaE0uuOSJd5fXdDErMTIC00aWIGhgcFAVcH5pGfN0+bI1JhRl3LWg4A+yA930e7s7G868cOpt1hNKRUHrxm+f6wrNEwtuSbeHlgpDdNExIuAKFlYW8cYbb1ExFxkCq5KqRIb0UNZGKk9SJAnxwPeuXEO3FepLhkOvtZzmPSNkKoo+ROvdzDOkfObcc+oCRAtbRpNuVIBB6/+mJY1p6Y9Cl95cm9YkJ6jjGeQ11+vREjqTkGI7LTea7NKtjIEBAoZMDG7UwvD0JAL9XUTYveoyJatrSvGcGl2gmyxT4IVqSffhFGiBshtBuhGkAC5VlThB1c7ujsauGN24lBcltgXDAUXZilhNL4tk8GdJJIgnFFeaTCb079Dm+BgVbQ3nLp7H6JFpvHP1MixStKmJSQxPHlA3ahdrSuEmuTiZyWEstUtYLVMJpaTH6+qhAvcP9CPB4xZKu1jfXCE/Jqh0qzwfaFLFy/J5W1gERYtuRcn569UGom5QdxxIWVRon4AnidGyxmWhSY6lMRwRAiuuXYvKcHvuLi69/SPcvvkeNpc3dL1dxuO49nGFkKH32ltdQiISnAyErd8VX1J3KEUe89CxaVifJsgS7WrRgmyTF2m0T/Mk/3vJwTreRhL4EcfvECX8D9D9+GMM8nt5dJFHStwQ1CtN5CWnAYEUMqpA0OzQ2LCCLPomusC8Jh0kZvaQA2YZjw9MTKgGR6jBFQKhjfVNIuIcEe261whgeD1dYjUxumvpehB/IpoutMLy9zPLOersDknkUwhaP5W/SZejPJMObRZymPzi53CgizGtUMbZC49xcSt0gVy4clV7rtf5nqEjU7i1uYyKjJ5gnA+FotrHJVk0CUPS+3X06BEKs6nUaYtAUvLegh2kABKLyU78hlqTgMRmrUyvVaShNOiNvM84giwD5n4ItHV7Alc9ZOjr5WoBM4fHMUVlW5y/hw2ux8riEubn7uC73/oGmlwjq0lPRLDY1ZUaC4bMrxmGvdEg32+0ahqqrE+ce0ZRnXZLqoCdf0FrOOchaQ8ZeuJ19fc2v7htGtqHVBNYTy2T3eR93X2IE3IPM94mU9LlV0bYNhCgG2ys7WL2rauYvXoTGck8kWvubu1gj8oRIJru6utRVJngIo70Dyqnla5F6QOWHQobm2t8b07dqmSIVHA8L9nsLJvMhJKoWxfPQq4p7kwVIRr2kjeyiBSuzfducNFkwURBE5E4cltbKtzKXkGTLqIIVS5ympz+1sqCFgFk14QpA2kcQ3cWeBshLOQYMyM8Bo2flj2MHmKLerOi1l1vlInMw0hlZHM9rTduItMVQywVJgsI6iYyKxLUNZUWpJqGFUu7XPL5MuPxNoYGg3ju2RPYWL6vPDmWTNJTSEcIj0nlWLl/VztBT5w6SgUkl200aPiBb6aEhpFBNKm4AY0NPvJjXEvwt1/otAzYnd3thrctzANYjhbwlToFZJsG+R4F+dZ71/HK6cexdHcW7lIYiXRaCxjBUJyvzTPWFnFo6iDS3VlMTHZpgvwuEWpAFISWVCJVshnHg/xS0TyJi339Y+jt6YHDBVheXsLi5iKpyhZ6iMxHRkYVREmTuSBqsW7L9p69jdYyxMPydxd41xMMRBh3vW2lmnygq5OQ0J3K0CrDaNJ7lGhhY0fGsb63gwqVBdIoFwxpMR9SXPcKh9qSlOLnJCGR7iNQbFd1c5js4F+YX9CdFiny/m6eq/BoybtHpTOFjzhdtrS2SuzNWBFv2w1dreO0VHnabSk8VPHyK2cIuN7jZbg6HcAVz9goYKO2i4GeLjz++FP4wauv4o+/tobxoVG0ytVfIND4Z61muyweQpQmIOjO478UYcD9JAUdh585ESsWvthpuNM2bmm689t6BA1qWyovdmF7HbtuDf3SebG4imi1qRpfMPa4wDaefP5plEKMF+Rm7XKBgKaq/VJR6ZcuEE3uFaU4rbsWpGIkfcayz0g4nxQYpqemMTI4RKWYwx6FspnbJNCIaJ/UIaLjLgIQQduS+FC0TYEUCeSkgiSNaeLGxa1OjY4BexVUeQxZ3AqR/tbSCnqiSSSJpKN8vre2ijutPOywqVw3Ils2RcKWq0maplS0eO0rq3ThBGYFKq8Vkj7mHBUyi0MHp3Hm5EWeewwyd6BeafE9e5pPr9JNl/I1VAwqBN3rDj1ImKzBofeUNtuA37nZ39+LP/+jPyeXT6C7t4/c/Yh2YcaJ/hlQsE0LN7pCGJgaxN23r6KXuGfHKcSHJsc/2ZtIf3mdFj9/cxXG73zxN1RzaqIdodZf2gH3fZ3OMSlAt31OJ6jW9NtQWt6oEfJHh24wpjG8WaxjIJ7GK2efRLYd1CL73P175Jt75GbH0TU8hDwP239gDHW6IKnmlOmqU1x0rhgqdHe1orSqtpDb2VUw1qrWdVhJKCCbzwLaziNKJWhaqkmra0so83Lp8HBu5iw9w7iibvgURFy5IGuvGmNjnLE+StS6TSAlHY5VUjjdZM3LEhwRKjWVGkVPTeBabhV3N1ZRqFd8r0VFoFLJlhbJhzeoiF3JrFpoT0+fdC9ieKTbi6EO6R6v321LStFCo2KrFVcJAB1RNiHBdoOeqalQO2B5RQuHClRgXDVMRzs90hTsLuN6rlbwNtCTeoYJOiO9KSS6kjqQJcZre+bEGRw4cAB/9t3v4NCRI9+j631ZPMvlt96G8duf/zVtrKu79Wwt3N5sB1zL1L4ir527DY/PaftH29jfYtry+6ZlK764STlgi672iWNncKxnBIdHJ6UzEGsrq9hZ2US6K4tgNqMd/ULms0Tc5UJeq0UyCsKmxbcoeKEqUpSP8n01ukyxUN3QzWctGHBxZcO30CJB12Ltq3urOlhNigG9iV5MiCCjUW1CkGYC4dKSw/7oZz+N+Ru3MX/9JroFFMr8LHqfSgh6/Mr2ngK0xOQQlsl511bWNP1YDZE61UvIDvUqeMtmewkKM4yFKciQA22il12ArYIObzO0QT6kLTTikeq1ppep4/ElEVQtljE+PMLPx0gxPcApw2+aFLpsEbVpbFt7WzATUZx45hy9YJ0YZBfuXhXry8tY3Fnjd9bpkQKIkRZKXV2a6hWJu4adCCf6qcS7ktIMtChCGfFDDvxB0SGn03bS2VDmaEudV7aThz9BJiAETzXR9vbxSEamJ4o75JgrtNzZlTkdnSQdk5Ju6xtIKjfdJYrulnJfjHGt4dVppbUrEA0iGexBiV8q7kysz456jeIhia+Wx427SPplftTOzo7meiVld/7IeU0NSppOmgclwyUPyWBJ7vfe/Dwee/IiFkgnLl2/gnCRVLBcR44KViEwyzF2VlBHb6xbz2eXcbSrqwvHTp7H4aNHMXhiClfmZ3GbYEcoYks3dzWRq+xxfSzt5xIYmmTMFZPwzMDUBoGm5M2l7Ua2vwYj2NlcxxOnz+H86TMETls0ZEHYTSQJjIr0FmP9IzznIg4Mj+P60h3cuT6HD/zMR7C9tY7V2/MYOTmIZ9IxRJJhepG6bo+9fe0a3rt6HUcPSTdN0nJbrQ9YM8bvD3X1wPitX/wnOtrANlpfaQTszzhebU4XXR46Ukm2VVCIsout5e8h2m8aNA2/OYyxilq7vr6Giy9exAj54l/8u694Fyn8NxiF2XBw+shJXDz3uHK+/FYOsVBUrb9BgBWtt7Bxf0mph7Tk7FKImrqT9CKfhYoMDQ1pPFwhiJG8rXBjbWsRi5fMlumNK5DuyqXVFaUuVdR0ttQ6Chg1B9BHFxqjENokcyRA2oXZ20+OOzZED2N6tVgi6joR7U5xD0efOIf0xDCWaV2v/uhvdRNbnJYs1mkIeg95dExirETQYCCsM0tk/dotR7e0ipeqF6rY5vV99n2MgjIoJR5mzO7D3I07xA0RjI2PYY3rJ647lKDBEFPMry2jyRCazaQw0NWnFSmHHFlafiTrJ7y4RY+QTCQRl1EWlFMsHP6q1XY/a5LBWC+dO0thtQPkZL9NkBV2/cl12rrjenNwBDvqGAZpJfHndHT2iGuBWnYZymQ4/lyvlPAb//lvYOz8Oay99gYapFAhclyxJFfbcog+SS1CynMc7bqUOqh8NieVJi6EdAnKQxL/ks2SQvfebk6FKOhaukiknCjWK7vpOvVXSchL90aFLlnyssKlkgQlPams9os9+dhziBHNdpt0jUTUEyMTGB8YxSEiUOHx0rItgjVpUdJcLkkJca0L9EgyrUfwxvThGbpuAVdVVQZXjDXgzeQQjBJwpY01qBvSpBFVkHKDx5PJA826dLtUcGZ6Gv3dXVih8AZHBnBw8hDW1tZIr8o8/mFN2CwurWrP94HBMVR2i8iEUnAbMqJBWnrCOu1gsG8EkUAMTo3rhzDXJ6qtrKXt4sj99+Z+871LVxzr/WdP0/js045p/1P34U1nWhnUMWhaurK0ncTwOgY7bSWd4S26Px+kPlVkImFcGJvA9t/8GEepcc31HDJmBAczvZgkaMjSYvto0VHJikn9lYCqtL1NVJvXTkxJNXq9wgHdC5Qin5NiuFSnUrSaBjlrjpatO+mpdJKPFqByYPwAnv3Yx7kYu1ycRS1UCFicGB9HT28P43ANMwcPIWEG0cPYeGzqsFK5OMGKIZ0VjMVlp6GAUhVYwoJ4LH6ftMTIxvHhg5Naj50YG8ede3fRJG9th7x9wzrRwPHGNHZ2HWpJj6BKlEHSurJ2NaLog2OjdK1ZLaBcv35NZ1Eee/IJ5It53KHQByZHtW12cfaubkERN7xOBRgZHESKYFBApqRuZX903W82SNLCJectyaS7V94LF9c2vxY3guvWy+dOSfbq0zy3Dzmmuz9RxrC9FlHPNVsw/SENXr3Sbx/tjBLyyXKFAu7rzWKsrxdf/X/+HVJ02fVijRy4TCTZIP9cQ0NQLt18bmMbxT0iaQIpWZA0XUyQz6lY3Nspb3tdEJIeFSuVeCrUaZeWXOYx0nRREpPj9A7yvudfeBHbBCDvvP22umrt/5qe0j3EQptkn3CByLuXSL+8uYOh/gGUm1UFiZJ1k7lZ3iBUS2vQMm6pQcUSIUfoFUTAlnSCCL/ngvaNjeDu+jIt2NwfWRHwBextKaXwdZxCXQUsCiYJmRIVeaBHPArPhz+fOHESm1RyAZBHTp7AIq+hydAgVa5spluBpNQEpP4tFTDZ0iPClLq4FCPEy3nUMIwkFeXya2+iSdAVdPFePBS5ZL1w7oSg5P+UQj6mvYGG5e0ENE1v0KcOBTE1syStmt64vwfC7WRChBdXGWvS/T148Uu/iMc/+DKKm9tYpSAj2R7CN1MrK2L10r5ToJvd3iFi5PfJlJmIbKnMleBWG2hwQUTwMshLhpTJd0mZTybgiOeQ5Id4jhCFLm55YWEBN2/cQFF6haWt1hdoUspu3d3aiaFbYmTLCl1vVyimNCosk21cr0igVTNbk8TaKChJAkkWSOZB0pdJWs79e/fQTyuqkR8PTk3osS9fvowMn1XIjjdiUTJs9WpdObLkyx0dr2DpWCmZKlgp53H0yBGi6SK9Sy9Gjx5TZS8VStqMuDx3nzy4B+GebsxcPK87IyUUSEJkmdzb67b0vIOkeIU6ri8sIT+/jNL6thoDFaHSaLe/Yj139rgUF36TC5P25mFYftumpQVn0/GnyUqSXFpGfdetD02dymumRuUqFUDIyvMf+zDe+Ma38J1vfhu71KY2XaJMd3a1tYWxKUiEzFiY4yKWuPi71TLBTEFPVOqu8r5N6WKkxcR4siJMKUSIMMUdS1wThZAuxa2tLQKPKgWa0eqNxGiJzWlaWiKR0qKF1E0lR61eQLolGt5cTWkucP2tntLY16LiJelBJHUoCRypSklxv8KFl3psMV9Ahaj40IXTKLRrGCXvlnTr7dt3pC9HB7YJd6/TWkXAkq2Teq6j4wVdtcIWgVG5lMchfjYV9RrbpQVpj+42Fk+gJ9tFrxTBnTmykFRMXXWGFt87MoQULbfWbmhOukhvJPOgpcwoW2Zzq5vYnJ1H3Axprp5sIElj+C3r2TMzKV7sv9RNZd7oURWuuBh1xY7XyC3KbKm1epu4Td9yO58QK2/UHYz0DOHc4AS+/of/ngtEjkml2KJVLtFay1Jop0bvSO8VQVeRnyP1x26b6LlZwXqtiPn8NvKMhT0ENWW+t8QFFE2XPbxiodJGura2QutMKkuTuZTS12ToVpumFjdkd382m1W3JS5SeaZYDwXfIOqUXYgyrKSuteeIXmNEcuu5vObDRcGkZ0tC1ubOphYOLAIqmWqwQZc5deo43AhdO1+bPDaDS6+/ia3VDbIIaRZsKU7QoaA6t8rZL6iLO5dmu2qtTEU0MX1gwmvv4fEzFNJeuaCJlShxwdDECC4z3HTTc+W2t0gJK+gmku8b6ifDiGNzeQUVCjklTQG8xraEge1dnfvdpmKmY7G027L/J+vZU0fO0BV9SSaOO37fhjff2XwwMdYXpukLtLPVtCNc/ZvkZ4U/0DpuXrqs8a4pHYE8AcjQMVqRcFRp8pZUpz4sSxMmLQvaF1yhdsoFbpOaFGjVe/78jWDQ0vg+ODqCEWp+mgj0DNH/HvmyuDgBG9J+K1YrqFkWLc6Y7m0I42dpmTLtTlKMgmKFl3f7C2pFQ97YKF5Tg2FBvEi6r8eb6E7QJ7RQBp5IhUjWIa/bV4HBo4exsbeN3sF+TV5ce/tddCXS6uFsp9NX7u/+93fsiYsV7xCPR7C5vo6TR45iZGgEJQIw2WqT6WM44VrU2+TkBKRcMJSpUGPDwxqvVzdWsLO1gQM9A0hKC/LGFnKbW0hQ4IJLdqkIsn1WvIf0hMdCka/TRR97gWL4pA5ScTtjjEy1WNcf6m12Zl7BfGhOo/eadOPp9P2mi7DkqG1PQyXLJdxTPIMmQ7QPyRsprGDG7+AXbdEB4FLD5d/EJQfoplsMaPl6GUW6QtkJALqt8dNH0Dc+grGD41ghYNvkxUldtVKq0v3GcODAuLpx6cQU6mQ2ZZNXQAduyznGpfOy7XVexGglkZ40Ci268P4u5GslLkxFwVY0Q05JDioUSeiZTsSnq85ToSTjtE4XOT19WGvVm1ub6M324vI775KqWN7cEstTYNvfZB1wvVyCuPGgzsNiPLXCuHrlPV1H2U+VJkOI92VQs+tIMa4mqAz9YweQIyOQxJEo5vbSChZvzmL55m2szy8gv7WjeXabX5vuzmBtY0PndMqGA6GJ1Wrtx9YL509+nN/xgtdEp2NGVND7AtQZUKY/4sgTaEcr5WHJ7+3O8EJHyb4UJjopTsOfj2R02oL86TpOp+vhoXloQcOjPpKTlTJaQsAFnyXVuLYuXSFNjE9O4NVvf0exgWykLjM+Zvv7MDA85E0UCAd0K2aWvFdAWpBnVSf4EzRcoZbLbohYNoUm13loZhrre5tKdWTvkri+XVpMnLFOzktit3SJSspVuh5lQ7dsh7HpqgUNm8mo/744rly+qpvjJU/f0BFIXtVJxzmRs8oeomaz5nWHSg81z3WPnurd62/j8ptvISXZqXgY+d1tXH/7Mu7duIm3v/8D7RUTcBiQ4eryaBvaPSndn+cvXMDBI4cRpEFE6b0KtYpiBB02b8iUWus92T56yPI3ZatgDU+gkpkyfsKMZG+ulvkPb6Xc/9nEo1u0ftpnvGZ6iVHtpnarkf8SgOzmcWb6CEZOnMHU0RnMkhtmU13UcNKpZls5bpBhQPYOHT5xgqBnD/dvzaK2tICUFYETtlGke5dsXUZ4dKWAtZ1tHD17QsfrJ3sysLhA2UwX8cIG30/+Sm45MDqqg07Cu/QGhV0kw9JsZyHMBa5xvW69cxnoz2DwxGEsL84jR+tOdw9gfTunMTgRTcAhBglLXKa1hqRh0KSiRQMEkgRPjOGTR0YIlKJI0bP0Dfbg/ttX8N7lKxq3ozLom+tyfOKQImUxsG0CygDlEosQOWf7SZV4PRT6MOmg5Mx3GaqW780Tu5DP0xOlzdC4ZNYGNFYZQa9b0TX8JIah8N6AsT+5vdM/3YnN3jxLF/8/x1395Al6+1SF1ivdFbSGdCqJX/v1X9LxfFECpvvLi9gl7RqKZZAham5V6nTHSU1SHJk+iN7xUfS64zj+1FO6p/j+jVsETUH0FKs6KaBJ0NayGGPpfveIZNOHx5F0a4z9BA+kPumxfiTWZaDaJkanDiKUjsNISDnUUSWSuvMuFShBC8cuNO4eODKNtbsLiIr/slt48uIFzEzPaGdpq8b4vVvADqlLgfE6ECFVc5v6fYJHbt+6iWeevEhliOLundvYmV/VbTNi/VJNEtwgGTrhyKOM1f1dvWq59Yat+YHZuXs4/9xTCgoXVkiT6H2kQ0kGm7akJXcn32e98tjZX6f5DHVGGnXyUuqkbd/4HH+fjOvdXqZz0wrZMiLv68zNNjp3ZJF4HvQK8Z3diY4/iEXbbQxviEgyk/F2w5vQC5Z+qmq5qjsOX3n6eXzp576A0k4e28trdK9ljaN9mR6kowmlczr9jceTHGxmgKhbwJrcGkhuekHLDE/2IzNzEJvXZxEiYOkd6NX04vDYkBZFTF5g36kTjGdzGKBymJEQghRoLBXn8XpgEsn3nDmFULmOvolxdA2P6ISf/uFBLG1v4NCxoxgeGSbf38Lpo0dxiApz8fwp0rCydlvcvXEVd26/h9XVeWxvk9rQJW+srSnClkrSuRMncZi44bXvfV/R8MzRI5r7lhr8yXOntaFCRgV3y06PZh2zjL92vUGsMeFtHbKb6BnoR5mgbILHCdKj5ZZXNXUsdeVkJllgiLD6vb0w3g0yOlPOXaeDmL1J76a/H9ZD1a5vuf5dRh6Y4SP3XXpwSx6favF/TdnFINUoItkG40Vur6gdHGkK5OjJE7hw8gwGIklMDx3A9bfe1TzsEQIa2W8sY3+FW8rcjSqRYqyrC3OzsxgQkCJVqahMo0nraIg6kUcgEsC1197E0q1bOH/gIF0mXTIVQnpszp44jvvryxjcJEgiKi9Q+81AWIHhGuPglatX5cYg2kI0f/0OtpaWNWzdZkwkyUQ4Gcc2ufq//tf/GvnqDsL8z4gFiP6LesuBvlg/sr39urvgpQ+9rAmdMj2TJF8S6Qz5breOYPzrP/0zZIJxTI1Owiaij/V2a/7dZqgoUagSLhIU0OLygqKaofExhpw8CuU9RfuCewS7XHr9h1i7OYdJKqHMwJbN+ITX/aSIVlJbchyPDnVGAu3PVHpkg/ODkcP/3/HU2o+7RmfutKDkcAxFgqI8gYvcS0GGkhw9dgKPP/UEZo4fQTKbQYjfuTe3hHdefwPuXhnnT55TxSgxrmznlnR0X4uCM+hmQ7TcXsbKPXLQRGIXUSJIuU9SgJbYRR5cnL0Pd2UXT599HAEKd21xQSeqC2IWarZZ2iP6fUd3Pa7vbGm2SYDZ6vYKBsJZxAge37O9Ls4k/5NRiTHGw6pMY6dCvfnW3yCR6saHPv5ZTIyM4MYlImnZUhOQiexNFGltJy+cwwiVa57xM903RBxxXIHhNhVrhIKFE6TiAe+QWk49dQZjBH75nR1V0rC28RA0SlmTj5FhKs1QN9apbM12FYX8Fn7w6iI2y3myihUMDwxi6gitu2Hrxr1Gu5GQQWjKVnR4v85kNh66/8SjwzD9+2M9NCzU9KbN+oUHz5ilS9D2a03O/vRYseCFuXn0cSFeePIJHRjaPzSouwV0XzIPWKKruTt3Fzd+dAkJks1EIIgdaquk8LQbighY+rCEU8kdzHrFCqiURVrW3WvXcTzDeHbwIIFLGLlb97B06RrsXBE3i+toN6r8WJOyDOpOhaXVNZx8+jGMMdbubOwo2h0aG8XVdy/rnC3p2d65dlcTCDWevIx/ev6ll3Tn5AKBzGYxhxefegFDk5OaTKnulXBoYAwrd++TDrQRC0fROzVOgR3GW4y1kIb0gWMwwwnUa3ltDSowJB09ewyF3DpqaOLky4/j2nvXuFZ1ZInQGww3kumav3cX0ZQMkwlhZWMZG7urVHaeczkEacyOBg1MHZxElMqXk85NwQwykdcwyQjNQFzaUSTZKHXIzk2eHsa+3pYrD+WKG7Z8Ei/Uw255BQF5PRQNeUNDaW3SoipuvEZqkmdcufD4Rfzs576A0UOH6UoDKLaqOtnm/ltv4MzMEU0jfudb38Tf/vAHqHOxBLXaBFHB1+TWbw289OyL+PjzH0AtX9acsSzg3sIyqtt76I+nsEKdqq1tETLmUFpZ0emxSlGCBFmTwxieHKVbDSPQ14u5H76Ocx95BcEnzwNrq2hs0t0RmUoTgFxiD4GOS8tszW0ScVPBogY+8jOfxdztWcwRoceJzqNmGDffuY5jh4nct8u4fecm5epokUJWc4+UaPzkFOYbBTS7Y5g8NIOBkVHYxRbcgoyW2KQXWMPSxhx24nl87Od/BsEjvZh/cwVF0qYL08ewQ4sRN10LC72q4cr1t7j2XneokwqhLdN5KlXSMAsFovdwJY1wOIVerkejVYHbasWM//Gf/4rc2yQi1tvWeq1XB5b0nezTkSnikhwQjqk0yvWqJgZdh+wNrpaqivokLVjhybTkBk7JmFY+pB/q+PGj+NSnP4mxc+cZI+6iyO/I0SrD5JxDBAgCqK6+/jq+/c1vY2l9Cd0k7DJ0TGiCTDyXTog+utv3Pf0C2vyuaq6EMhddcr5JfmdxJ6djdCWLVWhWtcjenUiqkslgE5fuWhSqTb4QiAWxurKq7vGj/+SXsbG5Qle4i/oyQdLkYbRlc9rqCvkxvQB9xo2vvordlXVEJge1k6NSrOh3JYNR3cp58PRxrS4F+J3CVQtEu3Kzrfubq7j4kfdj4MIJlBnyE6kUUtEMnD0b96/MUjFXceW91xAaaev9GmXP1oc/91kCwyxMhqU3/+ffxd79VbSpZFXd7VnRDtDadlHndwX0PklhLYd2dfVgfGwcsXiG3nGUnjiAues30CC2CcGo04LNqqRipVtDeJ+7fyucB/y0M3Ak0PbmM3ZQtMzgkJ4pQ3a3M5CYQWnEa2J7I4fJg4fwy5/+LF3xGc2N3rr8nqJCKSBMdU/A5AmHcjXcvf4OaovLeLx3HC+OH9XWFfHnOllO7gBGTCDVpfUfXlb6FJAxvOJd+Pk6AdtuYY8UYhh2yEQm0a2N865/J025Q8kmBVirudrmE6XV5+iOT144j82FeQQYxwNmEPHBfphdKc15SzyuEtFLRs2VdiHTwgJdvRkPI03OHSV1k605BuNjf08fmq7czymorjvCYwTomZ65eBIHzp/Atgi3O4WEjHFaXsedH11FqGFos/rC/CxmxL0HmlTMMkqVPAZiadx+87oObSsHt2BUWsgwnht057LTIdQ3omOIA0ZQuzVlQvTE9FFkB4YBnl/3WJ8CV5OArCI956FINWAZgYLr2t2uf3czx3j0Pg0PT/Q1/dnClh+ka40aQ0tK66NbxV1NK44xFvyHn/wUpp99Tmbsobhb0txoyIjohjIZbF4ll3zntTfw9t/8GF/8xKdw+uAMyjwhoUpQbQuiJjeL9L2HDBATtC43p0wko9oMULVqyJGynDh2XOvCsss+KtPiGGOLlSIGh8e8tGM8RkoS0fppkdbf291D7noE99aWMBjNejfR5Hk1qCBt6aUmiMvlizBDbbX2TNvACHm3pE8LtSaFSI9C1L5J0PT69/9Gi/b9Pb0YJLbIFfPa+hqnMsnIBzGY3ZUNbJEJ5GaX0FreY2hJarPcUVKqIOPqZr2ChkFv05WAW2ri8o/fRrxWQKab73NSVJ4ElSqqjfJV0q+mFDOKtGZiCtnpHwwmUW5JUyJxBxUnGrEwcHBc8wVmMFSQO9jkeIkT3n33BMA4GnwfTm5IG4zcmEluC2e6D4aCx6jNkjVZJsmWfbYf+cXPYfTEMR13t/3eHG6/exPVXRmk4mLm0Dh6qflXb9zC9998DcsEOTvFbdxYnsf7Tj+m0+XCqZiOBpb7He7UK7pjUPYziRAEeXb1DWu+uFoqK7AZIg3JiFXSTSelfWd9mwuVRoSvC/kPy32YeElyo0eZ6bG5tYHHXnoeNkGJ5Ju3drb1+gbHeNxSTYeQSQFjqPuATgHqo8t0VnLoomVtENmGepKa1w7rtlcbbV5bs5WDtcfPbucRy6a1s7Rb+rXDcVT4GZPfXZidx97N+8jI3OmY1NQj5LJ9mGssotoqoKs/q03zVimEw4eP4cblHyCdScG0g9pC1GoKPmrofq4KlS+/SxzC73j2w+9HknihGYtjg2EvS1wg13D4+DHy4XXkl9Zz1seefvITFOyUAW8DuO50cLx7MLj+fXNNf86560NpSbO2+JDSmU0X9h/96q/hlc9/nggxguWlFcwSfKyQ6tDXEeUGMDkwhFatgncvv4FX/+YvsbK3JXe61Pzx7J3b2o14QtKMlZJqjuSdU4yjY6NjiMfiuntCZnFVqaHSVCCpvtHhYd0oLV0XktZcuDennDHVleHFS3U8oPd1kDugSd1UqlFihdHeLi1HGkKlGD+lBCnvk5tmNIi4pT87Hoqih/HPXtnS3m3TH6cgFZ80hS5Vrhvk1jL1RjZZ5+kppAIm0+akX0y828rsHVIuKv/tu6gubSIjvVr8W1QsJUJvFCZjKMwh0BPQcRB3bszyO3v5NQ1YaT6SNvZKOXqFPa0+ra8uaV94gWFkmDH3qRef1xy8w3UMElt09/G65TZEXL+N+4tY5SMWDF2VYaTr/sBeFaT9d26h07l3sMZhcYFt2VnoouG00Tc+iv/sv/ivYCS7sL20iiY/FGwHMRDthd0TQ1S2Ssr8qhoR4M138cadt+la24j2pTUzVpG9jARA337nh6gYbUWOoSaPQb6oN6OmFsk9/QRFO23ZQM6/pZNaHpN67d7mtrotATe9QwPaY5ygYqyTKjTLTe1+lC7MEGNa9f5dDIyNIDaYpasnOk9EvaEqkiKl69VNffQCE6MH4BDM7dy4o62o4UabbjmLSqWo9/+VZryGZMyiBIDxpKYFZ548h4lTJ4nIN/G1P/hj3L56VZF0//iItwtQmgVW1+Ey7MT5OUPKP7ItVapZRknTkfVGHq++8WfIEnH3jYgvpSJnCG7X6gSmyzj6xGE0CibPMY2K7PzolptBt+GQAxvVHQQbGYLGTbz2xpuwai1tKox196xbn3r2mbOU1/Pa5ChzoKXw78/ptztznnykJT1RelcumdlErf4v/+v/Ru//e/PKe0S6/STsNvLrJOlr64ypBSLtFhemgDcu/QhXbl9Bk3TDiAe1QiMlO201ldH36QRmZ2eJjBs4c+o0SrQQQfM1anyJLkmEK71IIaLznn6vD2l7a1uHgovuHZ6Z1kK/5DxjiZTuzpeEhNxcShZv/s4dbBXy6D4wjEhaEvS2NsRJvVUqQ92ROJqSEp1bRHl1G7uLKzrKyaIw5Rw0PkEm30b0xssDDDfijmVQy8HTR9FFQRYJ9hbn7mFkYBADjMkTRLSy1zdJpmHIcfJ5epu8UkkzFeZatDG3R68TcRWodvd2Y6+2h5ZVRKKXekTBC32V0UonR6cxkB5G/9gUdhma/pff/x0V5s0bV7C+fB9NcuKGbCKQ3Zf0duvLK5ifn0ckmfx6IBgKL9tS2G7bjyQ1dKe/5d1gUkCUycUwZVBKKKYx7Znn34foxAFUb20gYcSwzBizt+3dBFJunRoiqtvJEYi8+SOsrM8j09Ol1RvhMUnCfKVlBGWSU5Ymszi1+dL8DeT+tIyPP/Oibqdx6o4KKppM6u59mbm8S5cpd1ApE6jJpqx+8toif5cy39DQiM7dkHMMyMRz2sH127c0hfn4i89gSbajCpik9cuWzpAwB3qI4i45+b1lOBt7KC5vKL+P98Wxwu8MSfsL3yvKqMPFCSxlCsDRcyfw/Uuv0aPEtBtS6JL0iAk2mKACSBNAgUpYoxIGJbXblp3XbcR7E7SsFNbyd3U/VSFQ01vd75b3UEdZO0qDpGGVvQrq27uYjI1jLDCAG2/dx9BTw/jyd7+NGBWoTRA9PDKAYeKa8d5eZIZGIXXKd3/8t7hzhwY3OoF2LLgUCAat+ZZt6l24H9xVxaswyP5Vx78nn7wuGuztXG/j6NFjaNzbxJuvval7imT+ssTLAPmiDGG5s7mMa7eu0h3WkBns8Sb0SKZsvxvE6dw42N937GgRfm5zEX/03a/h5adeQE9vmty3jkXySr1dHWO8dGdI4VwWR2+XITdllOPKCAcCNenk2OOCS6ttlYsqu/NTw30KqLQpvSb3P7IRZ/iQO6zUFzY1eVJe2UaL9C5Di5PWnNnL1/bvWNa5j4RMfZc6tzQCprd3dGvMLbrxl158UXdNbuT4OnGF3H/7/uJtxF0LQwRBi3fvEYGX0DPUQ26eUkHrLRLaLc3OyS17ZCN4qosMwXLoDcggakEkAoOMo8P4vT/4FpV3Bg1y7I2dDUyPjeI0gdTh4VFeB8Ekz+nNv/pL/OAHP0KNgvr5T34GbiKN777x1kLAMNz7gYBlB+y2pfdUsP0toq7z4L7skmo0Zc5EQHPVjil3XGni5s1bJPF0VXSF3UTUkrh49S+/o8BpfmkBsUxUp7NXiIhtPxvm1ZhN/zYC7c6AaQ8DyIiF4QFsUEB/8uZf4vjBwzg1cRgjPQcQYjyukeaE5Y5bVsjbkdesMX629A4kcprStiPdmrmtXXRnu3VfUoMWF5bh4LJ9hBCkScpSWyMipnLtLC6hKPcxJN90GXdlJEJZhp/KZFsZSubNBtZhOtKmUyYYaoctnH3sgjYMjPUNYpfn+u6P38T44UPIHhhCIpPA/M1ZCjyNmBXU2+TNrczDDRtIUIDBOBWxVOD5lPV2dTYfcus+1+J3co2C5Ln5DVLGfABD0Qm8M19B4uLzeOqDH8Rv/st/gSQN6EMXLuL4oWlcJ9BbXFzA8v37WN5awJlDp/GJD39U2cZubs++M9h7P+C49oJhujfNoHFCCtT79UH4O95cZz9VKXOawqbXZnPz1jU8/cTH6e+3yNniyGTi+MZ3/ww//Nsf6HT2rv6UbrMIxsKa3ZKY9xPvrOZ7C3mW2RMyjS6UJM2IRXB56RbuzN/F4Z4hDCSy6EnKvqSYpuuE0EuXv8PvkPGFqjSMMqNE7FEziExXl45S0J5hYgMdt0uuWFzdRIF0yiKwKhOkOYWKDgp3ylW6SFvvISy30Gu5LX8KvtdVKgFMPMKBw9O6bWV9cV4Zxmj/ENY21vAXX/8mTjxxQUuR47QsmcF5k15gfvau3q842zMAJ+DQy+zpnqiWI9Nz2tpxqW1L0uNtkObs1hnysvjAcx/EZPY0QrE+4NAIvvlHf4htrvWLJ88hQkT+lT/6E82aySY12Wv8iY98Bk+cuwCbnrRC7xkNRW8+fuzogvWZl56Wbocnmu3GqZZ/BxTH7+7XOYzwbwujA0Us/86cDsEk0XA4pUNTWm4F//Z3/1/GvjzIsru87rvL27d+vU9Pz75Kow2hzcgSQpTBlmwh7BgbXHHFVfyVpMqpSiUUlQQolwtXBaeKUMF2iBMClMPmRWBBlQDJVCFGZDTahxnNPt3TPb2+7n799uXem3O+3+++1zMocUY0Pcvrt/yWbz3fOf9Fzl86C99HEhDTOQyw4pwdNiyqJlBRAjHHUvUZmimtWzvkZKbPp7omCcZ4U6lPj7+rNLbkBr7eunFFruADrXeQznBxPMJxAh1MYx67q4wADDl3D+kJ0ZE0vR6i4GXkocsXEUCRA2SlAnO8LFX4Whf+l7IAPp6D9TFXebysEoGV2NXZYtfXzIFB2pHbb1eo7Bgi62KhpGopFPs4dPiwvH3u5zJ/dU4Pooszt7SwqB06NjhG4HfFM4N+xHc1w5ZEJbyWH2lJd6w0DpNckHsOPyzvOf6r4myl5dzJV+Vzn/1jacKPL1++JjNd+OdGJC+9/rqsbK1jETsw1UflVz7wiNx95204+L4hYst41H5+Lu+lnvH7ETe19yo29Z8Oe0XRjltsb7Kyn6VMjxiRGvuz+/fPSBuB13/9719CframmkSR21P2VOYdfccA4gdM8JboeqDY6RpuDS6gk3B0JodBl6Hzi1TIg46TBGpkmSO0po33s1Bfl6XKGk56RvxOKPffcQ8Cjn1YhDklQ6tiQZYQvXOji6rr11XAHAszDBZ3jSBIkbTUlle0KqeD7njPXStnoBZL57MM3RGDIAILmEVcQn47PTWlmkc8fIzuO7iBR2+7XTHNJHZbu7YgZ1dXZXVlVbII6Aio21xH6jKZVmGOUmlUJo7tlr999TvilFwpIfWJGpSDzcjV09fk+XOnYSFWJImL9LHf+x0JcknpLotUcZiWkKW0kGDedtcxeeyhe2XPzJjip1maxO0yPV1OOIbRa2QT8CPSBkTB6yFMhuNGA3M51K+3AZc2kkLNF1kWazTa8lf/68uyulHhKDjsvgl6QiWx7pnFoenbIebo7ODH1/EO0t1y/jeZ0BCfTk/LyIMH2QBHiTn7GoGTE8THB+Hkfh/3brNfk32PPCijyEmn775NyUWJPmIa1qP4BQLAhTPn8aNNefXMm3IU0SaxTDxUTEHIU7XZYRWL6uKJoUimo8TpUkSMsX/ffllcXZbFuetmCgJm8O2fn5MeHj56cK8ceeAeWUTGcOWVN2T10jXFZJPZttJaQgCKQ7mVk1Q+CUsEd+USUnBd0lMIWIOErCEeIApyBibcrwaydHVJpkZH5K47H5MDCKT+96mX5eQPnpfbDhySFCzdkbsPyt49u+X+d78Le9JRIKFbbWmu3WZJ1zfr5Ef913k7fMPy3nszCPtX8MkO0ufol/qFyGx2HOnyHim1nmgl6sb6DU0N2l1T5uSoaBB1zcRxFGvpptR/ebagPcBUO2ZikT/DhdUIz2onOkNpNPzPND9CJeUUZZXlgSOsha7i2N13iFfOy1qnp/ImCD4VHM5+sd8lj0ZZxu+/X+65viKXf/aKtFc35MUfn5Tbdu+TTLmIvBG+PZ3QmSSdUqROTWSw3+S/Ik765ddfUQ6w8khJlnEzySTHEdHZ44eV5I3EZexQHZoYl4Lwll9UovF9e/fqgfPYCJkclfzuUdlu1yUHn5mdLMhEf5f0kl05fPAg8uWc9De6Mj++KPthbqvtpnz+85+XznZdPvy+x+XyufNIB1vygfc9KgeQDtYrG8oHzYPOgg0vXAKpmmYrkXcF8cObNVbvjDkKNlzHeREre1CDWhXbizTR5ieNrL48qQWYInVdkzZ4SutHZ5nSFIKTbWxAq+S4ZynuQyqDJHmWkHf62rukmHM8HbhTP9Ul26pvQOJKnWTpgmPBD2XMUWXxhM4Xd6oNpUpMI6+eX16WkeIYFg4pUc3oHDFYIkkn66pt/Myhp57UZkIREei5F18St7IlOUTXBS8jGXz1fYpMl/R1OHd7ff66VGH6XETtna6hEcwRyUg6JaxDxmcRI5AiQuxMeVJO3HsPe7C4qTWZKo2qq6E8+0ZtUwHvswwScUhYUMr4Wbl3z73YC9YX2rI6vyJs691+333ywumX5dtf+4rcPVqWw4eOyG4sQo3jPiGZBXNKE8wRnXwyr+SkrBkI0j9fhxZwwoPkT8MosZHjRMaTD9+jWGF8lRF1fkgxuyqqZCXtdBjckF8PhCsdMwVhuO/irx2IDjuEbcYwHTW/ZNNBbKxMb76KO7pmapFITseOYCpdgxGlUM3gyNcxGvMYyqYnzGgmDwmzTeTuJ26/Q0F3bbzvLHK/BMuJSDvU/7gcCidLek8c7Vd3lfKAi0KIbRe3uY0Ah5MRhLESekoiUZKILy8vGSUzq00hVj49HIhiu7K6uiJTuMG3HToqnUZLYUhuwtNix+yeWYX/MIAifpvhamlsTGeMOQZDF8bXbtcasopULVcsyp4Dh+VPcWt/+uJJ+We/87tKCkOpAz+ZlQr5yPDzDzz4gA4TbCESN1YvUhfHqQkdKgg17vnPiHBf19YiqY6MXF3wEiczEDGPRANY7A48naYM4UDseKegM/PmaAC2GxYziOLnJnJz2ddNkj2VG+kaUhVO2+3ERXt2w39Bn9c+r04HuGZ6gHE2pw8K6ZwOWhN84NlJDM++Rd0Q3Dwj+RJpTftt5L7Md/fhpnKuqYxIOCiXNJXjgBtpiRlMZTgYhlucziUMey7l1B07FkrXwwOF1zx76lXCU3VozuVojg6BO8Y0IzhUMlS6IIpskcUHFkB8BIK1Vek2muLgYE0c2i9beP5/84lPyG2Ixj/5J38iV65el+9vnYH78+UQNvgyMoI7p6eVlI2TkySZYwTP6X/psnOWMTMoTrSFT/tSvGp+pxconXAQhW9jWV7ABv/mUITafee8Ndr5e2cYlO2QCuX/p3Ru1tF53yQbBfhzQswwOe+gF2sTqnCWOyDnMg0OSw3hRLeISOvUsqYEI9mimg2aynYvVFkCF7eXOrpkDiBChURp/AlOCNIKESUyOzUmrz//E6VZIJFp4JrBO07yJWznKkwGemsKqZzSKzBfVTRp34zB6PuA6U/h9eYuXpBsKivlypgEM9M4GG1FrhAaS+I3Fmg4AsoYwiFjEKyI04IlKfnqm58/9ZJ8/e+/LR9/6rfl0YcekKUL57Suf+jQMTl99pysMwtAIFtClC5WSoA98RKJZogwJfsQDi0BgHC3L2CZ3o4vqO+oTzQsOTjv38O2/qaRwXV1BlXnhFMJJb/s9Q3oXYMwpj+8OkrrMBDCNoFU6OptopAGbzE1fOh/le6BlBCObzWI/KHSsmK9zMwSN5b+2LVMrjKQu7DSaqEB6BfzZWkhgiTRC1t8HeSzFI4uI10jjpqKJe1aXSkSmQZwkGwsX5CtG2uyePmK7IU/5FhLr2dSwRQn9ch5gS9aNg6y9dsGQJcmnzYOkOHiMLPOXAuyr5OBkwezt16TrS5MZjGnMJxKpaa3LalWpy/5MTipDHLrvif1ELk7Arqv/M+vSHNzRb74yX8vSbiHhXNv4H0kaFplFxbxPUcPyvnaDcmPUt4voy6CbAeGFNZVLBt8jjLwsXSKJfseJxdJ3uamUuRV8eyQGSsr7gtY3mv42f1qFBFU+cpu01ethsGs8MA0W1EWq51r0NOOHgreIDLkEMOkN5emOnR1cz3rc90dOoZiN1UpISxdhFaRHN+IdsVTTJGZgmI1i+Y5lUhrU9xJ+WpFOPFQXd9QRAhvNjWEN9Yqir3Ok7YXH/qnr70mU1PTSreoN4KHKpEcfKYB1HvHiI6O1ib8W4xZT6PtWJqe6NEuTCkPPMHznFr0+obpHastURZ5aiHD8UE5+cOX5cVXfoqgzZff/9CvKayJTADJDG56CmvWbSNmqcve8REJxz1548yrOqYTa0GwoLHzF9NC30ldw4V4gVVDokgzXG/6QbPB/IHetSh0nsWH/JeGDtBQyXMxIzcyXTP2jO1YpPZSAwvj0YKGIWpJKFGIqN/l5iYpAeuYahU311f/a1hih79MQGUYBmIzbwUpXKsvHPv9yJj5EvwncVbMZTnakkvnpbqxpQSpo4URvLajTXeabg6STZbHtLpEbBhHUnsbVaWHwgP1sAW9SOJ5Sg0gd4xmRZa6kRlC1DUxgzIeOEPtZTLuhmy5cpNZgUw4hjcDm5FBgJQ/elgu37gkX/6Lv8DrBvKvPv4H8vqpn8mpk9hsmP6Zw/uVHpHRcQkmOJfJI+pGLADrVICbmJnaNZDqGQph7nRhzrNYm2vxn3qqFKOlosjK0ynlzd/h279A9OuE7DK5sX4Syxj9we3VCUHrJx2L9OEtdiPf5KvkSHMSZrJOg5M4IlYvrL7ZDV2r/WAi8chKSYvNdR3HHfhoXzHYrvX5xg2kkQbwJPdwI3DwpQWzzAobb7FOaOB1yV5DAB3BfFGrKxUEWdOjo4reIN5LD6WYPFgQBcfcnDcFmI5xEbyp7BSFCTMooAPdVBdz3IFiDbMEFwtLFZku08oiovmZCaVTfuZvvinP/vj78vjDD8rHPvZR2XjrnBwcGZVmIiOXVpbk5KtnJVUuw3ePyDjSodmCJ/vznlxcXDIVP2XUDYw78Qyl1VCokt3U4O90eB8HgqKcbVI16TwSqXpj/WCa6TD8TtAPn1aojp3wV/B6OFxwbgojYXJFsbzHCJiI/njclJFsFrllggkNaYW4paxWWV/rBfwe6sHwrEoK81CCHbSD43lD0SnLuueFZkCONEk0tW3KzyaM8mZjq4oYIdS8mNpKTaQodfhjguImyE4DU37+pZcl2Q6lmEvJAm4vUxMqqhEJGiKP3N7cVl3fXK4gfbcjqbyPFGZJ07w2mwJIt8ig18NnVcJkRttMy0i7xPnjPLnAehKyYeIklJF2E3HMysaSfP2//Qgmui2f/Od/gODpsLQvXZLXfvgjmc6PyL5imYx0UoEZ30BgVhspSoConPyf+TtPyNbaDYUheTlPupuIKZKk3MWxpESullJJYZX5TiLIvCAR1gV71cC/17yU+DbONSdU4ZgaUX4DW/20yYNNVUfZ7lxzg/QmaUjuK0ejaIXJN7mpHUPll9nclAEDeQmTy4rY22soIVxb5TJT8L6pS++YmgnDaBBV00dRsJGxAM0uwYDUPNrarGqVjczzATaWRKe0F5tb2wrZ4SHdXFuX1flFKeIzMrCSTiBu2jQXdFxWjGxey5p7pSe2uo5UG9f30qEvVdLWuCOhgSg9MymQmcOz57u8cF38MhkFanKhckXO1Zbl8V9+v/zarz+iqI4+Ds3K5Ut60wkWLOH1Enju2eyoYsWwnVLFa+bHRuXoQw/J21fPSiKXlWZE5GdaLWASlrLB+IRIHC0qed/I4vNv13uyCku23mvIjUaVRSWlM9PmNiNWo38bPotVfA6J/geHDsh2hCLXyv9aJh4GSp6RgdHChb2Rri1kcBP5nUGWY8pkGh3zrHjhDtyX66rP1gKwhefe7GNcZaBRqgncGJKjUASa6EaaoeZWTevRfVUC7yvpColOifyYmpyUxcUFhePyhrLKFZTySiPoWvFBxo+HDpgxFPrt0OrUk6GnW29r04IFoW7HMxXVfjBQ8A6xBiNwAZxNzowgzz06LS/fuCCvV96Sgl+QP/zI78v0xKhsX7mGQ9TCbaMZj7QMSkxXMl9Cvn5VpnBLc+SfzkQy164iPSvI8vqafjnJnLx2ZQnBWIi82rcqaJ6hv3Dlua7bfrZbm9NZ6M3qtjLpc8zaNOZuAtiRdTFseGH4VdzlDw7zW7OhQ71gxwREVoLTEIAx17X0DnrOHKUdZKpkct+hiXdFhpNsrjsQfDSqpkyP7GN3bDAnLTrYFLYR9+87oMHYljLdUfaupbPFKXyiGgsOSrkoiJanpIqFX11akalino0WZbqJ/Xws/sxfjLTJyKfK4wlyQXb0ucmqN5LJGVL0QPXftL/NQkaH0CLcDnZ8Rib2yFrYkR+/dUpwR+Xw7gNy521HpFetyI2NFaUPTuJn6lT/Tqc0IEtnM0pZyPfkIV0bC0tyKJ1XbugmXvdv/+bbWjItFEvy4pnzbIzpxeAYbmlsXDtZbU++2nWjhtGBtjEKsV9MSb3IRkyEvkTmdinTrOM8g837bhSFT5mih8VjOIbtPYjlYQi+FsMRldJKlfHBrBUz/2NZkg0C15plRVYw8mUgoqLTZpeVbxKmSqn3I2MZVJ6mZ1jce5aNnSaXRfxcLmeIyony7JlodmVt1fhvYqoKBT2ATZjyty9eVCm8LLFaWMQlmFDN+/G85KzWPBlvZbNa02FsSt5qNcw1uGwGNdssZ5IQPJPCe+rA/3eUVaeLt9+ByV7sEyBYkddX35BpKcknPvqHONhdWbkxJ+21TZ3kzyrXWFdcfBZSJY2XynLp0hW55933q6ndqK5JGWnT3lRS8n5ezne3pEkGerzeWLkkfQRe9VaoFIs9ysOzzp/NfLeXdJ+hJSm4KS2+sMRKqgjSNnkffs/DOurhyIDkzlpIl/g00r/9lhMNr7kZ8LaUh2JyVppnFjIozZbQPNeUKBPKkpcwJCQ6rGYKHIxeJycnZGJ62gADSOKZTBnaYjwxuS5Y0GeFkdSBBN4R7zw9My1Hjh414LYeR1e6OkHQsRPz/EA8BKzN8gBSzpVcGArqo6bxdk0JTmvNmqqlZYmTxgZyyk8H1fG+GrWaak8ovwcbJ92eEsvomjBqVqEStmiwBsi93VxaVvF8r9XOyXxjQaZkXO46dFxWrs1J5fqitNar2NikjCNta8Ns0pLR1/O5SZOojPOFsszO7paFG9dw4HqS7cASIvahYPbi+rJcWbwqB44el2qzp4iTDE0zaZXz2W4q4X8SfvYMseGssNGK8eKoODvbhq5VkA1tI8GzlCjWan8X+/lVvKuPO7bYYCYgoh0FyVjS3THRseMMOC61yRDzBXAqDh+INIG58aLsO7BP9hw4pOgNDq0R0F2tNXHYPEt86iljHW9NikEPMVh448pTQVA7aRg2DO1wPluE6UurT1ML4xo0aKPfVk7ZFEwzA0RW47r9jkbRVFVjQWKjUiGaUDYrm4JoSqaLozI+WpZ1mHRG65yWZ1mwE+CQFHLKjUn/7sMEduGPVuubcj3Y0LW5V/bL8d37pEVcFw5Ln9aoT3LUvlwnQ/2Uh/eStDrALcQUGVlbWpXFy3Ny7L53yRO/8pi88dqbsra4ohCi4uSIVC7MyYGpGXnX8eOSz5U1uOo3WtJAmoeD91VsyHe3EH8sN+vSQ0TOcFDdSM8IZTpf+9f/1pxINzRm16prcmY3MHoO94ZR+HX861GjSGpunErH2GoP+ZwTMMn5CHlvaEwsy3ksxtNUK2NeRB6tQGu9Y5PjWjliZYjzO5wsSCGA4hwOzTfbXLxhKleHG0Tzw9kj2THOyscksPHG75hcPRag6vWMfFw+WzBTkQwdYOrz1FZareiN3Fxdx/eu8pCQY4qCHNWVNa08EfpLNrm5+cuSkbSCA6uCHBvfYfhggsuqF3UJN6uj6t2+FBFBH5QslcgkM1pQpnwNENl9asLSdLdl/PCIFMZhSXDj+dokYSOf1dY6/m1qXI6/+3aG47J4Zl5eu3xF1gpJeeHaafmNJz4kTz71G1oWXUNOzLGUfqN+YXlp+aP5Qu5V1rZruBCXGk2pwmI2o1AnTyIDiLFysoEpVDi2/si2od5DR17FJn4JD/tTOqYwtAUIxxwG3/ZuGfiEncg28ofaA3HZkbebFMJZTr/j9B08clQypaJGektrWFj4w3YVN3TzGvLYpubVZLNThTWtvYYyAjMd/+JUUR7/TnC5Dp6xJadjrEmlG2qxVRduq+mmQBcp/FcQ3HCCsLpeQaBjwPX9jqEbpLIZrQZHT9q4B66tpC0JImP8xwHtFKLaO3GT9pUn5K2XT1OhSMawpUn4vgRTN0TICMmktdbWtiCnIWh9srQyNNHIW50N+Dt24ekrCTSEO6OK2tLyhmz/5FWtEpIXs+c7cm55TsbykyJz63Lxr38g49PjUt1Y02mJbCbxpVwhic2NtDOVQZRdxoGeQ4ZxcumGZHfNSIuFDpYPWaNyItMZYj7s2cBnWBYTbHDiQbyl33YcI3qsUqphdNOMqSqQhabiwiDJ2cnfYdt9zGHZnmRRYWxiF/xGoCznVPXq1JC/scZc2KN9VOocaN8Tz0eBDhV23lGZo7wto0kegBRuPPNQ0uvzUKlIVd8Qdi8tLcpEeRTvLVRuaoqE5BNphRqFUVpauL2jhaKMTkxJi8JbjAnwc1vVDWWJJbKDc01H7r9Ljh07Kn//tW+omNeoFHXy0euG4tn8nuvHmKBbs9oMrD25Rt+YN5bfmXpR4Iu5c2TV1rmUV+aX1FUSSFFxujDxjoyOjsrS4rK0Vreke6Yvhw4fkGN7Zr+dy6a/xOE4B1HyBgnJFd+WUkqm0J2R0wtLekx9NqjDGJAeDf0qF1VB2Vry4xxF9Hnc1LtwF48RRmM6SMFNJT3DqOraYMqSp7nWZDsGk0Vq3C42lfS8pc0NLScSW60pMC59upST3EhJweFuJiF7JstKI1ScKGmD31TcbJEhNOJbNdw6tvpcx9D+swDCKDvBVASLuQvBShEHgEKYDoMy3G5SGhOgR0IW1q93z85qQYRjM9RkZCDYrzYkS+BYBxuYimDyc7K6ti4X5s/LwfSkFDhFSKgxAqOEzfkiW3T9xQFcV+grMzlW9IyyzMr6KlKwDl6zrRQRJFHbxuFbqmO5symVkR1BXtzCZs+RsWhkRObr1fOvnK19/sTYRG0Wn3cKh4GdqwJ8OznA0jjADyBo84tNeRXr5iv6ghonJJrc0ctXlQ8Wzy2tA1bvJDbtC7gBX2S061q9w3BHsVsVyEJPy4ysbJFm6ebP6CirIWE9VSTjLbK74k1TubuNAKuyWdFGe508E3ggFVH4K09aQX2BOLgzr0nfb5obfQUPMPIulThTm9Elph+k6jij5S2YS6fZVl9LmT4Or3eaDZ0c3EKePIab0q3DXHNakUV6ghG6fZnePaWpWBuvzQY9535wbzUd1MKOa1S7SV5u0hOrhWDXMAxvvgT8fMVyWrFmXFdu8Ha1LdtY523e7m4dadUiJ/KlgJhmxElKrpyXlU5TanBjnV7vC91WcPJSvSvJ8qTW4Ru9phSwlmNFTmUg8WkEcmJ8Qkd0vA8/+piRRw1jEUpLwsKyWBzAWJOMCPp0EAVlHJIHmcoolZJjKlVxmsSGunaLcGh4C9yYX5qUgjBLRm3cqFqTE4ujo/T7pMcl9wen+Nk6c2jG1+H/GJThhkd2RkqRlqRjotQqBSRhnik6Sc2FwLb0Utm0NiEIoVHdo2xG0wc6o6jH3Lqr5p/9bI7E0CWx28S+tT6OsgGU48Hb5sbzQPi+p/6+vlaRBgK1nJvQSFUx405kYE4xNaOt1ukFIDN7a1v7wvxF/NjIaF4PrXZ7/KSsV6oa/W7jFl6prEkGF+Xdh2+XKqLpcViNMt7fNEWve+0v7Er4f3wglZURmPs6XnOZIpvpESmOT+HwpPRzM65hb5iiHL6ZPXKsdF18NwxsVDsmlk+Z6UIQ9LHOwef6QbQXp+9px5Yk4693pDSM3GEHyrU0ibYltwETnb5+XWQmUjB5Mu3vGH8TFW2e2T0jnXbLinBlTbuO+TE3Bx++gdySt1f7rwTO9yMNrPhBueBkue3bihUBAPxh5eX0fUV7kBicwpcMEns+g7SepoKq+hJ0VUbets4QSMFq2Mh0+KmM9J+um3V1saaFZeQ0teMElVMjLbHSOrXaJjvYQMBJPpEOIuHTb5+VOizpew+dkONUX7m+ogz1DIcrKxvPTJeyn+v2EUFwVhobNAdLt0n+zGYo6/jzLhyQSdzibAHxiNOUJC6L9xRucGCnF3SewTWVLF2QQJdG/SNvuN7mMNrGjbmKD30nbuEsfWASpzBlkRsJ28h3xLUD5GbuWFGRrsFUeRZERxJP5pW08uNjYzrhwPIfZd4o5UqgOtOlFm4cCwzUJ9iCaedzjeLx9MGErDhWWJppE30vf54qnvziz7MoEsHH9ZHnspAwaHjgN2RlT6n2YcIoh9J8u47SUxCnXaBmBNnfqXWIKJ658SaifoOzMm1RbXJGcRdZbt5cgh3wWag5zHw6l08pMJ0zU2SMb9Ft4Mis4MbN9Wvy9IeflnSlIVPpooL8ObqbTmV+NjIy8e8iN3duG2t8CbHD2426rPCm4uBU4EqqsJYXV1alTiNMTQwc/lSPvCqx8JWYcmUoQz88bLJHSm8fRaEtdkSnYIY/i+//CbfhyP8PF6X2jRyDUNRKdGiWgrioJaQoXMi9B/ZaBXIzzcgJwYWVJQXXkVmGhYrRiTGNlG8gQCGtIGeAR5E+qPms1sXD6c3iwFEunowAHCONWpHeRE7oa+HCM5wFrVpLFz2dzxl2dgLhcasUhI/DQwCer+MznjZcWb6ktqIhookGwIQY1aGTjqaWP9jipCqjesp7TUQaD1zQr6soWDYb4PWzUov68kb1ovz6kx+RR+9/WJ4/fVFHVYnDdlKpi6Xy2GeXNqunfn7+omxTQRUpVC2dVTI2EraQ8Xcdh5AD7KevLcr5hWtybHJS7hibFu/J9z5mChxadDdVLM7D9lSlOpZ9d2wBxPBjqSik41zANlWxYQ8hPSn4muf6Gk1q/ox0iWRcWosmuA3BQspJq06R4rn0OUMryY68FhHfdqMms/v34qb2NR8lDIWmtBeEthEhGkhxHngTaUppegJfk5IaKUgPq0x6Qbb2eNNJiDKBQINFD60cERjeN/1n16Z225wxGilrJE6cE/2+a+WBOMBe4KgMAW1hf8gKj79nQOgOSrXOoLJHS8LcnWIfHGXTBgy7aFRTy+d1Tmt1dV06deT89b5sInq+srwur61cY71N0ridJ597Xq7jz0ZWV5bGp3d9eq3X/daZ1RtytdeQCja3q8gSRzUdddqDU4meASjSzZF0fR0B4YW1ZfF+9bHHNNkxcCvHDplFaq7iDTaYKDEClZZs1JQn3TeQZzURZD2EqDnL/q+2wmiitVzpWRSk/XPoqw82gHjTVtS6tmO4o1kDXl5Zlj1792qKQ00Fzv7QPDeZN7IGzSnFlC/l8XGZnp2hTJBRRoVZJn+lahrXqhqtsomg4ps9A511LTJSwXyh4eDkImkVDI/RG0nUJNMlKqEyRQqNKDZXmzlvE3/ftVI+jmXHdy3YkJ2lPTig5JBkj7obdiTJrCISjR/WtyqquZjBQW+0u7Le6UgdJ+pya0UOTx+UaL0m5VRa9SGihFNpB73PeKX8X9bSnpy8fEnqWJM2AQ6uM5ixHiDhHNu3cY0scMBaN9bEj+Ep4Y5GwxALbSXdIyNOSd9MohEVKddREt5M9891sshxPwPLOrYz77v5+w4OaTsR42GjHJZFA0drxnwsuT5OyUvy0MO/bGICnNb8RFmyUUkXNIkNo+Ywfe02/BCDI5ps5q9Ju6G0GErF2+tbEJ9jNIazWexfpOYP8RN8fN6gLXFbXJh1l/NPbk/bkmzkx2LTJDpxLcaJNL+uZT5QxKeFFWnsiNdfrlflvofuUyuzfO2aThMSvZLAZ6YUnw4SkJ+k1ZTNvCOXNhbk9uIhOT46I7XGkuzZNSujR3dVal7wmX7S+fOTF96WM1fWZQtP4Hj/OD+3SVd9O7TQMzc4kps5n5Vhh/XpvvkQ0aDRYNGPdpMoN6N0gZ5/Kum4NZi/e2GWCnGkrK0G5Z822GTDlOeYiUUekKRvBsEd097g0DUhqlvb2zJ/Y0EOHTsiaZjdbtTX0VIGOwlLR1zD5nIzWbRn75ebyh4r1UcLeRNkMOjhRnFBmdKwlsvqlsrp9vo6+aeKaewdB0aPUbtH8MWb1EnyLDFrrCKOf2dvWVnpNb+1Wbm1QDSbXj4t4WhW9hw5KLXVTXGQp/LTU+eJDQqy/vRUJb2HKHgd77Uk733XA9LfbKg4ZqfVXOpG3c9cnZ/7M8rXVRHoXt7elD7hsbSMzrB3bq2oyU4cQ15382ZjjT/w3kctntlsXGTLZtx0Ju1BZCXeeWJtxqDFBfjWFE49816iC5AHn0aOCRfhnUAOPMZNVWgsTbYlHDdoSk+PE/1SaAWfjA6QIQHXWjduLTWL5xbmZWpmWnIIwJS4hcFKMql6hEydRkfH1HdS3FmBAr75sEpuSrIY2/LjIrCz0sJz5BR5UdG6NrUPGXzppCPeD1twUaevGkaRhQpx/plROGMSBlmccGgHTXN4By1Uk9gxx1+ob8nZ9hqi20j2jk9rdkGqJh48poU96kslQ1lrbskm/jsxfUQ8JK4XKKztOhe7vfZnamtrf0lVuG34a2LL2Kui343ii2L762bEx4lLF1aRLtKJDqaVXFfv/Y8+YnJga5wjTYVM8KOD4JEZIaW5DBwZnB0dRbFBlVZ1IkJZwteR5My7jr8fLz1LOgLTLzZ4LNfqLcXdH2WRiYavGft6Au946LYRSF1fXMBGlmXX5JT6VEJiOQ7CzebHLI6P6mHgLfN8U6hQvQIOcxMv5Zp0jbc8oMlltajXVakcVQbtB5aHE6cdFov1cNIhBn3Dhs5/4/NzWpDSt5yakNC8Rmz+xd5gqr1QV4KzyfNzc9JfqyoPCDtbvcAEe7zl630ES+G2zPpTMpYtyNrKGk3+z/A8n8ZDvkkIEvWT+Xz4aZnvtmQjpJVKiRWGvYkNOD5ovDCx1nPcR/Aef+RRiQZjwMNNNd8N4t9UWIMY6aHpgK+b7FvRCV//7OOKI588j8DrLDZ1zPMTx30nBtvFWksy2ODI0gwz6OrbPFx7uRZ8ziEx5q/zV6/qVOLszG4tI/K9ctqPEuxsNzbZTiTNEdl7iNSkHB1pGJjy7NjgJG7B9taWLscUDkzcLCH8hmZ8y8rUbJMCCj/D1iaRoJSPzVikhxmptdwlMTDQjWwLNZCMl5RJPy1TTkYyoREtUUAfmYqYEeCxi711/fEj47OyDbPsp/xnkHd/CuvzIw1vYEoJue0lHFnut+U6Pl3LNxdFtZhsgGiYF+z3eO/Cm+fKvMcfftgutkIArH8MzDRDFNjPEAwk3xVnpUhiT0lNiHIm/YEXGcU0mm7XSyzgUScRhfJePahmJLLdpR39W/PGTI4d2DK9mVw0p5NBAnPRHmE3584pAenevfsU0ZFGbkyx6e1WQwlCyUXp2g6Xmi/fN2qkjpGzYZRMzSPmoSwb8s8RtXtJUI5UZXtpDbdtWxltWXbl2IpqI+ayunlEcfYaHECz3bbQ0lpYpU5+xlK5JJNwG6TzzXvGfXH2iYeE8QY3bKvXltVwU0b9sqQ4NJdMfAF++T8gnfw5L4vW0Jk1uH1ZibqyhpfaoDw9NSpoVcIYcWPAF5EFYURWMuFWEvfhBuuCdwxtgjHIhmnHMb7IYC/FbCxubJYsrbglGXyIjNagk5rzJjwDH026yW1E2c/h9lZglg9iJcY9e4PjOEABBXzFyPZgnDiatyZFy6R99a3ZfEYuX7uq1avDx4+q72at3EslzZCcheFqVMxncw3+mlqFHWwsGxmlmUnJjhY08i4hmKstrsrizy/K+gVEu2u42a2+wl54QLox0IAmnOrm8L0uSWU818YqZmJRAel0GcmEHgq9yTTJsApsdfJ52JXrso2Y9+VGrYL1ysie3bPnfc/5Izz+j8KgVw3ZcrQyRiSSbedScjXCYcBn7/scP03Y2a2bo2cz/mMLpwPc3DDc9j74+OPmDRL66pqkWXMrw5Ri6Y7Mk/H2Jt2EbioBZ2nq5OF7ykHOKhaLpfO9CdtpYYDlnsJPvoXIOoc3eGJnt8qeP83kQjt6FMOCQouIcGKtW/yeYtDz83PIlVdldGxcRSEJx1UtXzyBb6NJrbcxyMKN21qrSBJPMUMfjiibcBcPG1m5fF0ql+aUUslrdCWFk0XKB22DssCCQ0C3wQDSsUFa0OuoOe8Fxtx2yEBPNdBSSYqlEc2XKfXOWSRucmhHTHhoOzC527hAW5267J7e/W0niD7VatS/xc/IkRcC+elqmP6N75mS8YN7ZKHTkCXk9m66oEwICtFwbq4P3hQ5OzsHBczvvSfe9z4zpUAcs2daftr281xbXjTIBgYhpFpIUfxRb6ynt5gUROShUhoGjZSTWsTXQVHCZXnvI2cOz/8cNnkLT7cPrzs2IBizmsWRnSSMLANPNPAroS3/mVYctXaXlpZgss/L+MiYjOQLMJ24Wa6VA/JM2524JMJvpkZGZRzmjX1bMsJXL87L8pkLsvDm2+KwPVhraJ+YgRtHO+mMiOJkYUXfg2uyCQZmCVI9kOcUkXUql4ZryCnNL6HcVONmUMThbsYOnBemVlPoK3OJMEpYr21eQOD5H/funv1Ut9G4wLSNhYjQpm0JPyOJbFq8HPJ5XjiKXTc70sLjCAtyBrVG5yaa53jEZ+cGx2mT98Tjj2klRjlQmMf5Mb2Cb8twhjBF/atupKebm4ySNMMaYPG7aTKYwTLPMQ3/Yc1ZR0w7eP2fwly/YlHtd4Qm7rc4ayVsslUuU0UjmQtdRWA3m418mstiqaxsdOTCoFYfbwC/Ij0Avs4xJbG5jZWKuPWONBaWZeGt87J27rLU5pfkxsUr4uDWZllVQ1Ssn5/N9lZdN9PxSOXkSLaUU6tB1IibZNsRt7Wcx+vnsRFJ+Eq2lhz9rrwkgVFHVVVwjuLg76jJ2I563U7U//JGd/PTeyf3fjOfSnfIgcnRVkcZMFLaw84VkSvjeTk5wclOAgxoT5eadYngApx31Kga3uIYZjCI7qluY5Q9+pZ4NBoM+RrGI9dUeIgdChNao6VZZkU57eKUEvsc+vr35iCoGLzBQFsMbmQPSWQ1lQJXXsJfvoQn/SH+/Hv416c0eaduBPNR3ljFSfdtqB/pdKFGoixTOqbJz3+/9977lAWH/qfVaCseK0IQw2kFBkvO4gYiX5i5s+cVnUFYEvu/U6mcqohvwVybpnxPS6K8fQXO8+L20Tyz58w6I10YuTm8ZGRMMDaUtzWfH9G5pqDd1EYFJyV4AbbqVTxHVkLOD+Wy300Hyb+6eP6Nb82OzEgE61DdqCB4RCA3NjFoVKgYJlxkucTZYlNBC5CTT2GT8/Dlm1RH971bKNoHIwlWCsnGHjaVJdrZd71w0OQfdjlNHJvw3R1TDaL0f4yYeWs5ccQkXge7icMKXasY7lqT4VqeDvPUgROfLEOGBp/5Lcd3n8Vn+l2Y/4/AV33QvGw4GPq+9ReLLTx/xGtxqOzIbcelVBozdTi+EPJc0hCSPqFe2ZSN1RVJwt86KusaSX0DGx4aZgK+FxY72IBngMSmfiaX1mZFixSJjsFk0x87GqNQb9iHKQ0VclTvt7Dg8Ok8AGy0M1jHgWj0WlIkMXgUPod8/lvdMPrG5RvXmx4vBElaxVg5+vbYMsa/YmbADgI6bhbTogLM/UShoKVYQozDW8qTsXlWOPOOkdLBhL/n2KjWc3YE2Oak+O6QLJQD3X7XdIyYk2oOHHoK0NZQSn1tnCK6A5naMJJBj/QXcUrSxNf/YHKPr3+CAObpIAg+FPQDJx50volWib4ZH4I6gSfuvlty4yM6QchWp69FlBAbDzOam5AETGlmekQnGXyY8+q1G1K/cl0yfRnQNTHlidONft+RKkw0O2kMsIjf7kZm8sLDTaRvp5kmRxdBe6QiJmd2oBDVvvrqRCYR+YH3nVQm9QwCor+uNrYbC6urMl9bkNtnjppsQ0wgqs/nejeVFiNYkMrGmoyNFZDSbWlvPAeTPYFbvLC+IW7GUbZcLR65zmA4L6Z6duzn0osQbzCj3b7OnLq2oiUWvC4auOjUkc69mtOQpkAjT2DoWtoeV2d1b6HSGAxFx9Pv/095SieE/Qy/grz7K3AVTyKCfgKBxxN46f1x3Yb5MVt/Dm4SccCjU5MaOJBTst01dWaNcqlAlqImsaMkZQIfm8K/0+fthrUZTWSk3W5jIdfxdzWDn2J+iQNb22jAz+Ln0660g476YoUq2ZllpW/wzLA7X09BiZR3DULkWcH34SK+DxfwPWpFUO2FVmZjc1125abk7uMnpIIDRsvnJZJWZtffMYMcKtEqrQf1hDO5kroIX6HGnuoFU7+JA4CeZ7OFMLwF9GjLtDsQsb5pwIcKGg93RNtscbFt5oUmACFchSMYaQ2ofC1Npiz2SqcJdV7JGfA6RPFEYmhDqtDye9iSZFwC1fIaaZy6BtgeBeH3EGh9D+/lCB7zAQQb78cj34enHYE/UD4ulWdPJpXLy9c55JSkEKj0/KQZHJdQaYXaeM4M/TX8c3VlXfrNACa0qtwYHPYiapNWKKh1tdZNmVaOo/jEP8E8s7mhFE/JGJaUNA0Mlh3DHhlF/yEZ+c87bvSD0HEvciJfMdywcCnc9LNXryA1qso9k8cl3QlkamJa6ttVnan22csNukNuE3srGMc0qBCrjAqGU2yCrmRlWdqKinEGQRTzeUdubi5EvAA6mE7GgoAHxHmHm2euHAmzyUsWQ1M4tsjKFXWTtBIcGN4OL6YIjmJj7AwOSiTDW9wflCgjC767ORW6Zar+IuzKRTz+i/j9nWHkPIKA7T04Tb/Uj4KDbDJwAVjv5uHstHs6eT+U1wyVwiEFP9ZlGtUNVVCDhXiy5LF6xhuStH3uSDkxE8pzpeVSvMd8saDjqqwlk8/Z7YVXcABfwt+cxGf6CT7TWxwp7bRJQdzTdVAgIg4J0SaL6/Ny1+TtcmD3HtVVIiMBIcLc5F68ybdQVXGDiZHr9VgxM5MRfS3qEI+e/L+2CGOmBQ7pKzeIpY70Y84LA0w3Zb544t+0A+0GOwaS4lporbeDTjpWJP2FSpk10ZHFK2m1LK4/BxYHFveab6FNvMXgv4WfeAuH4s8QlIzhee5rNJvv6ve7d8F/3uH7yduRR3ocOQ1cW2HCF/kGyFVZubYofscw8bEFmggNn2O33UCkm1AEBHWXEhScYmCowiNO0Gx3z4ZhcCYI+m/iPb62ubFxOpFIVkiTyElIV3mrXSWPCfye1onpW3EB5dyly1J2RlQtlQcwWygqrxWLt9zkWq0qv6BexGlArLkeKs4d833xfXKAzuDThzSTjqkhOEOGdQNkoDcJXG1hsgLo38xBNdwwz2KM4o1NWHVKJzTiWK7Yxwz17wZ+d7C/lv7BsRoQcXSnGvcckOLm0jXowvdV+vUdNncwA0V3iVNfwQI81+l0niPTra8wXe9Q2vMPY0kOwOjshb/e7UfhdL/RmajO3yhvLiwXk81OFuY2ydfEYnVxsJqdINhGDLGJS7OGNHDZTyYXcXPncQuv4o1ewnu9TKZbpTWC2S0W8+pO2FEiTQQBAdq5wiFJIc1S7WMszsLGinJS/tId92rTY21tTQ4eOKDBHMEJ3GSqkG7jJu9cKy2S4jF0fIGBu2rVjIgVdrSif0SoOXwH1e3/I8AAWmgEAugrIsAAAAAASUVORK5CYII=',
            timeImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkEwQzU1QjU2NUYyMTExRThBNTg2QjQ3NDlCMDk1MTAwIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkEwQzU1QjU3NUYyMTExRThBNTg2QjQ3NDlCMDk1MTAwIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTBDNTVCNTQ1RjIxMTFFOEE1ODZCNDc0OUIwOTUxMDAiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QTBDNTVCNTU1RjIxMTFFOEE1ODZCNDc0OUIwOTUxMDAiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6uSgjUAAADQUlEQVR42uyb2WsUQRDGK5tISETRBw2IGhBBjIoIEUUj+GA8ow9q8EEUFI2CikoE/QdMPPBJxAvFIwQVzweDIh4oXk/eomIETxYjBNGQmFXiV04NdJqdZHZZZ3d2q+AHtXP1fN3VNTPdvXnU8JWSsDJwXvxF4AUFY2PBafGXgOeJXiCSZMGbwShhAwVnG6Wyy8SnoAQXG36/AAUXe/i+rSDOtkFgOhjusd8NadPfGpBgv+X+Bh/ATfDN3JFn9OH+oB6sBn0oOywGDoNt4IcpmFv1OhhH2WlPQCVoifwTTXQmi8WyjQcn3T48X/qsa83SN+6CXx4XOAQWi38W1AR0437LLQRTwS4wQrbNAvNY8ArjwBZQAaK9FNxp+a0BCU6k3HPgPngsXZathkN6slWDUR8FvzH8VwGG5lsP38u+SNJyrZxbeLCx4ZHPgndKBuwCewIUXC8t2wH2+TznpeGXFFgvHx0+L8LH1aUh+XC52xM8p8vw8yOUY6aCVbAKDrfZX0MVyX52ZbBNsr+WujSkVbAKVsEqWAWrYBWsglWwCk6p4EZyxqxdMt3Me23UFtbv4dTbR3IGwj9LmE0A09Jdyf9D8FVyhnDvUPchUhLRJ8iZyQ99SLeB5WA2uB1HLBsP9E+R/aEW/B3MIJmh68V4nnYueBhWwX/AQvDA6iorwTVyZuKfgplWNMwBz3xcfz8oBcukrLQLviG4NgzcA0ek1fk3zz1fBkuN41qlpXuaFDsK1kmlNYBbmSB4KMg3fE5WEz0S5HGwwNj2SSrldZywXwtWGbmAM31JJmTp0eCiJKI1En5exhVzClQZUfEelIP15Kwz4TVfl8BP69y6VGT3VD2WqgQ/VgQuSJ92ExeL29FDFPJMfm2Y37S4Ja9Q96UW8axUjqtNVcHpfOsZIFl8t9UNCqUiDpKzuqAyLK+WfsvfIvCjqh0MNJJgqN6lE7W+gn4tBdXCvNayOkRaqq179xgl0NlDFayCVXDIBLfnkN42FtycQ4LfseCmHBLcxM/hITLqUJTt4QxGcgvzmuJNOdC6PMAQdbM0LwznIZXOLBTKmvivAsfsxxJ/f/KfIfZKIouFWGRMNBwAY8hYFf9XgAEAO22f7WMXmUIAAAAASUVORK5CYII=',
            RecordList: [0, 1, 2, 3, 4, 5, 6]
        };
    },

    methods: {
        back: function back() {
            navigator.pop({
                animated: "true"
            }, function (event) {
                // modal.toast({ message: 'callback: ' + event })
            });
        }
    }
};

/***/ }),

/***/ 322:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroller', {
    staticClass: ["attendanceRecord"],
    attrs: {
      "showScrollbar": "false"
    }
  }, [_c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env.deviceModel === 'iPhone10,3' ? '116px' : _vm.Env.platform === 'iOS' ? '68px' : '28px'
    }
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v("考勤记录")]), _c('image', {
    staticClass: ["more"],
    attrs: {
      "src": _vm.more
    },
    on: {
      "click": _vm.back
    }
  })]), _c('div', {
    staticClass: ["userMessage"]
  }, [_c('image', {
    staticClass: ["userImg"],
    attrs: {
      "src": _vm.userImg
    }
  }), _vm._m(0)]), _c('div', {
    staticClass: ["attendanceRegion"]
  }, [_c('image', {
    staticClass: ["timeImg"],
    attrs: {
      "src": _vm.timeImg
    }
  }), _c('text', {
    staticClass: ["countTimeTxt"]
  }, [_vm._v("统计时间：")]), _c('text', {
    staticClass: ["countTimeTxt"]
  }, [_vm._v("2018/05/12")]), _c('text', {
    staticClass: ["countTimeTxt"]
  }, [_vm._v(" — ")]), _c('text', {
    staticClass: ["countTimeTxt"]
  }, [_vm._v("2018/05/18")])]), _c('div', {
    staticClass: ["attendanceRecordList"]
  }, _vm._l((_vm.RecordList), function(item, index) {
    return _c('div', {
      staticClass: ["attendanceRecordListItem"]
    }, [_vm._m(1, true), _c('div', {
      staticClass: ["goWorkSignIn"]
    }, [_c('div', {
      staticClass: ["goWorkSignInLeft"]
    }, [_c('div', {
      staticClass: ["greenDot"]
    }), _c('text', {
      staticClass: ["signInTex", "styleRight"]
    }, [_vm._v("上班签到")]), (index !== 0) ? _c('text', {
      staticClass: ["signInTex"]
    }, [_vm._v("18:15:30")]) : _vm._e(), (index === 0) ? _c('text', {
      staticClass: ["notSignInTxt"]
    }, [_vm._v("未签到")]) : _vm._e()]), (index !== 0) ? _c('text', {
      staticClass: ["signInTex"]
    }, [_vm._v("天瑞国际商务写字楼")]) : _vm._e()]), _c('div', {
      staticClass: ["bottomSign"]
    }, [_c('div', {
      staticClass: ["bottomSignLeft"]
    }, [_c('div', {
      staticClass: ["greenDot"]
    }), _c('text', {
      staticClass: ["signInTex", "styleRight"]
    }, [_vm._v("下班签退")]), (index !== 1) ? _c('text', {
      staticClass: ["signInTex"]
    }, [_vm._v("18:15:30")]) : _vm._e(), (index === 1) ? _c('text', {
      staticClass: ["notSignInTxt"]
    }, [_vm._v("未签退")]) : _vm._e()]), (index !== 1) ? _c('text', {
      staticClass: ["signInTex"]
    }, [_vm._v("天瑞国际商务写字楼")]) : _vm._e()])])
  }))])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["userName"]
  }, [_vm._v("茉莉")]), _c('text', {
    staticClass: ["userFrom"]
  }, [_vm._v("外包开发人员")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["timeHead"]
  }, [_c('text', {
    staticClass: ["countTimeTxt"]
  }, [_vm._v("星期五   ")]), _c('text', {
    staticClass: ["countTimeTxt"]
  }, [_vm._v("2018/05/18")])])
}]}
module.exports.render._withStripped = true

/***/ })

/******/ });