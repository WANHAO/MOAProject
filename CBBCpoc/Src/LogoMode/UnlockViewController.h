//
//  UnlockViewController.h
//  unlockText
//
//  Created by 尹星 on 2017/10/26.
//  Copyright © 2017年 尹星. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WeexSDK/WeexSDK.h>
typedef void (^LoginCallback)(NSDictionary *dic);

@interface UnlockViewController : UIViewController
@property(nonatomic,copy)LoginCallback logincallback;
@end
