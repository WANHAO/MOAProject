//
//  WXCustomEventModule.m
//  CBBCpoc
//
//  Created by 方克东的mac on 2018/4/19.
//  Copyright © 2018年 Alibaba Cloud. All rights reserved.
//

#import "WXCustomEventModule.h"
#import <WeexSDK/WeexSDK.h>
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "TouchIDViewController.h"
#import "UnlockViewController.h"

#define KEY_WINDOW  [[UIApplication sharedApplication]keyWindow]
#define TOP_VIEW  [[UIApplication sharedApplication]keyWindow].rootViewController.view
#define TOP_VC  [[UIApplication sharedApplication]keyWindow].rootViewController
@implementation WXCustomEventModule

WX_EXPORT_METHOD(@selector(showParams:))

WX_EXPORT_METHOD(@selector(showParams2:callback:))
WX_EXPORT_METHOD(@selector(verifyFingerprint:callback:))
WX_EXPORT_METHOD(@selector(verifyGes:callback:))

-(void)showParams:(NSString *)inputParam
{
    if(!inputParam){
        return;
    }
//    NSLog(@"%@",inputParam);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"tip" message:inputParam delegate:nil
                                                                                cancelButtonTitle:@"sure" otherButtonTitles: nil] ;
    [alert show];
}

-(void)showParams2:(NSString *)inputParam callback:(WXModuleKeepAliveCallback) callback
{
    if(!inputParam){
        return;
    }
    //    NSLog(@"%@",inputParam);
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"tip" message:inputParam delegate:nil
//                                          cancelButtonTitle:@"sure" otherButtonTitles: nil] ;
//    [alert show];

    callback(@{@"param":@"hello world!"}, YES);

}

-(void)verifyFingerprint:(NSString *)verifyTimes callback:(WXModuleKeepAliveCallback) callback
{
    //未输入过密码，跳转登录界面
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"UserInfo"] boolValue]) {
        [self p_windowRootControllerWithLogin];
    }else {
        [self p_windowRootControllerWithUnlock:callback];
    }
}
-(void)verifyGes:(NSString *)verifyTimes callback:(WXModuleKeepAliveCallback) callback
{
    //未输入过密码，跳转登录界面
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"UserInfo"] boolValue]) {
        [self p_windowRootControllerWithLogin];
    }else {
        [self p_windowRootControllerWithUnlock:callback];
    }
    
}

- (void)p_touchIDFailed
{
    [self p_windowRootControllerWithLogin];
}

- (void)p_loginSuccess
{
//    [self p_windowRootControllerWithUnlock];
}

- (void)p_touchIDSuccess
{
    [self p_windowRootControllerWithHome];
}

- (void)p_touchIDLoginSuccess
{
    [self p_windowRootControllerWithHome];
}

//homeVC为rootController
- (void)p_windowRootControllerWithHome
{
    HomeViewController *homeVC = [[HomeViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homeVC];
    [[self findViewController] presentViewController:navigationController animated:YES completion:nil];
}

//loginVC为rootController
- (void)p_windowRootControllerWithLogin
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
//    self.window.rootViewController = loginVC;
//    [self.window makeKeyAndVisible];
    [TOP_VC presentViewController:loginVC animated:YES completion:nil];
//    [[self findViewController] presentViewController:loginVC animated:YES completion:nil];
}

//touchIDVC为rootController
- (void)p_windowRootControllerWithTouchID
{
    TouchIDViewController *touchIDVC = [[TouchIDViewController alloc] init];
//    self.window.rootViewController = touchIDVC;
//    [self.window makeKeyAndVisible];
    [[self findViewController] presentViewController:touchIDVC animated:YES completion:nil];
}

//unlockVC为rootController
- (void)p_windowRootControllerWithUnlock:(WXModuleKeepAliveCallback)callback
{
    UnlockViewController *unlockVC = [[UnlockViewController alloc] init];
    unlockVC.logincallback = ^(NSDictionary *dic){
        callback(dic,YES);
    };
    
    [TOP_VC presentViewController:unlockVC animated:YES completion:nil];
}

- (UIViewController *)findViewController
{
    for (UIView* next = self; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

@end
