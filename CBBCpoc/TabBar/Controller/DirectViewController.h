//
//  DirectViewController.h
//  CBBCpoc
//
//  Created by 万浩 on 2018/5/31.
//  Copyright © 2018年 Alibaba Cloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectViewController : UITabBarController

+ (instancetype)shareInstance;

@end
