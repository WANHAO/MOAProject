//
//  DirectViewController.m
//  CBBCpoc
//
//  Created by 万浩 on 2018/5/31.
//  Copyright © 2018年 Alibaba Cloud. All rights reserved.
//

#import "DirectViewController.h"
#import "MainTabBar.h"
#import "MainNavigationController.h"
#import "WXDemoViewController.h"
#import "HomeViewController.h"
#import "FirstPartViewController.h"
#import "SecPartViewController.h"
#import "ScanCodeViewController.h"
#import "MYHomeViewController.h"

#import "BaseController.h"
@interface DirectViewController ()<MainTabBarDelegate>
@property(nonatomic, weak)MainTabBar *mainTabBar;

@end

@implementation DirectViewController

/**
 *  单利
 *
 *  @return 唯一性
 */
+ (instancetype)shareInstance{
    static DirectViewController * instance = nil;
    static dispatch_once_t tocken ;
    
    dispatch_once(&tocken, ^{
        
        instance = [[DirectViewController alloc] init];
    });
    
    return instance;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self SetupMainTabBar];
    [self SetupAllControllers];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    for (UIView *child in self.tabBar.subviews) {
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
}

- (void)SetupMainTabBar{
    MainTabBar *mainTabBar = [[MainTabBar alloc] init];
    mainTabBar.frame = self.tabBar.bounds;
    mainTabBar.delegate = self;
    [self.tabBar addSubview:mainTabBar];
    _mainTabBar = mainTabBar;
}


- (void)SetupAllControllers{
    NSArray *titles = @[@"移动OA", @"工作圈", @"通讯录", @"我的"];
    NSArray *images = @[@"移动OA", @"工作圈", @"通讯录灰", @"wode"];
    NSArray *selectedImages = @[@"矢量智能对象拷贝24", @"工作圈active", @"通讯录", @"椭圆8"];
    NSArray *urlString1 = @[@"http://emas-ha-remote-log-poc.oss-cn-beijing.aliyuncs.com/eweex/app/111-11/upload/13c75e8c-c687-4b10-adbf-61a1cdcc18e5/index.js", @"http://emas-ha-remote-log-poc.oss-cn-beijing.aliyuncs.com/eweex/app/111-22/upload/8db1e47e-aa9c-4685-bf60-cd69216ba2a7/flance.js", @"http://emas-ha-remote-log-poc.oss-cn-beijing.aliyuncs.com/eweex/app/111-22/upload/3c63cb76-ef71-47bb-9aeb-0cfcfb6a1b7a/life.js", @"http://emas-ha-remote-log-poc.oss-cn-beijing.aliyuncs.com/eweex/app/111-11/upload/ed43d32c-a5c7-482b-8d80-a1d98782397d/activity.js"];
    
    NSArray *urlString2 = @[@"http://publish-poc.emas-ha.cn/app/111-22/index", @"http://publish-poc.emas-ha.cn/app/111-22/life", @"http://publish-poc.emas-ha.cn/app/111-22/flance", @"http://publish-poc.emas-ha.cn/app/111-22/activity"];
    
    WXDemoViewController *vc1 = [[WXDemoViewController alloc] init];
        vc1.url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"DIRhome" ofType:@"js" inDirectory:@""]];
//    vc1.url = [NSURL URLWithString:urlString2[0]];
    
    WXDemoViewController *vc2 = [[WXDemoViewController alloc] init];
        vc2.url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"DIRgold" ofType:@"js" inDirectory:@""]];
//    vc2.url = [NSURL URLWithString:urlString2[1]];
    
    WXDemoViewController *vc3 = [[WXDemoViewController alloc] init];
        vc3.url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"DIRlife" ofType:@"js" inDirectory:@""]];
//    vc3.url = [NSURL URLWithString:urlString2[2]];
    
    WXDemoViewController *vc4 = [[WXDemoViewController alloc] init];
        vc4.url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"DIRmine" ofType:@"js" inDirectory:@""]];
//    vc4.url = [NSURL URLWithString:urlString2[3]];
    
    
    NSArray *viewControllers = @[vc1,vc2,vc3,vc4];;
    
    for (int i = 0; i < viewControllers.count; i++) {
        UIViewController *childVc = viewControllers[i];
        [self SetupChildVc:childVc title:titles[i] image:images[i] selectedImage:selectedImages[i]];
    }
}


- (void)SetupChildVc:(UIViewController *)childVc title:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName{
    MainNavigationController *nav = [[MainNavigationController alloc] initWithRootViewController:childVc];
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    childVc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImageName];
    childVc.tabBarItem.title = title;
    [self.mainTabBar addTabBarButtonWithTabBarItem:childVc.tabBarItem];
    [self addChildViewController:nav];
}



#pragma mark --------------------mainTabBar delegate
- (void)tabBar:(MainTabBar *)tabBar didSelectedButtonFrom:(long)fromBtnTag to:(long)toBtnTag{
    self.selectedIndex = toBtnTag;
}


@end
