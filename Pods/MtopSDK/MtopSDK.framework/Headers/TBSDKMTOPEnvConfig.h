//
//  TBSDKMTOPEnvConfig.h
//  MtopSDK
//
//  Created by Roger on 11/27/14.
//  Copyright (c) 2014 mtop. All rights reserved.
//

#import <Foundation/Foundation.h>

/** TBSDK环境设置 */
typedef enum
{
    environmentDebug =  1,     /**< 枚举，预发环境 */
    environmentDaily,          /**< 枚举，日常环境 */
    environmentDailyTwo,       /**< 枚举，日常二套环境 */
    environmentRelease         /**< 枚举，正式环境 */
}MtopEnvironment;

@interface TBSDKMTOPEnvConfig : NSObject

@property (nonatomic, strong)            NSString             *appkey;
@property (nonatomic, strong)            NSString             *appSecret;
@property (nonatomic, strong)            NSString             *uid;
@property (nonatomic)                    MtopEnvironment      environment;
@property (strong, nonatomic)            NSString*            gatewayDomain;
@property (nonatomic, strong)            NSString             *customMtopRequestURL;
@property (nonatomic, strong)            NSString             *authCode;
@property (nonatomic, assign)            NSUInteger           appkeyIndex;
@property (nonatomic, strong)            NSArray              *tradeUnitDomainList;
//MTOP下发需更新交易单元化域名的API列表
@property (atomic, strong)               NSArray              *needUpdateTradeUnitList;
//appConf版本号
@property (atomic,copy)                  NSString             *appConfVersion;
//是否使用安全保镖(默认为yes)
@property (nonatomic, assign)            BOOL                 useSecurityGuard;
@property (nonatomic, assign)            BOOL                 enableHttps;//默认YES


+ (id)shareInstance;

- (NSString *)appkey;

- (NSString *)readUtdid;

- (void)fetchTradeUnitListFromDisk;

/*!
 * url encode
 */
+ (NSString *)urlEncodeString:(NSString *) string;
@end
