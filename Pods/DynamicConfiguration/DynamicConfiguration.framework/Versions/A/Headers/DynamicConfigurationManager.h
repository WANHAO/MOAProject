//
//  DynamicConfigurationManager.h
//  DynamicConfiguration
//
//  Created by haoxuan on 17/2/12.
//  Copyright © 2017年 Taobao lnc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DynamicConfigurationStorageProtocol.h"
#import "DynamicConfigurationUpdateProtocol.h"

//更新时机支持业务方控制，业务方在需要更新的时候发出这个notification就可以了。
#define DCConfigurationShouldUpdateNotificaion @"kDCConfigurationShouldUpdateNotificaion"

@interface DynamicConfigurationManager : NSObject

@property (nonatomic,weak) id<DynamicConfigurationStorageProtocol> storageDelegate;
@property (nonatomic,weak) id<DynamicConfigurationUpdateProtocol> updateDelegate;

/*
 * 单例后原有的init unavailable
 */
- (instancetype)init NS_UNAVAILABLE;

/*
 * 获取DynamicConfigurationManager实例
 */
+ (DynamicConfigurationManager*) sharedInstance;

/*
 * 根据goalUrl获取redirectUrl。
 * @return goalUrl 如果失败，则返回原goalUrl，否则返回对应的goalUrl
 * @para goalUrl
 */
- (NSString*)redirectUrl:(NSString*)goalUrl;

/*
 * 删除 goalurl 对应的配置。
 * @para goalUrl
 */
- (void)deleteConfigurationForGoalUrl:(NSString *)goalUrl;

@end
