//
//  DynamicConfigurationUpdateProtocol.h
//  DynamicConfiguration
//
//  Created by haoxuan on 17/2/16.
//  Copyright © 2017年 Taobao lnc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DynamicConfigurationUpdateProtocol <NSObject>
/*
 * 更新本地配置
 * @para 完成异步更新后的回调handler
 * handler 中的dictionary应该有个固定的格式
 */
- (void)updateConfigurationWithCompletionHandler:(void (^)(NSDictionary*))handler;

@end
