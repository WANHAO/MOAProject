//
//  DynamicConfigurationStorageProtocol.h
//  DynamicConfiguration
//
//  Created by haoxuan on 17/2/16.
//  Copyright © 2017年 Taobao lnc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DynamicConfigurationStorageProtocol <NSObject>
/*
 * 获取本地存储中key对应的value
 * @para key
 * @return value. 如果没有值返回nil
 */
- (id)valueForKey:(NSString*)key;
/*
 * 在本地存储中存储或修改key/value键值对
 * @para key/value 键值对。如果key或value为空，则什么也不做。
 */
- (void)setValue:(id)value forKey:(NSString*)key;
/*
 * 移除key对应的键值对.。如果不存在key对应的键值对，什么也不做。
 * @para key.
 */
- (void)removeObjectForKey:(NSString*)key;
/*
 * 移除本地所有键值对
 */
- (void)removeAllObjects;

@end
