//
//  MtopAuthBeforefilter.h
//  mtopext
//
//  Created by jiangpan on 2017/8/24.
//  Copyright © 2017年 taobao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BeforeFilter.h"

#define AUTH_BEFORE_FILTER_NAME @"b_auth_filter"

@interface MtopAuthBeforefilter : NSObject <BeforeFilter>

@property(assign, nonatomic) MtopAuthExpiredOption defaultAuthExpiredOption;  // 全局的默认

/*!
 * 获取单例
 */
+ (instancetype) getInstance;

@end
