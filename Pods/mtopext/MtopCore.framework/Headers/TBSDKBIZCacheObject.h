//
//  TBSDKBIZCacheObject.h
//  MtopSDK
//
//  Created by Roger.Mu on 11/26/14.
//  Copyright (c) 2014 Taobao.com. All rights reserved.
//


@interface TBSDKBIZCacheObject : NSObject


-(instancetype) initWithBlock: (NSString *) blockName
           withSize:(int) size
       withCompress:(BOOL) compress
        withEncrypt:(BOOL) encrypt
      withRemovable:(BOOL) removable;

@property (nonatomic, copy) NSString              *blockName;
@property (nonatomic)         int                   size;
@property (nonatomic)         BOOL                  compress;
@property (nonatomic)         BOOL                  encrypt;
@property (nonatomic)         BOOL                  removable;

// cache apis config list
@property (nonatomic, strong) NSMutableArray        *apis;

@end

#pragma mark -- biz inner class of api cache information

@interface ApiCacheInfo : NSObject

@property (nonatomic, copy) NSString              *api;
@property (nonatomic, copy) NSString              *v;
@property (nonatomic, copy) NSString              *scope;
@property (nonatomic, assign)         BOOL                  offline;
@property (nonatomic, assign)         BOOL                  cache;
@property (nonatomic, assign)         BOOL                  push;
@property (nonatomic, strong) NSMutableArray        *excKeyList;

- (instancetype) initWithApiName:(NSString *) api
                     withVersion:(NSString *) v
                       withScope:(NSString *) scope
                   withOfflineOp:(BOOL)       isOffline
                       withCache:(BOOL)       isCache
                        withPush:(BOOL)       isPush
            withExcludeQueryList:(NSArray *)  excKeyList;

@end
